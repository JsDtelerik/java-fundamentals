package day_14_midExamTraining;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class TreasureHunt {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        List<String> itemsInTheChest = Arrays.stream(scan.nextLine().split("\\|")).collect(Collectors.toList());
        String[] command = scan.nextLine().split("\\s+");

        while (!command[0].equals("Yohoho!")){

            switch (command[0]){
                case "Loot":
                    itemsInTheChest = lootItemsInChest(itemsInTheChest, command);
                    break;
                case "Drop":
                    itemsInTheChest = dropItemAndMoveItToTheEnd(itemsInTheChest, command);
                    break;
                case "Steal":
                    itemsInTheChest = removeStollenItemsAndPrintTheListWithStollenItems(itemsInTheChest, command);
                    break;

            }
            command = scan.nextLine().split("\\s+");
        }

        printTreasure(itemsInTheChest);


        }


    static List<String> lootItemsInChest(List<String> itemsInTheChest, String[] command) {

        for (int i = 1; i <command.length; i++) {
            if(itemsInTheChest.contains(command[i])){
                continue;
            }else{
                itemsInTheChest.add(0, command[i]);
            }
        }
        return itemsInTheChest;
    }

    static List<String> dropItemAndMoveItToTheEnd(List<String> itemsInTheChest, String[] command) {
        if(Integer.parseInt(command[1]) < 0 || Integer.parseInt(command[1])>itemsInTheChest.size()-1){
            return itemsInTheChest;
        }else{
            itemsInTheChest.add(itemsInTheChest.get(Integer.parseInt(command[1])));
            itemsInTheChest.remove(Integer.parseInt(command[1]));
        }

        return itemsInTheChest;
    }

    static List<String> removeStollenItemsAndPrintTheListWithStollenItems(List<String> itemsInTheChest, String[] command) {
        int commandIndex = Integer.parseInt(command[1]);
        List<String> stollenItems = new ArrayList<>();
        if(commandIndex>itemsInTheChest.size()-1){
            for (int i = itemsInTheChest.size()-1; i >= 0; i--) {
                stollenItems.add(0, itemsInTheChest.get(i));
            }
        }else if(commandIndex<0){
            return itemsInTheChest;
        }else {
            for (int i = itemsInTheChest.size()-1; i >= itemsInTheChest.size()-commandIndex; i--) {
                stollenItems.add(0, itemsInTheChest.get(i));
            }
        }

        for (int i = 0; i < stollenItems.size(); i++) {
                itemsInTheChest.remove(stollenItems.get(i));

        }
        System.out.println(String.join(", ", stollenItems));

    return itemsInTheChest;
    }
    static double calculateTheAverageSumOfItemsLength(List<String> itemsInTheChest) {
        double separatedItemsValue= 0.0;
        for (int i = 0; i < itemsInTheChest.size(); i++) {
            String item = itemsInTheChest.get(i);
            separatedItemsValue += item.length();

        }

        return separatedItemsValue/itemsInTheChest.size();
    }
    static void printTreasure(List<String> itemsInTheChest) {


        if(itemsInTheChest.isEmpty()){
            System.out.println("Failed treasure hunt.");
        }else{
            double averageSum = calculateTheAverageSumOfItemsLength(itemsInTheChest);
            System.out.printf("Average treasure gain: %.02f pirate credits.", averageSum);
        }
    }
}