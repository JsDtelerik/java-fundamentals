package day_14_midExamTraining;

import java.util.Scanner;

public class BlackFlag {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int daysOfPlunder = Integer.parseInt(scan.nextLine());
        int dailyPlunder = Integer.parseInt(scan.nextLine());
        double expectedPlunder = Double.parseDouble(scan.nextLine());
        double collectedPlunder = 0.0;

        for (int i = 1; i <=daysOfPlunder; i++) {

        collectedPlunder +=dailyPlunder;

        if(i % 3 == 0){
            collectedPlunder += dailyPlunder*0.5;
        }
        if(i % 5 == 0){
            collectedPlunder *= 0.70;
        }
        }
        if(expectedPlunder<=collectedPlunder){
            System.out.printf("Ahoy! %.02f plunder gained.", collectedPlunder);
        }else{
            System.out.printf("Collected only %.02f%% of the plunder.", (collectedPlunder/expectedPlunder)*100);
        }

    }
}
