package day_14_midExamTraining;


import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class ManOWar {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        List<String> pirateShip = Arrays.stream(scan.nextLine().split(">")).collect(Collectors.toList());

        List<String> warShip = Arrays.stream(scan.nextLine().split(">")).collect(Collectors.toList());

        int sectionMaxHeath = Integer.parseInt(scan.nextLine());

        String[] command = scan.nextLine().split("\\s+");
        boolean checkPirateShip = false;
        boolean checkWarShip = false;

        while (!command[0].equals("Retire")){

            switch (command[0]){
                case "Fire":
                warShip =  tryToSunkTheWarship(warShip, command);
                    break;
                case "Defend":
                pirateShip = tryToSunkPirateShip(pirateShip, command);
                    break;
                case "Repair":
                pirateShip = repairSectionOfThePirateShip(pirateShip, command, sectionMaxHeath);
                    break;
                case "Status":
                    printStatusOfThePirateShip(pirateShip, sectionMaxHeath);
                    break;
            }

            checkPirateShip = checkShipSections (pirateShip);
             checkWarShip = checkShipSections(warShip);
            if(checkPirateShip){
                break;
            }

            if(checkWarShip){
                break;
            }

            command = scan.nextLine().split("\\s+");
        }

        if(checkWarShip){
            System.out.println("You won! The enemy ship has sunken.");
        }else if(checkPirateShip){
            System.out.println("You lost! The pirate ship has sunken.");
        }else{
            printStaleMate(warShip, pirateShip);
        }
    }
    static boolean checkShipSections(List<String> ship) {
        for (int i = 0; i < ship.size(); i++) {
            int value = Integer.parseInt(ship.get(i));
            if(value == 0){
                return true;
            }
        }
        return false;
    }

    private static boolean chekFirstIndex(int index, List<String> ship) {

        if(index<0 ||index>ship.size()-1){
            return false;
        }else {
            return true;
        }
    }

    static List<String> tryToSunkTheWarship(List<String> warShip, String[] command) {
        boolean index = chekFirstIndex(Integer.parseInt(command[1]), warShip);
        int indexToInt = Integer.parseInt(command[1]);
        int demage = Integer.parseInt(command[2]);

        if(index){
            int getWarshipSectionToInt = Integer.parseInt(warShip.get(indexToInt))-demage;
            if(getWarshipSectionToInt<0){
                getWarshipSectionToInt = 0;
            }
            String addThisString = String.valueOf(getWarshipSectionToInt);
            warShip.set(indexToInt, addThisString);
        }else{
            return warShip;
        }

        return warShip;
    }
    static List<String> tryToSunkPirateShip(List<String> pirateShip, String[] command) {
        boolean indexOne = chekFirstIndex(Integer.parseInt(command[1]), pirateShip);
        boolean indexTwo = chekFirstIndex(Integer.parseInt(command[2]), pirateShip);
        int start = Integer.parseInt(command[1]);
        int end = Integer.parseInt(command[2]);
        if (indexOne && indexTwo){

            for (int i = start; i <= end ; i++) {
                int currentPositionValue =  Integer.parseInt(pirateShip.get(i))-Integer.parseInt(command[3]);
                if(currentPositionValue<0){
                    currentPositionValue = 0;
                }
                pirateShip.set(i, String.valueOf(currentPositionValue));

            }
        }

        return pirateShip;
    }



    static List<String> repairSectionOfThePirateShip(List<String> pirateShip, String[] command, int sectionMaxHeath) {
        boolean index = chekFirstIndex(Integer.parseInt(command[1]), pirateShip);
        if(index){
        int repairWith = Integer.parseInt(command[2]);
                int checkSection = Integer.parseInt(pirateShip.get(Integer.parseInt(command[1])))+repairWith;

            if(checkSection>sectionMaxHeath){
                int sectionHealth = sectionMaxHeath;
                pirateShip.set(Integer.parseInt(command[1]), String.valueOf(sectionHealth));
            }else{
                pirateShip.set(Integer.parseInt(command[1]), String.valueOf(checkSection));
            }
        }

        return pirateShip;
    }

    static void printStatusOfThePirateShip(List<String> pirateShip, int sectionMaxHeath) {

        int counter = 0;
        int lowestPercentHealth = (int)(sectionMaxHeath*0.2);
        for (int i = 0; i < pirateShip.size(); i++) {
            int value = Integer.parseInt(pirateShip.get(i));
            if(value<lowestPercentHealth){
                counter++;
            }
        }

        System.out.printf("%d sections need repair.%n", counter);
    }

    static void printStaleMate(List<String> warShip, List<String> pirateShip) {
        int sumOfPirateShip = 0;
        int sumOfWarShip = 0;

        for (int i = 0; i < pirateShip.size(); i++) {
            int value = Integer.parseInt(pirateShip.get(i));
            sumOfPirateShip +=value;
        }
        for (int i = 0; i < warShip.size(); i++) {
            int value = Integer.parseInt(warShip.get(i));
            sumOfWarShip +=value;
        }

        System.out.printf("Pirate ship status: %d%n" +
                "Warship status: %d", sumOfPirateShip, sumOfWarShip);
    }
}
