package day_14_midExamTraining;

import java.util.*;
import java.util.stream.Collectors;

public class assist_ {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        List<String> initialLoot = Arrays.stream(scanner.nextLine().split("\\|")).collect(Collectors.toList());

        String input = scanner.nextLine();
        while (!"Yohoho!".equals(input)) {

            List<String> command = Arrays.stream(input.split("\\s+")).collect(Collectors.toList());
            switch (command.get(0)) {
                case "Loot":
                    command.remove(0);
                    loot(initialLoot, command);
                    break;
                case "Drop":
                    drop(initialLoot, command);
                    break;
                case "Steal":
                    List<String> stolenLoot = new ArrayList<>();
                    steal(initialLoot, command, stolenLoot);

                    break;
            }
            input = scanner.nextLine();
        }

        if (initialLoot.isEmpty()) {
            System.out.println("Failed treasure hunt.");
        } else {
            double sumOfElementsLength = 0.0;
            for (String s : initialLoot) {
                sumOfElementsLength += s.length();
            }
            System.out.printf("Average treasure gain: %.2f pirate credits.", sumOfElementsLength / initialLoot.size());
        }
    }

    private static void loot(List<String> initialLoot, List<String> command) {
        for (String s : command) {
            if (!initialLoot.contains(s)) {
                initialLoot.add(s);
                Collections.rotate(initialLoot, 1);
            }
        }
    }

    private static void drop(List<String> initialLoot, List<String> command) {
        int index = Integer.parseInt(command.get(1));

        if (index >= 0 && index < initialLoot.size()) {
            String currLoot = initialLoot.get(index);
            initialLoot.remove(index);
            initialLoot.add(currLoot);
        }
    }

    private static void steal(List<String> initialLoot, List<String> command, List<String> stolenLoot) {
        int count = Integer.parseInt(command.get(1));

        if (count <= initialLoot.size() && count >=0) {
            for (int i = 0; i < count; i++) {
                String currLoot = initialLoot.get(initialLoot.size() - 1);
                initialLoot.remove(initialLoot.size() - 1);
                stolenLoot.add(currLoot);
                Collections.rotate(stolenLoot, 1);
            }
        } else if(count<0) {
            return;
        }else{
            for (int i = 0; i < initialLoot.size() ; i++) {
                stolenLoot.add(initialLoot.get(i));

            }
            initialLoot.clear();
        }

        String stolen = String.join(", ", stolenLoot);
        System.out.println(stolen);

    }
}