package day_14_midExamTraining;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class AssistShip {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        List<Integer> pirateShip = Arrays.stream(scanner.nextLine().split(">")).map(Integer::parseInt).collect(Collectors.toList());
        List<Integer> warShip = Arrays.stream(scanner.nextLine().split(">")).map(Integer::parseInt).collect(Collectors.toList());
        int maxHealth = Integer.parseInt(scanner.nextLine());

        String input = scanner.nextLine();
        while (!"Retire".equals(input)) {
            String[] command = input.split("\\s+");


            switch (command[0]) {
                case "Fire":
                    int fireIndex = Integer.parseInt(command[1]);
                    int damage = Integer.parseInt(command[2]);

                    if (validIndex(warShip, fireIndex)) {
                        fire(warShip, fireIndex, damage);
                        if (warShip.get(fireIndex) <= 0) {
                            System.out.println("You won! The enemy ship has sunken.");
                            return;
                        }
                    }
                    break;
                case "Defend":
                    int startIndex = Integer.parseInt(command[1]);
                    int endIndex = Integer.parseInt(command[2]);
                    int defendDamage = Integer.parseInt(command[3]);

                    if (validIndex(pirateShip, startIndex) && validIndex(pirateShip, endIndex)) {
                        defend(pirateShip, startIndex, endIndex, defendDamage);

                        for (Integer section : pirateShip) {
                            if(section <= 0) {
                                System.out.println("You lost! The pirate ship has sunken.");
                                return;
                            }
                        }
                    }
                    break;
                case "Repair":
                    int health = Integer.parseInt(command[2]);
                    int repairIndex = Integer.parseInt(command[1]);
                    if (validIndex(pirateShip, repairIndex)) {
                        repair(pirateShip, repairIndex, health, maxHealth);
                    }
                    break;
                case "Status":

                    int sectionCounter = 0;
                    status(pirateShip, sectionCounter, maxHealth);
                    break;
            }


            input = scanner.nextLine();
        }
        int pirateShipSectionSum = 0;
        int warshipSectionSum = 0;

        for (Integer section : pirateShip) {
            pirateShipSectionSum += section;
        }
        for (Integer section : warShip) {
            warshipSectionSum += section;
        }
        System.out.println("Pirate ship status: " + pirateShipSectionSum);
        System.out.println("Warship status: " + warshipSectionSum);
    }

    private static boolean validIndex(List<Integer> ship, int index) {
        return index >= 0 && index < ship.size();
    }

    private static void fire(List<Integer> warShip, int index, int damage) {
        warShip.set(index, warShip.get(index) - damage);

    }

    private static void defend(List<Integer> pirateShip, int index, int endIndex, int defendDamage) {
        for (int i = index; i <= endIndex; i++) {
            pirateShip.set(i, pirateShip.get(i) - defendDamage);
        }
    }

    private static void repair(List<Integer> pirateShip, int index, int health, int maxHealth) {
        pirateShip.set(index, pirateShip.get(index) + health);

        if (pirateShip.get(index) > maxHealth) {
            pirateShip.set(index, maxHealth);
        }
    }

    private static void status(List<Integer> pirateShip, int sectionCounter, int maxHealth) {
        for (Integer section : pirateShip) {
            double percentage = (section * 1.0 / maxHealth) * 100;
            if (percentage < 20) {
                sectionCounter++;
            }
        }
        System.out.printf("%d sections need repair.%n", sectionCounter);
    }
}


