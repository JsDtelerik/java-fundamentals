package day_11_lists;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class AppendArraysEd {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        List<String> numbers = Arrays.stream(scan.nextLine().split("\\|+")).collect(Collectors.toList());
        String appended = "";
        for (int i = numbers.size()-1; i >=0 ; i--) {
                appended +=numbers.get(i)+ " ";
        }

        appended = appended.trim().replaceAll("\\s+", " ");
        System.out.println(appended);
    }
}
