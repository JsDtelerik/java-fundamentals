package day_11_lists;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class houseParty {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        List<String> names = new ArrayList<>();
        int numOfCommands = Integer.parseInt(scan.nextLine());

        for (int i = 1; i <= numOfCommands; i++) {
            String[] command = scan.nextLine().split("\\s+");
            if(command[2].equals("going!")){
                if(names.contains(command[0])){
                    System.out.printf("%s is already in the list!%n", command[0]);;
                }else{
                    names.add(command[0]);
                }
            }else if(command[2].equals("not")){
                    if(!names.contains(command[0])){
                        System.out.printf("%s is not in the list!%n", command[0]);
                    }else{
                        names.remove(command[0]);
                    }
            }
        }
        printGuestList(names);
    }

    private static void printGuestList(List<String> names) {
        for (String name : names) {
            System.out.println(name);

        }
    }
}
