package day_11_lists;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class tratfirstED {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        List<String> arrayOfData = Arrays.stream(scan.nextLine().split("\\s+")).collect(Collectors.toList());

        List<String> command = Arrays.stream(scan.nextLine().split("\\s+")).collect(Collectors.toList());

        while (!command.get(0).equals("3:1")) {

            switch (command.get(0)) {
                case "merge":

                    int chekStartIndex = checkIndexes(arrayOfData, command.get(1));
                    int checkEndIndex = checkIndexes(arrayOfData, command.get(2));
                    arrayOfData = mergeElements(chekStartIndex, checkEndIndex, arrayOfData);

                    break;
                case "divide":
                    int chekDivideIndex = checkIndexes(arrayOfData, command.get(1));
                    boolean invalidDivider = false;
                    if(Integer.parseInt(command.get(2))<=0 || Integer.parseInt(command.get(2))>100){
                        invalidDivider = true;
                    }
                    if(!invalidDivider){
                        arrayOfData = returnDividedResultOfData(arrayOfData, chekDivideIndex,Integer.parseInt(command.get(2)));
                    }


                    break;
            }

            command = Arrays.stream(scan.nextLine().split("\\s+")).collect(Collectors.toList());
        }


        printResult(arrayOfData);

    }


    static Integer checkIndexes(List<String> arrayOfData, String s) {
        int indexToInt = Integer.parseInt(s);
        if (indexToInt < 0) {
            return 0;
        } else if (indexToInt > arrayOfData.size() - 1) {
            return arrayOfData.size() - 1;
        } else {
            return indexToInt;
        }
    }

    static List<String> mergeElements(int chekStartIndex, int checkEndIndex, List<String> arrayOfData) {
        StringBuilder appendElements = new StringBuilder();
        for (int i = chekStartIndex; i <= checkEndIndex; i++) {
            appendElements.append(arrayOfData.get(i));

        }
        int counter = 0;
        for (int i = chekStartIndex; i <= checkEndIndex; i++) {

            arrayOfData.remove(i - counter);
            counter++;


        }
        arrayOfData.add(chekStartIndex, appendElements.toString());
        return arrayOfData;
    }

    static List<String> returnDividedResultOfData(List<String> arrayOfData, int chekDivideIndex, int divide) {


        String[] dividedWord = new String[divide];
        String wordToBeDivided = arrayOfData.get(chekDivideIndex);

        int index = 0;
        int counter = 0;


        int symbolPerGroup = wordToBeDivided.length() / divide;
        int symbolsInLastGroup = 0;
        if (wordToBeDivided.length() % 2 == 0) {
            symbolsInLastGroup = wordToBeDivided.length() / divide;
        } else {
            symbolsInLastGroup = (wordToBeDivided.length() / divide) + 1;
        }
        StringBuilder appendElements = new StringBuilder();

        for (int i = 0; i <= wordToBeDivided.length()-1; i++) {
            counter++;
            if (counter <= symbolPerGroup && index < dividedWord.length - 1) {
                appendElements.append(wordToBeDivided.charAt(i));
                if(counter==symbolPerGroup){
                    dividedWord[index++] = appendElements.toString().trim();
                    appendElements = new StringBuilder();
                    counter = 0;
                }
            } else if (counter <= symbolsInLastGroup && index <= dividedWord.length) {
                appendElements.append(wordToBeDivided.charAt(i));
                if(counter == symbolsInLastGroup){
                    dividedWord[index++] = appendElements.toString().trim();
                    appendElements = new StringBuilder();
                    counter = 0;
                }
            }




        }
        arrayOfData.remove(chekDivideIndex);
        index = chekDivideIndex;
        for (int i = 0; i < dividedWord.length; i++) {
            arrayOfData.add(index++, dividedWord[i]);

        }

        return arrayOfData;
    }




    private static void printResult(List<String> arrayOfData) {

        System.out.println(String.join(" ", arrayOfData));

    }

}

