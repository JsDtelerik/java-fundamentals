package day_11_lists;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class AnonymousThreat {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        List<String> arrayOfData = Arrays.stream(scan.nextLine().split("\\s+")).collect(Collectors.toList());

        List<String> command = Arrays.stream(scan.nextLine().split("\\s+")).collect(Collectors.toList());

        while (!command.get(0).equals("3:1")) {

            switch (command.get(0)) {
                case "merge":

                    int chekStartIndex = checkIndexes(arrayOfData, command.get(1));
                    int checkEndIndex = checkIndexes(arrayOfData, command.get(2));
                    arrayOfData = mergeElements(chekStartIndex, checkEndIndex, arrayOfData);

                    break;
                case "divide":
                    int chekDivideIndex = checkIndexes(arrayOfData, command.get(1));
                    boolean invalidDivider = false;
                    if(Integer.parseInt(command.get(2))<=0 || Integer.parseInt(command.get(2))>100){
                        invalidDivider = true;
                    }
                    if(!invalidDivider){
                        arrayOfData = returnDividedResultOfData(arrayOfData, chekDivideIndex,Integer.parseInt(command.get(2)));
                    }

                    break;
            }

            command = Arrays.stream(scan.nextLine().split("\\s+")).collect(Collectors.toList());
        }

        printResult(arrayOfData);

    }

    static Integer checkIndexes(List<String> arrayOfData, String s) {
        int indexToInt = Integer.parseInt(s);
        if (indexToInt < 0) {
            return 0;
        } else if (indexToInt > arrayOfData.size() - 1) {
            return arrayOfData.size() - 1;
        } else {
            return indexToInt;
        }
    }

    static List<String> mergeElements(int chekStartIndex, int checkEndIndex, List<String> arrayOfData) {
        StringBuilder appendElements = new StringBuilder();
        for (int i = chekStartIndex; i <= checkEndIndex; i++) {
            appendElements.append(arrayOfData.get(i));
        }
        int counter = 0;
        for (int i = chekStartIndex; i <= checkEndIndex; i++) {

            arrayOfData.remove(i - counter);
            counter++;

        }
        arrayOfData.add(chekStartIndex, appendElements.toString());
        return arrayOfData;
    }

    static List<String> returnDividedResultOfData(List<String> arrayOfData, int chekDivideIndex, int divide) {


        List<String> dividedWord = new ArrayList<>();
        String wordToBeDivided = arrayOfData.get(chekDivideIndex);

        int index = 0;
        int counter = 0;

        int symbolPerGroup = wordToBeDivided.length() / divide;

        int groups = divide-1;
        StringBuilder appendElements = new StringBuilder();
        int groupCounter = 0;
        int symbolsInLastGroup = 0;
        for (int i = 0; i <= wordToBeDivided.length()-1; i++) {
            counter++;
            if (counter <= symbolPerGroup && groupCounter<groups) {
                appendElements.append(wordToBeDivided.charAt(i));
                if(counter==symbolPerGroup){
                    dividedWord.add(index++, appendElements.toString());
                    appendElements = new StringBuilder();
                    counter = 0;
                    groupCounter++;
                }
            } else {
                symbolsInLastGroup = wordToBeDivided.length()-(symbolPerGroup*groupCounter);
                appendElements.append(wordToBeDivided.charAt(i));
                if(counter == symbolsInLastGroup){
                    dividedWord.add(index++, appendElements.toString());
                    appendElements = new StringBuilder();
                    counter = 0;
                    groupCounter++;
                }

            }

        }
        arrayOfData.remove(chekDivideIndex);
        index = chekDivideIndex;
        for (int i = 0; i < dividedWord.size(); i++) {
            arrayOfData.add(index++, dividedWord.get(i));

        }

        return arrayOfData;
    }


    private static void printResult(List<String> arrayOfData) {

        System.out.println(String.join(" ", arrayOfData));

    }

}
