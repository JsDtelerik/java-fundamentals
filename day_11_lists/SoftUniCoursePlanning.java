package day_11_lists;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class SoftUniCoursePlanning {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        List<String> schedule = Arrays.stream(scan.nextLine().split(",\\s+")).collect(Collectors.toList());
        List<String> command = Arrays.stream(scan.nextLine().split(":")).collect(Collectors.toList());

        while (!command.get(0).equals("course start")){

            String commandToSwitch = command.get(0);

            switch (commandToSwitch){
                case "Add":
                schedule = addANewLessonAtTheEndOfOurSchedule(schedule, command.get(1));
                    break;
                case "Insert":
                schedule = insertANewLessonAtTheGivenIndex(schedule, command.get(1), Integer.parseInt(command.get(2)));
                    break;
                case "Remove":
                schedule = removeALessonIfItExist(schedule, command.get(1));
                    break;
                case "Swap":
                schedule = swapLessons(schedule, command.get(1), command.get(2));
                    break;
                case "Exercise":
                schedule = addExercise(schedule, command.get(1));
                    break;
            }
            command = Arrays.stream(scan.nextLine().split(":")).collect(Collectors.toList());
        }
        printSchedule(schedule);
    }


    static List<String> swapLessons(List<String> schedule, String firstLesson, String secondLesson) {
        String firstLessonCheck = firstLesson +"-Exercise";
        String secondLessonCheck = secondLesson +"-Exercise";
           int firstLessonIndex = 0;
           boolean firstLessonChecker = false;
           if(schedule.contains(firstLessonCheck)){
               firstLessonChecker = true;
           }

        int secondLessonIndex = 0;
        boolean secondLessonChecker = false;
        if(schedule.contains(secondLessonCheck)) {
            secondLessonChecker = true;
        }
        label:
         if(schedule.contains(firstLesson)){
             for (int i = 0; i < schedule.size(); i++) {
                 if(schedule.get(i).equals(firstLesson)){
                     firstLessonIndex = i;
                     if(schedule.contains(secondLesson)){
                         for (int j = 0; j < schedule.size(); j++) {
                             if(schedule.get(j).equals(secondLesson)){
                                 secondLessonIndex = j;
                                 schedule.set(i, secondLesson);
                                 schedule.set(j, firstLesson);
                                 if(firstLessonChecker && !secondLessonChecker){

                                     schedule.add(j+1, firstLessonCheck);
                                     schedule.remove(i+1);
                                     break label;
                                 }else if(firstLessonChecker && secondLessonChecker){
                                     schedule.remove(i+1);
                                     schedule.remove(j+1);
                                     schedule.add(i+1, secondLessonCheck);
                                     schedule.add(j+1, firstLessonCheck);
                                     break label;
                                 }else if(!firstLessonChecker && secondLessonChecker){
                                     schedule.remove(j+1);
                                     schedule.add(i+1, secondLessonCheck);
                                     break label;
                                 }else{
                                     break label;
                                 }
                             }
                         }
                     }
                 }
             }
         }

        return schedule;
    }


    static List<String> addANewLessonAtTheEndOfOurSchedule(List<String> schedule, String lessonTitle) {
            if(schedule.contains(lessonTitle)){
                return schedule;
            }else{
                schedule.add(schedule.size(), lessonTitle);
            }
        return schedule;
    }


    static List<String> insertANewLessonAtTheGivenIndex(List<String> schedule, String lessonTitle, int index) {

        if(schedule.contains(lessonTitle)){
            return schedule;
        }else{
            schedule.add(index, lessonTitle);
        }

        return schedule;
    }

    static List<String> removeALessonIfItExist(List<String> schedule, String lessonTitle) {
        String checker = lessonTitle + "-Exercise";
        if(schedule.contains(checker)){
            schedule.remove(checker);
        }
        if (schedule.contains(lessonTitle)){
            schedule.remove(lessonTitle);
        }else{
            return schedule;
        }

        return schedule;
    }


    static List<String> addExercise(List<String> schedule, String lessonTitle) {
        String lessonPlusExercise = lessonTitle+"-Exercise";
        if(schedule.contains(lessonTitle)){
            if(schedule.contains(lessonPlusExercise)){
                return schedule;
            }else{
                for (int i = 0; i < schedule.size(); i++) {
                    if(schedule.get(i).equals(lessonTitle)){
                        schedule.add(i+1, lessonPlusExercise);
                    }
                }
            }

        }else{
            schedule.add(schedule.size(), lessonTitle);
            schedule.add(schedule.size(), lessonPlusExercise);
        }

        return schedule;
    }


    static void printSchedule(List<String> schedule) {
        int index = 0;
        int counter = 1;
        for (String s : schedule) {
            System.out.printf("%d.%s%n",counter,schedule.get(index));
            index++;
            counter++;

        }
    }
}
