package day_11_lists;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class AppendArrays {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        List<String> numbers = Arrays.stream(scan.nextLine().split("\\s+")).collect(Collectors.toList());
        List<String> editedList = new ArrayList<>();
        boolean isDone = false;
        for (int i = 0; i < numbers.size(); i++) {
            if(numbers.get(i).equals("|")){
                i++;
            }
            List<String> tempArr = getTempListWhileNumbersIndexIsEqualOrContainsI(numbers, i);

            for (int j = 0; j < tempArr.size(); j++) {
                editedList.add(j, tempArr.get(j));

            }

            i += tempArr.size()-1;
            tempArr =  cleanTempArr(tempArr);
        }

        editedList = removeIfromTheEditedList(editedList);

        System.out.println(String.join(" ", editedList));
       }




    private static List<String> getTempListWhileNumbersIndexIsEqualOrContainsI(List<String> numbers, int i) {
        List<String> temp = new ArrayList<>();
        int tempIndex = 0;
        for (int index = i; index < numbers.size(); index++) {
            if(numbers.get(index).equals("|")){
                    return temp;
                }


            if(numbers.get(index).contains("|")){
                temp.add(tempIndex, numbers.get(index));
                return temp;
            }else{
                temp.add(tempIndex, numbers.get(index));
                tempIndex++;
            }
        }
        return temp;
    }


    private static List<String> cleanTempArr(List<String> tempArr) {
        for (int i = 0; i < tempArr.size(); i++) {
            tempArr.remove(i);
        }
        return tempArr;
    }

    static List<String> removeIfromTheEditedList(List<String> editedList) {
        StringBuilder convert = new StringBuilder();
        for (int i = 0; i < editedList.size(); i++) {
            convert.append(editedList.get(i)+ " ");
        }
        String convertToString = convert.toString();
        List<String> convertedList = Arrays.stream(convertToString.split("\\|")).collect(Collectors.toList());
        return convertedList;
    }


}
