package day_11_lists;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class bombNumbers {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        List<Integer> numbers = Arrays.stream(scan.nextLine().split("\\s+")).map(Integer::parseInt).collect(Collectors.toList());

        int[] bombPower = Arrays.stream(scan.nextLine().split("\\s+")).mapToInt(Integer::parseInt).toArray();

        boolean chekIfListContainsBomb = check(numbers, bombPower);
        List<Integer> editedList = new ArrayList<>();
        if(chekIfListContainsBomb){
            editedList = editListOfNumber(numbers, bombPower);
        }else{
            editedList = numbers;
        }

        int sumNumbers = sumNumbersFromEditedList(editedList);
        printResult(sumNumbers);
    }


    private static boolean check(List<Integer> numbers, int[] bombPower) {

        if(numbers.contains(bombPower[0])){
            return true;
        }else{
            return false;
        }
    }

    static List<Integer> editListOfNumber(List<Integer> numbers, int[] bombPower) {
        int keepIndexOfLastRightDetonation = 0;
        for (int i = 0; i < numbers.size(); i++) {
            boolean detonation = false;
            if(numbers.get(i) == bombPower[0]){
                for (int detonationLeft = 1; detonationLeft <= bombPower[1]; detonationLeft++) {
                    if(i-detonationLeft>=0){
                        numbers.set(i-detonationLeft, bombPower[0]);

                    }
                }
                for (int detonationRight = 1; detonationRight <= bombPower[1]; detonationRight++) {
                    if(i+detonationRight<=numbers.size()-1){
                        numbers.set(i+detonationRight, bombPower[0]);
                        detonation =true;
                        keepIndexOfLastRightDetonation =i+detonationRight+1;
                    }
                }

            }
            if(detonation){
                i = keepIndexOfLastRightDetonation;
            }

        }

        for (int i = 0; i < numbers.size(); i++) {
            if(numbers.get(i) == bombPower[0]){
                numbers.remove(i);
                i=-1;
            }
        }

        return numbers;
    }


    static int sumNumbersFromEditedList(List<Integer> editedList) {
        int sum = 0;
        for (int i = 0; i < editedList.size(); i++) {

            sum+=editedList.get(i);
        }
        return sum;
    }


    static void printResult(int sumNumbers) {
        System.out.println(sumNumbers);
    }
}
