package day_11_lists;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class changeList {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        List<Integer> input = Arrays.stream(scan.nextLine().split("\\s+")).map(Integer::parseInt).collect(Collectors.toList());

        String[] command = scan.nextLine().split("\\s+");

        while (!command[0].equals("end")){
            if(command[0].equals("Delete")){
                input.removeAll(Arrays.asList(Integer.parseInt(command[1])));
            }else if (command[0].equals("Insert")){
                input.add(Integer.parseInt(command[2]), Integer.parseInt(command[1]));
            }

            command = scan.nextLine().split("\\s+");
        }
       printResult(input);
    }

    private static void printResult(List<Integer> input) {
        for (Integer integer : input) {
            System.out.print(integer + " ");

        }
    }
}
