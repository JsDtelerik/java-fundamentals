package day_11_lists;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class train {

        public static void main(String[] args) {
            Scanner scan = new Scanner(System.in);

            List<Integer> wagons = Arrays.stream(scan.nextLine().split("\\s+")).map(Integer::parseInt).collect(Collectors.toList());
            int maxCapacityPerWagon = Integer.parseInt(scan.nextLine());

            String[] command = scan.nextLine().split("\\s+");

            while (!command[0].equals("end")){
                if(command[0].equals("Add")){
                    wagons.add(wagons.size(), Integer.parseInt(command[1]));
                }else{
                    for(int i = 0; i <wagons.size(); i++) {

                        if(wagons.get(i)+Integer.parseInt(command[0])<=maxCapacityPerWagon){
                            int sumPassengers = wagons.get(i)+Integer.parseInt(command[0]);
                            wagons.set(i, sumPassengers);
                            break;
                        }
                    }
                }
                command = scan.nextLine().split("\\s+");
            }
            printResult(wagons);

        }

        private static void printResult(List<Integer> wagons) {
            for (Integer passengers : wagons) {
                System.out.print(passengers + " ");
            }
        }
    }


