package day_11_lists;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class listOperations {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        List<Integer> numbers = Arrays.stream(scan.nextLine().split("\\s+")).map(Integer::parseInt).collect(Collectors.toList());

        String[] command = scan.nextLine().split("\\s+");
        // List<Integer> editedList = new ArrayList<>();
        while (!command[0].equals("End")){

            switch (command[0]){

                case "Add":
                    numbers = addOperation(numbers, command);
                    break;
                case "Insert":
                    numbers = insertOperation(numbers, command);
                    break;
                case "Remove":
                    numbers = removeOperation(numbers, command);
                    break;
                case "Shift":
                    numbers = shiftOperation(numbers, command);
                    break;
            }

            command = scan.nextLine().split("\\s+");
        }
        printResult(numbers);
    }

    static List<Integer> addOperation(List<Integer> numbers, String[] command) {
        numbers.add(numbers.size(), Integer.parseInt(command[1]));

        return numbers;
    }

    static List<Integer> insertOperation(List<Integer> numbers, String[] command) {
        if(indexChek(numbers, command)){
            numbers.add(Integer.parseInt(command[2]), Integer.parseInt(command[1]));
            return numbers;
        }else{
           return numbers;
        }
    }

    static List<Integer> removeOperation(List<Integer> numbers, String[] command) {
        if(indexChek(numbers, command)){
            numbers.remove(Integer.parseInt(command[1]));
            return  numbers;
        }else{
            return numbers;
        }
    }

    static List<Integer> shiftOperation(List<Integer> numbers, String[] command) {
        if(command[1].equals("right")){

            for (int i = 0; i < Integer.parseInt(command[2]); i++) {

                numbers.add(0, numbers.get(numbers.size()-1));
                numbers.remove(numbers.size()-1);
            }
            return numbers;
        }else {
            for (int i = Integer.parseInt(command[2]); i >0 ; i--) {
                numbers.add(numbers.size(), numbers.get(0));
                numbers.remove(0);
            }
            return numbers;
        }

    }

    static boolean indexChek(List<Integer> numbers, String[] command) {
        int indexChecker = 0;
        if(command[0].equals("Insert")){
            indexChecker = Integer.parseInt(command[2]);
        }else if (command[0].equals("Remove")){
            indexChecker = Integer.parseInt(command[1]);
        }
        if (indexChecker>numbers.size()-1 || indexChecker<0){
            System.out.println("Invalid index");
            return false;
        }else{
            return true;
        }
    }

    static void printResult(List<Integer> editedList) {
        for (Integer integer : editedList) {
            System.out.print(integer + " ");

        }
    }


}
