package day_11_lists;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class CardsGame {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        List<Integer> firstPlayerDeck = Arrays.stream(scan.nextLine().split("\\s+")).map(Integer::parseInt).collect(Collectors.toList());
        List<Integer> secondPlayerDeck = Arrays.stream(scan.nextLine().split("\\s+")).map(Integer::parseInt).collect(Collectors.toList());
        boolean winner = false;
        while(!winner){
            if(firstPlayerDeck.get(0).equals(secondPlayerDeck.get(0))){
                firstPlayerDeck.remove(0);
                secondPlayerDeck.remove(0);
            }else if (firstPlayerDeck.get(0)>secondPlayerDeck.get(0)){
                firstPlayerDeck.add(firstPlayerDeck.size(), firstPlayerDeck.get(0));
                firstPlayerDeck.add(firstPlayerDeck.size(), secondPlayerDeck.get(0));
                firstPlayerDeck.remove(0);
                secondPlayerDeck.remove(0);
            }else if (secondPlayerDeck.get(0)>firstPlayerDeck.get(0)){
                secondPlayerDeck.add(secondPlayerDeck.size(), secondPlayerDeck.get(0));
                secondPlayerDeck.add(secondPlayerDeck.size(), firstPlayerDeck.get(0));
                secondPlayerDeck.remove(0);
                firstPlayerDeck.remove(0);
            }

            int sum =0;
            if(firstPlayerDeck.size()==0){
                sum = calculateSumOfWinnersCards(secondPlayerDeck);
                System.out.printf("Second player wins! Sum: %d", sum);
                winner = true;
            }else if(secondPlayerDeck.size()==0){
                sum = calculateSumOfWinnersCards(firstPlayerDeck);
                System.out.printf("First player wins! Sum: %d", sum);
                winner = true;
            }
        }
    }

    static int calculateSumOfWinnersCards(List<Integer> playerCards) {
        int sum = 0;
        for (Integer playerCard : playerCards) {
            sum += playerCard;
        }
        return sum;
    }
}
