package day_11_lists.additional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Messaging {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        List<Integer> numbers = Arrays.stream(scan.nextLine().split("\\s+")).map(Integer::parseInt).collect(Collectors.toList());
        String message = scan.nextLine();

        List<String> decodedMessage = new ArrayList<>();

        while (numbers.size() > 0) {

            int currentIndex = findCurrentIndex(numbers, message);

            decodedMessage.add(String.valueOf(message.charAt(currentIndex)));
            message = removeLetterFromCurrentIndex(message, currentIndex);
            numbers.remove(0);

        }
        
        printResult(decodedMessage);
    }



    static int findCurrentIndex(List<Integer> numbers, String message) {
        String currentNumber = "";
        for (int i = 0; i < numbers.size(); i++) {
            currentNumber = numbers.get(i).toString();
            break;
        }

        int addedNumberForAnIndex = 0;
        for (int i = 0; i < currentNumber.length(); i++) {

            addedNumberForAnIndex += Character.getNumericValue(currentNumber.charAt(i));
        }

        if (addedNumberForAnIndex > message.length()) {
            addedNumberForAnIndex -= message.length();
        }
        return addedNumberForAnIndex;
    }

    static String removeLetterFromCurrentIndex(String message, int currentIndex) {
        StringBuilder removeLetter = new StringBuilder();
        for (int i = 0; i < message.length(); i++) {
            if(i != currentIndex){
                removeLetter.append(message.charAt(i));
            }
        }
        return removeLetter.toString();
    }


    static void printResult(List<String> decodedMessage) {
        System.out.println(String.join("", decodedMessage));
    }

    
}
