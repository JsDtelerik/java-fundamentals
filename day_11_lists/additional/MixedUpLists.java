package day_11_lists.additional;

import java.lang.reflect.Array;
import java.util.*;
import java.util.stream.Collectors;

public class MixedUpLists {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        List<Integer> firstList = Arrays.stream(scan.nextLine().split("\\s+")).map(Integer::parseInt).collect(Collectors.toList());
        List<Integer> secondList = Arrays.stream(scan.nextLine().split("\\s+")).map(Integer::parseInt).collect(Collectors.toList());
        List<Integer> listWithNumbers = returnsListWithallDigitsFromListOneAndListTwoWithoutRangeNumbers(firstList, secondList);
        List<Integer> range = findRangeOfNumbers(firstList, secondList);

        printRangeOfNumbers(listWithNumbers, range);
    }

    static List<Integer> returnsListWithallDigitsFromListOneAndListTwoWithoutRangeNumbers(List<Integer> firstList, List<Integer> secondList) {
        List<Integer> numbers = new ArrayList<>();
        label:
        for (int i = 0; i <= firstList.size()-1; i++) {
            if(firstList.isEmpty() || secondList.isEmpty()){
                break label;
            }
            numbers.add(firstList.get(i));
            for (int j = secondList.size()-1; j >= 0; j--) {
            numbers.add(secondList.get(j));
                firstList.remove(i);
                secondList.remove(j);
                i = -1;
                break;
            }
        }
        return numbers;
    }

    static List<Integer> findRangeOfNumbers(List<Integer> firstList, List<Integer> secondList) {
        List<Integer> lastTwo = new ArrayList<>();
        if(firstList.size()>secondList.size()){
            if(firstList.get(firstList.size()-1)>firstList.get(firstList.size()-2)){
                lastTwo.add(firstList.get(firstList.size()-2));
                lastTwo.add(firstList.get(firstList.size()-1));
            }else{
                lastTwo.add(firstList.get(firstList.size()-1));
                lastTwo.add(firstList.get(firstList.size()-2));
            }
        }else{
            if(secondList.get(secondList.size()-1)>secondList.get(secondList.size()-2)){
                lastTwo.add(secondList.get(secondList.size()-2));
                lastTwo.add(secondList.get(secondList.size()-1));
            }else{
                lastTwo.add(secondList.get(secondList.size()-1));
                lastTwo.add(secondList.get(secondList.size()-2));

            }
        }
        return lastTwo;
    }

    static void printRangeOfNumbers(List<Integer> listWithNumbers, List<Integer> range) {
        List<Integer> numbers = new ArrayList<>();
        for (int i = 0; i < listWithNumbers.size(); i++) {

            if(listWithNumbers.get(i)>range.get(0) && listWithNumbers.get(i)<range.get(1)){
                numbers.add(listWithNumbers.get(i));
            }
        }

        Collections.sort(numbers);

        for (Integer number : numbers) {
            System.out.print(number + " ");

        }
    }

}
