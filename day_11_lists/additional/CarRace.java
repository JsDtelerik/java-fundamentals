package day_11_lists.additional;

import java.util.Arrays;
import java.util.Scanner;

public class CarRace {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int[] raceLine = Arrays.stream(scan.nextLine().split("\\s+")).mapToInt(Integer::parseInt).toArray();

        double leftCarTIme = 0.0;
        for (int i = 0; i < raceLine.length/2; i++) {
            if(raceLine[i] != 0){
                leftCarTIme += raceLine[i];
            }else{
                leftCarTIme *=0.8;
            }
        }
        double rightCarTime = 0.0;
        for (int i = raceLine.length-1; i > raceLine.length/2; i--) {
            if(raceLine[i] != 0){
                rightCarTime += raceLine[i];
            }else{
                rightCarTime *=0.8;
            }
        }

        if(leftCarTIme<rightCarTime){
            System.out.printf("The winner is left with total time: %.01f", leftCarTIme);
        }else{
            System.out.printf("The winner is right with total time: %.01f", rightCarTime);
        }

    }
}
