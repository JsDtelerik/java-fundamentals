package day_11_lists;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class PokemonDontGo {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        List<Integer> numbers = Arrays.stream(scan.nextLine().split("\\s+")).map(Integer::parseInt).collect(Collectors.toList());
        List<Integer> removedNumbers = new ArrayList<>();
        while (numbers.size()!=0){
            int n = Integer.parseInt(scan.nextLine());
            int chekIndex = returnValidIndexOrN(numbers, n);
           List<Integer> keepRemovedNumbers = removeValueOfNumber(removedNumbers, numbers, chekIndex);
            numbers = increaseDecreaseListOfNumbers(numbers, n, chekIndex);

        }

        print(removedNumbers);
    }




    static int returnValidIndexOrN(List<Integer> numbers, int n) {
        if(n<0){
            return 0;
        }else if(n>numbers.size()-1){
            return numbers.size()-1;
        }else{
            return n;
        }
    }


    static List<Integer> removeValueOfNumber(List<Integer> removedNumbers, List<Integer> numbers, int n) {

        removedNumbers.add(numbers.get(n));
        return removedNumbers;
    }


    static List<Integer> increaseDecreaseListOfNumbers(List<Integer> numbers, int n, int checkIndex) {

        int keepValueOfN = numbers.get(checkIndex);

        if(n<0){
            numbers.set(0, numbers.get(numbers.size()-1));
        }else if (n>numbers.size()-1){
            numbers.set(numbers.size()-1, numbers.get(0));
        }else{
            numbers.remove(n);
        }

        for (int i = 0; i < numbers.size(); i++) {
            if(numbers.get(i)<=keepValueOfN){
                numbers.set(i, numbers.get(i)+keepValueOfN);
            }else if (numbers.get(i)>keepValueOfN){
                numbers.set(i, numbers.get(i)-keepValueOfN);
            }
        }


        return numbers;
    }

    static void print(List<Integer> removedNumbers) {
        int sum =0;
        for (Integer removedNumber : removedNumbers) {
            sum += removedNumber;

        }
        System.out.println(sum);
    }
}

