package day_15_associativeArrays;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class SoftUniParking {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        Map<String, String> visitors = new LinkedHashMap<>();
        int n = Integer.parseInt(scan.nextLine());

        for (int i = 1; i <= n; i++) {
            String[] command = scan.nextLine().split("\\s+");

            switch (command[0]){
                case "register":
                visitors = checkVisitorsAndPrintAddedOnesAndThoseWhoExists(visitors, command[1], command[2]);
                    break;
                case "unregister":
                    visitors = checkVisitorsAndRemoveThemFromTheListIfVisitorExist(visitors, command[1]);
                    break;
            }

        }
        for (Map.Entry<String, String> stringStringEntry : visitors.entrySet()) {
            System.out.printf("%s => %s%n", stringStringEntry.getKey(), stringStringEntry.getValue());

        }

    }

    static Map<String, String> checkVisitorsAndPrintAddedOnesAndThoseWhoExists(Map<String, String> visitors, String userName, String licencePlate) {
        if(!visitors.containsKey(userName)){
            visitors.put(userName, licencePlate);
            System.out.printf("%s registered %s successfully%n", userName, licencePlate);

        }else{
            System.out.printf("ERROR: already registered with plate number %s%n", licencePlate);

        }
        return visitors;
    }

    static Map<String, String> checkVisitorsAndRemoveThemFromTheListIfVisitorExist(Map<String, String> visitors, String userName) {

        if(visitors.containsKey(userName)){
            visitors.remove(userName);
            System.out.printf("%s unregistered successfully%n", userName);
        }else{
            System.out.printf("ERROR: user %s not found%n", userName);
        }
        return visitors;
    }
}
