package day_15_associativeArrays.additional;

import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;
import java.util.stream.Collectors;

public class Pirates {
    static class City{
        private String name;
        private Integer population;
        private Integer gold;

        public City(String name, Integer population, Integer gold){
            this.name = name;
            this.population = population;
            this.gold =gold;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Integer getPopulation() {
            return population;
        }

        public void setPopulation(Integer population) {
            this.population = population;
        }

        public Integer getGold() {
            return gold;
        }

        public void setGold(Integer gold) {
            this.gold = gold;
        }

        @Override
        public String toString() {
            return String.format("%s -> Population: %d citizens, Gold: %d kg%n", getName(), getPopulation(), getGold());
        }
    }

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String[] command = scan.nextLine().split("\\|\\|");
        Map<String, City> locations = new TreeMap<>();
        while (!command[0].equals("Sail")){
            if(locations.containsKey(command[0])){
               locations.get(command[0]).setPopulation(locations.get(command[0]).getPopulation()+Integer.parseInt(command[1]));
               locations.get(command[0]).setGold(locations.get(command[0]).getGold()+Integer.parseInt(command[2]));
            }else{
                City city = new City(command[0], Integer.parseInt(command[1]), Integer.parseInt(command[2]));
                locations.put(command[0], city);
            }
            command = scan.nextLine().split("\\|\\|");
        }

        String[] events = scan.nextLine().split("=>");
        while (!events[0].equals("End")){
            switch (events[0]){
                case "Plunder":
                    locations = plunderLocation(locations, events[1], Integer.parseInt(events[2]), Integer.parseInt(events[3]));
                    break;
                case "Prosper":
                    locations = increasingTownTreasury(locations, events[1], Integer.parseInt(events[2]));
                    break;
            }


            events = scan.nextLine().split("=>");
        }

        System.out.printf("Ahoy, Captain! There are %d wealthy settlements to go to:%n", locations.size());
        if(locations.isEmpty()){
            System.out.printf("Ahoy, Captain! All targets have been plundered and destroyed!");

        }else{
            locations.entrySet().stream().sorted((a, b) -> b.getValue().getGold().compareTo(a.getValue().getGold()))
                    .forEach(town ->{
                        System.out.print(town.getValue());
                    });

        }




    }



    static Map<String, City> plunderLocation(Map<String, City> locations, String town, int people, int stolenGold) {
        locations.get(town).setPopulation(locations.get(town).getPopulation()-people);
        locations.get(town).setGold(locations.get(town).getGold()-stolenGold);
        System.out.printf("%s plundered! %d gold stolen, %d citizens killed.%n", town, stolenGold, people);

        if (locations.get(town).getPopulation() <= 0 || locations.get(town).getGold()<=0){
            System.out.printf("%s has been wiped off the map!%n", town);
            locations.remove(town);
        }
    return locations;
    }
    static Map<String, City> increasingTownTreasury(Map<String, City> locations, String town, int incomesInGold) {
        if(incomesInGold<0){
            System.out.printf("Gold added cannot be a negative number!%n");
            return locations;
        }else{
            locations.get(town).setGold(locations.get(town).getGold()+incomesInGold);
            System.out.printf("%d gold added to the city treasury. %s now has %d gold.%n", incomesInGold, town, locations.get(town).getGold());
        }
        return locations;
    }


}
