package day_15_associativeArrays.additional;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Ranking {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        Map<String, String> content = new LinkedHashMap<>();
        String[] commandOne = scan.nextLine().split(":");
        while (!commandOne[0].equals("end of contests")){
            content.put(commandOne[0], commandOne[1]);
            commandOne = scan.nextLine().split(":");
        }
        Map<String, Integer> mapInMap = new LinkedHashMap<>();
        Map<String, Map<String, Integer>> usersAchievement = new LinkedHashMap<>();

        String[] commandTwo = scan.nextLine().split("=>");

        while (!commandTwo[0].equals("end of submissions")){
            if(chekIfContentAndPasswordAreCorrect(commandTwo, content)){
                if(!usersAchievement.containsKey(commandTwo[0])){
                    mapInMap.put(commandTwo[2], Integer.parseInt(commandTwo[3]));
                    usersAchievement.put(commandTwo[0], mapInMap);
                }else if(usersAchievement.containsKey(commandTwo[2])){
                    if(mapInMap.containsKey(commandTwo[2]) && mapInMap.get(commandTwo[2])<Integer.parseInt(commandTwo[3]));
                    mapInMap.put(commandTwo[2], Integer.parseInt(commandTwo[3]));
                    usersAchievement.put(commandTwo[0], mapInMap);
                }
            }
            commandTwo = scan.nextLine().split("=>");
        }
        usersAchievement.entrySet().stream().collect(Collectors.toList());
    }

    static boolean chekIfContentAndPasswordAreCorrect(String[] commandTwo, Map<String, String> content) {
        if(content.containsKey(commandTwo[0])){
            if(content.get(commandTwo[0]).equals(commandTwo[1])){
                return true;
            }
        }
        return false;
    }
}
