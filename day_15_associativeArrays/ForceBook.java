package day_15_associativeArrays;

import java.util.*;

public class ForceBook {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        Map<String, List<String>> forceBook = new LinkedHashMap<>();
        String input = scan.nextLine();

        while (!input.equals("Lumpawaroo")){
           String sign = returnDelimiter(input);

            switch (sign){
                case "|":
                    String[] command = input.split(" \\| ");
                    forceBook = addNewMemberToGivenSide(forceBook, command);
                    break;
                case "->":
                    String[] command2 = input.split(" -> ");
                    forceBook = changeSideOfTheMember(forceBook,command2);

                    break;
            }

            input = scan.nextLine();
        }

        forceBook.entrySet().stream().sorted(Comparator.comparing(Map.Entry::getKey)).sorted((e1, e2) -> Integer.compare(e2.getValue().size(), e1.getValue().size()))
                .filter(e -> e.getValue().size()>0)
                .forEach(e -> {
                    System.out.printf("Side: %s, Members: %d%n", e.getKey(), e.getValue().size());
                    e.getValue().stream().sorted(String::compareTo).forEach(user -> System.out.println("! "+user));
                });

    }

    static String returnDelimiter(String input) {
        StringBuilder sign = new StringBuilder();

        for (int i = 0; i < input.length(); i++) {
            char symbol = input.charAt(i);
            if(symbol == 124){
                sign.append(symbol);

            }else if(symbol == 45 || symbol == 62){
                sign.append(symbol);
            }
        }
        return sign.toString();
    }


    static Map<String, List<String>> addNewMemberToGivenSide(Map<String, List<String>> forceBook, String[] command) {

        for (Map.Entry<String, List<String>> stringListEntry : forceBook.entrySet()) {
            if(stringListEntry.getValue().contains(command[1])){
                return forceBook;
            }
        }
        if(!forceBook.containsKey(command[0])){
            forceBook.put(command[0], new ArrayList<>());
            if(!forceBook.get(command[0]).contains(command[1])){
                forceBook.get(command[0]).add(command[1]);
            }
        }else{
            forceBook.get(command[0]).add(command[1]);
        }
        return forceBook;
    }

    static Map<String, List<String>> changeSideOfTheMember(Map<String, List<String>> forceBook, String[] command) {
        if (!forceBook.containsKey(command[1])) {
            forceBook.put(command[1], new ArrayList<>());
            forceBook.get(command[1]).add(command[0]);
        }

            if(!forceBook.get(command[1]).contains(command[0])){
                forceBook.get(command[1]).add(command[0]);
            }
        for (Map.Entry<String, List<String>> stringListEntry : forceBook.entrySet()) {
            if(!stringListEntry.getKey().contains(command[1])){
                if(stringListEntry.getValue().contains(command[0])){
                    forceBook.get(stringListEntry.getKey()).remove(command[0]);
                }
            }

        }

        System.out.printf("%s joins the %s side!%n", command[0], command[1]);
            return forceBook;
    }

}
