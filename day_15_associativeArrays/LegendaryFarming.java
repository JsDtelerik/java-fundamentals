package day_15_associativeArrays;

import java.util.*;
import java.util.stream.Collectors;


public class LegendaryFarming {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        Map<String, Integer> collectedMaterial = new TreeMap<>();

        collectedMaterial.put("shards", 0);
        collectedMaterial.put("fragments", 0);
        collectedMaterial.put("motes", 0);

        boolean isFound = false;
        do {
            List<String> items = Arrays.stream(scan.nextLine().split("\\s+")).collect(Collectors.toList());
            for (int i = 0; i < items.size(); i++) {
                int countOfMaterial = Integer.parseInt(items.get(0));
                String material = items.get(1).toLowerCase();
                if (!collectedMaterial.containsKey(material)) {
                    collectedMaterial.put(material, countOfMaterial);
                } else {
                    collectedMaterial.put(material, collectedMaterial.get(material) + countOfMaterial);
                    if (collectedMaterial.get("motes") >= 250 || collectedMaterial.get("shards") >= 250 || collectedMaterial.get("fragments") >= 250) {
                        isFound = true;
                        break;
                    }
                }
                items.remove(0);
                items.remove(0);
                i = -1;
            }

        } while (!isFound);

        if(collectedMaterial.get("shards")>=250){
            System.out.println("Shadowmourne obtained!");
            collectedMaterial.put("shards", collectedMaterial.get("shards")-250);
        }else if (collectedMaterial.get("fragments")>=250){
            System.out.println("Valanyr obtained!");
            collectedMaterial.put("fragments", collectedMaterial.get("fragments")-250);
        }else if(collectedMaterial.get("motes")>=250){
            System.out.println("Dragonwrath obtained!");
            collectedMaterial.put("motes", collectedMaterial.get("motes")-250);
        }
        Map<String, Integer> sortedMaterial = collectedMaterial.entrySet().stream()
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));


        for (Map.Entry<String, Integer> stringIntegerEntry : sortedMaterial.entrySet()) {
            if(stringIntegerEntry.getKey().equals("shards") || stringIntegerEntry.getKey().equals("motes") || stringIntegerEntry.getKey().equals("fragments")){
                System.out.printf("%s: %d%n", stringIntegerEntry.getKey(), stringIntegerEntry.getValue());

            }
        }
        sortedMaterial.remove("shards");
        sortedMaterial.remove("fragments");
        sortedMaterial.remove("motes");

       Map<String, Integer> sortByKey = sortedMaterial.entrySet().stream().sorted(Map.Entry.comparingByKey())
               .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));

        for (Map.Entry<String, Integer> stringIntegerEntry : sortByKey.entrySet()) {
            System.out.printf("%s: %d%n", stringIntegerEntry.getKey(), stringIntegerEntry.getValue());
        }
    }
}
