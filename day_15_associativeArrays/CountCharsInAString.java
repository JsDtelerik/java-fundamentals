package day_15_associativeArrays;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class CountCharsInAString {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String text = scan.nextLine();

        Map<Character, Integer> letterAppearance = new LinkedHashMap<>();

        for (int i = 0; i < text.length(); i++) {
            char symbol = text.charAt(i);
            if(symbol == 32){
                continue;
            }else{
                if(!letterAppearance.containsKey(symbol)){
                    letterAppearance.put(symbol, 1);
                }else{
                    letterAppearance.put(symbol, letterAppearance.get(symbol)+1);
                }
            }
        }
        for (Map.Entry<Character, Integer> characterIntegerEntry : letterAppearance.entrySet()) {
            System.out.println(characterIntegerEntry.getKey() + " -> " + characterIntegerEntry.getValue());
        }


    }
}
