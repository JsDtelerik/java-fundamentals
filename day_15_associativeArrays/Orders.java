package day_15_associativeArrays;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class Orders {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        Map<String, Double> productPrice = new LinkedHashMap<>();
        Map<String, Integer> productsQuantity = new LinkedHashMap<>();

        String[] command = scan.nextLine().split("\\s+");

        while (!command[0].equals("buy")){
            if(!productPrice.containsKey(command[0])){
                productPrice.put(command[0], Double.parseDouble(command[1]));
                productsQuantity.put((command[0]),Integer.parseInt(command[2]));
            }else{
                productPrice.put(command[0], Double.parseDouble(command[1]));
                productsQuantity.put(command[0], productsQuantity.get(command[0])+Integer.parseInt(command[2]));
            }
            command = scan.nextLine().split("\\s+");
        }


        for (Map.Entry<String, Integer> stringIntegerEntry : productsQuantity.entrySet()) {
            System.out.printf("%s -> %.02f%n", stringIntegerEntry.getKey(), stringIntegerEntry.getValue()*productPrice.get(stringIntegerEntry.getKey()));


        }

    }
}
