package day_15_associativeArrays;

import java.util.*;
import java.util.stream.Collectors;

public class SoftUniExamResults {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        Map<String, Integer> usersPoint = new LinkedHashMap<>();
        List<String> language = new ArrayList<>();

        String[] command = scan.nextLine().split("-");

        while (!command[0].equals("exam finished")) {
            if (command[1].equals("banned") && usersPoint.containsKey(command[0])) {
                usersPoint.remove(command[0]);
            }
            if (usersPoint.containsKey(command[0])) {
                if (usersPoint.get(command[0]) < Integer.parseInt(command[2])) {
                    usersPoint.put(command[0], Integer.parseInt(command[2]));
                }
                language.add(command[1]);
            } else if (!usersPoint.containsKey(command[0]) && !command[1].equals("banned")) {
                usersPoint.put(command[0], Integer.parseInt(command[2]));
                language.add(command[1]);
            }

            command = scan.nextLine().split("-");
        }
        System.out.println("Results:");
        usersPoint.entrySet().stream().sorted(Comparator.comparing(Map.Entry::getKey))
                .sorted((e1, e2) -> Integer.compare(e2.getValue(), e1.getValue()))
                .forEach(e -> {
                    System.out.printf("%s | %d%n", e.getKey(), e.getValue());
                });

        System.out.println("Submissions:");
        List<String> listedCollection = language.stream().sorted(String::compareTo).collect(Collectors.toList());

        for (int i = 0; i < listedCollection.size(); i++) {
            if (i == 0) {
                System.out.printf("%s - %d%n", listedCollection.get(i), Collections.frequency(listedCollection, listedCollection.get(i)));
            } else if (!listedCollection.get(i).equals(listedCollection.get(i - 1))) {
                System.out.printf("%s - %d%n", listedCollection.get(i), Collections.frequency(listedCollection, listedCollection.get(i)));
            }
        }
    }
}

