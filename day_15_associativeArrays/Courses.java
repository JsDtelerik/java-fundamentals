package day_15_associativeArrays;

import java.util.*;

public class Courses {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        Map<String, List<String>> courses = new LinkedHashMap<>();
        String[] command = scan.nextLine().split(" : ");

        while (!command[0].equals("end")){

            if(!courses.containsKey(command[0])){
                courses.put(command[0], new ArrayList<>());
            }
            courses.get(command[0]).add(command[1]);
            command = scan.nextLine().split(" : ");
        }

        courses.entrySet().stream().sorted((e1, e2) -> Integer.compare(e2.getValue().size(), e1.getValue().size()))
                .forEach( e -> {
                    System.out.println(e.getKey() + ": " + e.getValue().size());
                    e.getValue().stream().sorted(String::compareTo).forEach(student -> System.out.println("-- "+ student));
                });
    }
}
