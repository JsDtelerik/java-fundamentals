package day_15_associativeArrays;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class StudentAcademy {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        Map<String, List<Double>> studentsDatabase = new LinkedHashMap<>();

        int n = Integer.parseInt(scan.nextLine());

        for (int i = 1; i <= n; i++) {
            String studentName = scan.nextLine();
            double studentGrade = Double.parseDouble(scan.nextLine());

            if (!studentsDatabase.containsKey(studentName)) {
                studentsDatabase.put(studentName, new ArrayList<>());
            }
            studentsDatabase.get(studentName).add(studentGrade);

        }
        Map<String, Double> studentsAverageScore = new LinkedHashMap<>();
        for (Map.Entry<String, List<Double>> stringListEntry : studentsDatabase.entrySet()) {
            double averageScorePerStudent = 0.0;
            int count = 0;
            for (int i = 0; i < stringListEntry.getValue().size(); i++) {
                averageScorePerStudent += stringListEntry.getValue().get(i);
                count++;
            }
            studentsAverageScore.put(stringListEntry.getKey(), averageScorePerStudent/count);

        }

        studentsAverageScore.entrySet().stream().filter(e -> e.getValue()>= 4.50).sorted((e1, e2) -> Double.compare(e2.getValue(), e1.getValue()))
                .forEach(e ->{
                    System.out.printf("%s -> %.02f%n", e.getKey(), e.getValue());
                } );

    }


}

