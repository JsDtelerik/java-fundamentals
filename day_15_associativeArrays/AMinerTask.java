package day_15_associativeArrays;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class AMinerTask {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        Map<String, Integer> resources = new LinkedHashMap<>();

        String mainCommandLine = scan.nextLine();
        while (!mainCommandLine.equals("stop")){
            int quantityResource = Integer.parseInt(scan.nextLine());
            if(!resources.containsKey(mainCommandLine)){
                resources.put(mainCommandLine, quantityResource);
            }else{
                resources.put(mainCommandLine, resources.get(mainCommandLine)+quantityResource);
            }

            mainCommandLine = scan.nextLine();
        }
        for (Map.Entry<String, Integer> stringIntegerEntry : resources.entrySet()) {
            System.out.printf("%s -> %d%n", stringIntegerEntry.getKey(), stringIntegerEntry.getValue());
        }
    }
}
