package day_15_associativeArrays;

import java.util.*;

public class CompanyUsers {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        Map<String, List<String>> companies = new LinkedHashMap<>();

        String[] command = scan.nextLine().split(" -> ");

        while (!command[0].equalsIgnoreCase("End")){

            if(!companies.containsKey(command[0])){
                companies.put(command[0], new ArrayList<>());
            }
            if(!companies.get(command[0]).contains(command[1])){
                companies.get(command[0]).add(command[1]);
            }

            command = scan.nextLine().split(" -> ");
        }

        companies.entrySet().stream().sorted(Map.Entry.comparingByKey()).forEach(e -> {
            System.out.println(e.getKey());
        e.getValue().forEach(company -> System.out.println("-- "+ company));
        });

    }
}
