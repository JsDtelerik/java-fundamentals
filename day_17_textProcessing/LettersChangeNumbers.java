package day_17_textProcessing;

import java.util.Scanner;

public class LettersChangeNumbers {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        String[] input = scan.nextLine().split("\\s+");
        double totalSum = 0.0;

        for (int i = 0; i < input.length; i++) {
            String subInput = input[i];
            char firstLetter = subInput.charAt(0);
            char lastLetter = subInput.charAt(subInput.length()-1);
            double number = Double.parseDouble(subInput.substring(1, subInput.length()-1));

            if(Character.isLowerCase(firstLetter)){
                int letterPosition = firstLetter-96;
                number *= letterPosition;
            }else {
                int letterPosition = firstLetter-64;
                number /= letterPosition;
            }


            if(Character.isLowerCase(lastLetter)){
                int letterPosition = lastLetter-96;
                number += letterPosition;
            }else{
                int letterPosition = lastLetter-64;
                number -= letterPosition;
            }

            totalSum += number;
        }

        System.out.printf("%.02f", totalSum);
    }
}
