package day_17_textProcessing;

import java.util.Scanner;

public class ValidUsernames {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        String[] names = scan.nextLine().split(", ");

        for (String name : names) {
            if(nameIsValid(name)){
                System.out.println(name);
            }

        }
    }

    static boolean nameIsValid(String name) {
        boolean nameIsValid = false;

        if(name.length()>=3 && name.length()<= 16){
            nameIsValid = true;
        }

        if(nameIsValid){
            for (int i = 0; i < name.length(); i++) {
                char symbol = name.charAt(i);
                if(!Character.isAlphabetic(symbol) && !Character.isDigit(symbol) && symbol != '-' && symbol != '_'  ){
                    nameIsValid = false;
                    return nameIsValid;
                }
            }
        }
        return nameIsValid;
    }
}
