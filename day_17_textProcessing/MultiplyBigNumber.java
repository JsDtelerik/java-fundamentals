package day_17_textProcessing;

import java.math.BigInteger;
import java.util.Scanner;

public class MultiplyBigNumber {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

     /*BigInteger n1 = new BigInteger(scan.nextLine());
        BigInteger n2 = new BigInteger(scan.nextLine());

        System.out.println(n1.multiply(n2));*/

     String n1 = scan.nextLine();
        int n2 = Integer.parseInt(scan.nextLine());
        int keepOverage = 0;
        StringBuilder addAllDigits = new StringBuilder();
        for (int i = n1.length()-1; i >=0 ; i--) {
            int currentNumber = Integer.parseInt(String.valueOf(n1.charAt(i)));
            int currentCalculation = (currentNumber*n2);
            if(keepOverage>0){
                currentCalculation+=keepOverage;
                keepOverage=0;
            }

            if(currentCalculation>9){
                int recalculateCurrResult = currentCalculation % 10;
                keepOverage = currentCalculation/10;
                addAllDigits.append(recalculateCurrResult);
                if(i == 0){
                    addAllDigits.append(keepOverage);
                    keepOverage = 0;
                }
            }else{
                addAllDigits.append(currentCalculation);
            }


        }

        addAllDigits.reverse().toString();
        StringBuilder digitsFromNullTo = new StringBuilder();
        if(addAllDigits.charAt(0) == '0'){
            boolean firstPositive = false;
            for (int i = 0; i < addAllDigits.length(); i++) {
                if(addAllDigits.charAt(i) != '0'){
                    digitsFromNullTo.append(Integer.parseInt(String.valueOf(addAllDigits.charAt(i))));
                        firstPositive = true;
                }else if (firstPositive){
                    digitsFromNullTo.append(Integer.parseInt(String.valueOf(addAllDigits.charAt(i))));
                }
            }
            if(digitsFromNullTo.length()<=0) {
                digitsFromNullTo.append(0);
            }
        }


        if (digitsFromNullTo.length()>0) {
            System.out.println(digitsFromNullTo);

        } else {
            System.out.println(addAllDigits);
        }


    }
}
