package day_17_textProcessing;

import java.util.Scanner;

public class StringExplosion {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        String text = scan.nextLine();
        int leftPower = 0;
        for (int i = 0; i < text.length(); i++) {
            if (Character.isDigit(text.charAt(i))) {
                int lastIndex = Integer.parseInt(String.valueOf(text.charAt(i)));
                int remove = returnsValueFromText(text, lastIndex, i, leftPower) + i;
                if(remove-i>=leftPower){
                    leftPower = 0;
                }

                String substring = text.substring(0, i);
                if(remove>text.length()-1){
                    text = substring;
                }else{
                    text = substring + text.substring(remove);
                }

                if(remove<i+lastIndex){
                    //leftPower = 0;
                    leftPower += (i+lastIndex)-remove;
                }
                i = -1;

            }

        }
        System.out.println(text);
    }

    static int returnsValueFromText(String text, int lastIndex, int currentIndex, int leftPower) {
        for (int i = 1; i <= lastIndex; i++) {
            if(text.length()-1>currentIndex){
                if (text.charAt(++currentIndex) == '>') {
                    return i;
                }
            }
        }
    if(lastIndex == 0){
         return 1+leftPower;
    }else{
        return lastIndex+leftPower;
    }


    }
}
