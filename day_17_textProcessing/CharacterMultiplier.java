package day_17_textProcessing;

import java.util.Scanner;

public class CharacterMultiplier {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        String[] input = scan.nextLine().split("\\s+");
        String firstString = input[0];
        String secondString = input[1];
        int longestSting = returnsLongerStringLength(firstString, secondString);
        int sum = 0;
        for (int i = 0; i < longestSting; i++) {
            if(firstString.length()-1>=i && secondString.length()-1>=i){
                sum += firstString.charAt(i)*secondString.charAt(i);
            }else if(firstString.length()-1>=i){
                sum += firstString.charAt(i);
            }else if(secondString.length()-1>=i){
                sum += secondString.charAt(i);
            }

        }
        System.out.println(sum);

    }

    static int returnsLongerStringLength(String firstString, String secondString) {
        if(firstString.length()>=secondString.length()){
            return firstString.length();
        }else{
            return secondString.length();
        }

    }
}
