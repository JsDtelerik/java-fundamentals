package day_17_textProcessing;

import java.util.Scanner;

public class CaesarCipher {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String message = scan.nextLine();
        StringBuilder encryptedMessage = new StringBuilder();

        for (int i = 0; i < message.length(); i++) {
            char symbol = message.charAt(i);
            encryptedMessage.append((char)(symbol+3));
        }
        System.out.println(encryptedMessage);
    }
}
