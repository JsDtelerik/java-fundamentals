package day_17_textProcessing;

import java.util.Scanner;

public class ReplaceRepeatingChars {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        String text = scan.nextLine();

        StringBuilder editedText = new StringBuilder();

        for (int i = 0; i < text.length(); i++) {
            if(text.length()-1 == i){
                editedText.append(text.charAt(i));
            }else{
                if(text.charAt(i) != text.charAt(i+1)){
                    editedText.append(text.charAt(i));
                }
            }

        }
        System.out.println(editedText);
    }
}
