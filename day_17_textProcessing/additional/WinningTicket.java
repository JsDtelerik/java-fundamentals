package day_17_textProcessing.additional;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class WinningTicket {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        List<String> tickets = Arrays.stream(scan.nextLine().split("\\s*,\\s*")).collect(Collectors.toList());

        for (int i = 0; i < tickets.size(); i++) {

            if(!checkTicketLength(tickets.get(i))){
                System.out.println("invalid ticket");
               tickets.remove(i);
                i = -1;
                continue;
            }else {
                char quotes = '"';
                char specialSymbol = getSpecialSymbolFromTheTicket(tickets.get(i));
                int sequence = winningSequence(tickets.get(i), specialSymbol);
                if (!checkTicketContent(tickets.get(i), specialSymbol)) {
                    if (sequence < 6) {
                        System.out.printf("ticket %c%s%c - no match%n", quotes, tickets.get(i), quotes);
                        tickets.remove(i);
                        i = i-1;
                        continue;
                    }
                }
                    if (sequence >= 6) {
                        String winningTicket = tickets.get(i);

                        if (sequence==10) {
                            System.out.printf("ticket %c%s%c - %d%c Jackpot!%n", quotes, winningTicket, quotes, sequence, specialSymbol);
                        } else {
                            System.out.printf("ticket %c%s%c - %d%c%n", quotes, winningTicket, quotes, sequence, specialSymbol);
                        }
                    }
                }
                tickets.remove(i);
                i = -1;
            }


    }

     static Integer winningSequence(String ticket, char specialSymbol) {
         int countLeft = 0;

        int startAt = 0;

        while(startAt <ticket.length()/2){
            int currentLeftCombination = 0;

         for (int i = startAt; i < ticket.length()/2; i++) {
             if(ticket.charAt(i)==specialSymbol){
                 currentLeftCombination++;
             }else if(ticket.charAt(i) !=specialSymbol){
                 i++;
                 startAt = i;

                 break;
             }
                    if(i == (ticket.length()/2)-1){
                        i++;
                        startAt = i;
                    }
             }
            if(countLeft<=currentLeftCombination){
                countLeft = currentLeftCombination;

            }
                 }

         int countRight = 0;
            int secondStartAt = ticket.length()/2;
         while(secondStartAt < ticket.length()){
             int currentLeftCombination = 0;

             for (int i = secondStartAt; i < ticket.length(); i++) {
                 if(ticket.charAt(i)==specialSymbol){
                     currentLeftCombination++;
                 }else if(ticket.charAt(i) !=specialSymbol){
                     i++;
                     secondStartAt = i;

                     break;
                 }
                 if(i == ticket.length()-1){
                     i++;
                     secondStartAt = i;
                 }
             }
             if(countRight<=currentLeftCombination){
                 countRight = currentLeftCombination;
             }
         }

         if (countLeft >= 6 && countRight >= 6) {
             if (countLeft <= countRight) {
                 return countLeft;
             } else{

                 return countRight;
             }
         }
         return 0;
     }

    static boolean checkTicketLength(String ticket) {
        if(ticket.length() != 20){
            return false;
        }
        return true;
    }

    static boolean checkTicketContent(String ticket, char specialSymbol) {

        int countLeft = 0;
        int countRight = 0;

        for (int i = 0; i < ticket.length()/2; i++) {
            char symbol =ticket.charAt(i);
            if(symbol == specialSymbol ){
                if(i>0){
                    if(ticket.charAt(i-1) == specialSymbol){

                        countLeft++;
                    }
                }else{

                    countLeft++;
                }
            }
        }

        for (int i = ticket.length()/2; i < ticket.length() ; i++) {
            char symbol =ticket.charAt(i);
            if(symbol == specialSymbol ){
                if(i>ticket.length()/2){
                    if(ticket.charAt(i-1) == specialSymbol){

                        countRight++;
                    }
                }else{

                    countRight++;
                }
            }
        }

            if (countLeft == countRight && countLeft >= 6 && countRight >= 6){
                return true;
            }

        return false;
        }


    private static char getSpecialSymbolFromTheTicket(String ticket) {
        char keepChar = ' ';
        int countWinningChar = 0;
        for (int i = 0; i < ticket.length(); i++) {
            char symbol = ticket.charAt(i);
            if(symbol == '@' || symbol == '#' || symbol == '$' || symbol == '^'){

                if(keepChar != symbol && countWinningChar<6){
                    keepChar = symbol;
                    countWinningChar=0;
                }
                countWinningChar++;

            }
        }
        return keepChar;
    }
}
