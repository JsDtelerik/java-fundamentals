package day_17_textProcessing.additional;

import java.util.Scanner;

public class AsciiSumator {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        char firstChar = scan.nextLine().charAt(0);
        char secondChar = scan.nextLine().charAt(0);
        String mainInput = scan.nextLine();
        int lowerRange = getLowerRange(firstChar, secondChar);
        int higherRange = getHigherRange(firstChar, secondChar);
        int sum = 0;
        for (int i = 0; i < mainInput.length(); i++) {
            char symbol = mainInput.charAt(i);
            if(symbol>lowerRange && symbol<higherRange){
                sum +=symbol;
            }
        }
        System.out.println(sum);
    }

    static int getHigherRange(char first, char second) {
        if(first>=second){
            return first;
        }else{
            return second;
        }
    }

    static int getLowerRange(char firstChar, char secondChar) {
        if(firstChar <=secondChar){
            return firstChar;
        }else {
            return secondChar;
        }

    }
}
