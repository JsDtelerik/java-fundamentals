package day_17_textProcessing.additional;

import java.util.Scanner;

public class MorseCodeTranslator {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String[] message = scan.nextLine().split("\\s+");


        char[] english = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l',
                'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x',
                'y', 'z', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0',
                ',', '.', '?' };
        String[] morse = { ".-", "-...", "-.-.", "-..", ".", "..-.", "--.", "....", "..",
                ".---", "-.-", ".-..", "--", "-.", "---", ".--.", "--.-", ".-.",
                "...", "-", "..-", "...-", ".--", "-..-", "-.--", "--..", ".----",
                "..---", "...--", "....-", ".....", "-....", "--...", "---..", "----.",
                "-----", "--..--", ".-.-.-", "..--.." };
        StringBuilder decryptedMessage = new StringBuilder();
        for (int i = 0; i < message.length; i++) {
            String symbol = message[i];
            if(symbol.equals("|")){
                decryptedMessage.append(" ");
            }else{
                for (int j = 0; j < morse.length; j++) {
                    if(morse[j].equals(symbol)){
                        decryptedMessage.append(english[j]);
                        break;
                    }

                }
            }


        }
        System.out.println(decryptedMessage.toString().toUpperCase());
    }
}
