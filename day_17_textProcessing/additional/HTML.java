package day_17_textProcessing.additional;

import java.util.Scanner;

public class HTML {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String titleOfArticle = scan.nextLine();
        System.out.printf("<h1>%n" +
                "   %s%n" +
                "</h1>%n", titleOfArticle);
        String contentOfTheArticle = scan.nextLine();
        System.out.printf("<article>%n" +
                "   %s%n" +
                "</article>%n", contentOfTheArticle);
        String input = scan.nextLine();
        while (!input.equals("end of comments")){
            System.out.printf("<div>%n" +
                    "   %s%n" +
                    "</div>%n", input);

            input = scan.nextLine();
        }

    }
}
