package day_17_textProcessing.additional;

import java.util.Arrays;
import java.util.Scanner;

public class TreasureFinder {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int[] numbers = Arrays.stream(scan.nextLine().split("\\s+")).mapToInt(Integer::parseInt).toArray();

        String input = scan.nextLine();
        while (!input.equals("find")){
            StringBuilder currentMessage = new StringBuilder();
            int indexForNumbers = 0;
            for (int i = 0; i < input.length(); i++) {
                char symbol = input.charAt(i);
                int decreaseNum = numbers[indexForNumbers];
                currentMessage.append((char)(symbol-decreaseNum));
                if(numbers.length-1 == indexForNumbers && input.length()-1 >= i){
                    indexForNumbers = 0;
                    continue;
                }
                indexForNumbers++;
            }
             currentMessage.toString();
            String treasure = currentMessage.substring(currentMessage.indexOf("&")+1, currentMessage.lastIndexOf("&"));
            String coordinates = currentMessage.substring(currentMessage.indexOf("<")+1, currentMessage.indexOf(">"));
            System.out.printf("Found %s at %s%n", treasure, coordinates);



            input = scan.nextLine();
        }
    }
}
