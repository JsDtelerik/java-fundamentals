package day_17_textProcessing.additional;

import java.util.Scanner;

public class ExtractPersonInformation {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int n = Integer.parseInt(scan.nextLine());

        for (int i = 1; i <= n; i++) {
            String input = scan.nextLine();
            String name = input.substring(input.indexOf('@')+1, input.indexOf('|'));
            int age = Integer.parseInt(input.substring(input.indexOf('#')+1, input.indexOf('*')));
            System.out.printf("%s is %d years old.%n", name, age);

        }
    }
}
