package day_17_textProcessing;

import java.util.Scanner;

public class ExtractFile {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        String[] directory = scan.nextLine().split("\\\\");
        String fileNameAndExtension = directory[directory.length-1];
        int getIndexOfDot = fileNameAndExtension.indexOf(".");

        System.out.printf("File name: %s%n" +
                "File extension: %s", fileNameAndExtension.substring(0, getIndexOfDot), fileNameAndExtension.substring(getIndexOfDot+1));
    }
}
