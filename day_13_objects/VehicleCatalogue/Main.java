package day_13_objects.VehicleCatalogue;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String[] command = scan.nextLine().split("\\s+");
        List<Vehicle> vehicleList = new ArrayList<>();
        double carsHorsePower = 0.0;
        double trucksHorsePower = 0.0;
        int carsCount = 0;
        int trucksCount = 0;
        while(!command[0].equals("End")){
            switch (command[0].toLowerCase()){
                case "car":
                    carsCount++;
                    carsHorsePower += Double.parseDouble(command[3]);
                    break;
                case "truck":
                    trucksCount++;
                    trucksHorsePower += Double.parseDouble(command[3]);
                    break;
            }
            String setCarsType = "Car";
            String setTrucksType = "Truck";
            if(command[0].equalsIgnoreCase("car")){
                Vehicle vehicle = new Vehicle(setCarsType, command[1], command[2], command[3]);
                vehicleList.add(vehicle);
            }else if(command[0].equalsIgnoreCase("truck")){
                Vehicle vehicle = new Vehicle(setTrucksType, command[1], command[2], command[3]);
                vehicleList.add(vehicle);
            }


            command = scan.nextLine().split("\\s+");
        }

        String nextCommand = scan.nextLine();

        while (!nextCommand.equals("Close the Catalogue")){

            for (int i = 0; i < vehicleList.size(); i++) {
                if(vehicleList.get(i).getModel().equals(nextCommand)){
                    System.out.print(vehicleList.get(i));
                }

            }

            nextCommand = scan.nextLine();
        }
        if(carsCount==0){
            System.out.printf("Cars have average horsepower of: 0.00.%n");
        }else{
            System.out.printf("Cars have average horsepower of: %.02f.%n", carsHorsePower/carsCount);
        }
        if(trucksCount==0){
            System.out.printf("Trucks have average horsepower of: 0.00.%n");
        }else{
            System.out.printf("Trucks have average horsepower of: %.02f.", trucksHorsePower/trucksCount);
        }


    }
}
