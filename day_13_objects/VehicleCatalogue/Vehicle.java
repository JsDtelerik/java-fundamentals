package day_13_objects.VehicleCatalogue;

public class Vehicle {
    private String vehicleType;
    private String model;
    private String color;
    private String horsePower;

    public Vehicle (String vehicleType, String model, String color, String horsePower){
        this.vehicleType = vehicleType;
        this.model = model;
        this.color = color;
        this.horsePower = horsePower;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public String getModel() {
        return model;
    }

    public String getColor() {
        return color;
    }

    public String getHorsePower() {
        return horsePower;
    }

    @Override
    public String toString() {
        return String.format("Type: %s\n" +
                "Model: %s\n" +
                "Color: %s\n" +
                "Horsepower: %s\n", vehicleType, model, color, horsePower);
    }
}
