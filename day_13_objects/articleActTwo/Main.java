package day_13_objects.articleActTwo;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        List<Article> articleList = new ArrayList<>();
        int n = Integer.parseInt(scan.nextLine());

        for (int i = 1; i <= n; i++) {
            String[] tokens = scan.nextLine().split(", ");
            Article article = new Article(tokens[0], tokens[1], tokens[2]);
            articleList.add(article);

        }
        String sortBy = scan.nextLine();
        //List<Article> sortedArticles =

        List<Article> sortedArticles = new ArrayList<>();
       switch (sortBy){
            case "title":
                sortedArticles = articleList.stream().sorted(Comparator.comparing(Article::getTitle)).collect(Collectors.toList());
                break;
            case "content":
                sortedArticles = articleList.stream().sorted(Comparator.comparing(Article::getContent)).collect(Collectors.toList());
                break;
            case "author":
                sortedArticles = articleList.stream().sorted(Comparator.comparing(Article::getAuthor)).collect(Collectors.toList());
                break;
        }

        for (Article sortedArticle : sortedArticles) {
            System.out.println(sortedArticle);

        }

    }
}
