package day_13_objects.OrderByAge;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        List<Persons> personsList = new ArrayList<>();
        String[] command = scan.nextLine().split("\\s+");

        while (!command[0].equalsIgnoreCase("end")){
            Persons person = new Persons(command[0], Integer.parseInt(command[1]), Integer.parseInt(command[2]));
           personsList.add(person);

            command = scan.nextLine().split("\\s+");
        }
       List<Persons> persons = personsList.stream().sorted(Comparator.comparing(Persons::getAge)).collect(Collectors.toList());
        for (Persons person : persons) {
            System.out.println(person);
        }
    }
}
