package day_13_objects.OrderByAge;

public class Persons {
    private String name;
    private int identificationCardNumber;
    private int age;

    public Persons(String name, int identificationCardNumber, int age){
        this.name = name;
        this.identificationCardNumber = identificationCardNumber;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public int getIdentificationCardNumber() {
        return identificationCardNumber;
    }

    public int getAge() {
        return age;
    }

    @Override
    public String toString() {
        return String.format("%s with ID: %d is %d years old.", name, identificationCardNumber, age);
    }
}
