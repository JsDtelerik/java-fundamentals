package day_13_objects.opinionPoll;

import java.util.*;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = Integer.parseInt(scan.nextLine());

        List<Person> personList = new ArrayList<>();
        for (int i = 1; i <= n; i++) {
            String[] command = scan.nextLine().split("\\s+");
            Person person = new Person(command[0], Integer.parseInt(command[1]));
            personList.add(person);

        }

        List<Person> persons = personList.stream().filter(e -> e.getAge() > 30).sorted(Comparator.comparing(Person::getName)).collect(Collectors.toList());

        for (Person person : persons) {
            System.out.println(person);

        }
    }
}
