package day_13_objects.Additional.Employee;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;


public class Main {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int n = Integer.parseInt(scan.nextLine());
        List<Employee> employeeList = new ArrayList<>();


        for (int i = 1; i <=n ; i++) {
            String[] tokens = scan.nextLine().split("\\s+");
            Employee employee = new Employee(tokens[0], Double.parseDouble(tokens[1]), tokens[2], tokens[3]);
           if(tokens.length>4){
               employee.setEmail(tokens[4]);
           }else{
               employee.setEmail("n/a");
           }
           if(tokens.length>5){
               employee.setAge(Integer.parseInt(tokens[5]));
           }else{
               employee.setAge(Integer.parseInt(String.valueOf(-1)));
           }

           employeeList.add(employee);

        }

        List<String> departmentNames = employeeList.stream().map(Employee::getDepartment).distinct().collect(Collectors.toList());
        List<Department> departments = departmentNames.stream().map(department -> new Department(department, employeeList.stream().filter(employee -> employee.getDepartment()
                .equals(department)).collect(Collectors.toList()))).sorted(Comparator.comparing(Department::getAverageSalary)).collect(Collectors.toList());

        Department department = departments.get(departments.size()-1);
        department.getEmployeeList().sort(Comparator.comparing(Employee::getSalary).reversed());
        System.out.printf("Highest Average Salary: %s %n", department.getName());

        for (Employee employee : department.getEmployeeList()) {
            System.out.printf("%s %.02f %s %d %n", employee.getName(), employee.getSalary(), employee.getEmail(), employee.getAge());

        }

    }

}
