package day_13_objects.studentsGrade;

import java.util.*;
import java.util.stream.Collectors;

public class main {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int n = Integer.parseInt(scan.nextLine());
        List<Student> studentList = new ArrayList<>();
        for (int i = 1; i <=n ; i++) {
            String[] tokens = scan.nextLine().split("\\s+");
            Student student = new Student(tokens[0], tokens[1], tokens[2]);
            studentList.add(student);
        }
        List<Student> sortedStudents = studentList.stream().sorted(Comparator.comparing(Student::getGrade)).collect(Collectors.toList());
        Collections.reverse(sortedStudents);
        for (Student sortedStudent : sortedStudents) {
            System.out.println(sortedStudent);

        }
    }
}
