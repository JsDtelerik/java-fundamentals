package day_13_objects.article;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String[] input = scan.nextLine().split(", ");
        Articles article = new Articles(input[0], input[1], input[2]);
        int n = Integer.parseInt(scan.nextLine());

        for (int i = 1; i <= n; i++) {
            List<String> command = Arrays.stream(scan.nextLine().split(": ")).collect(Collectors.toList());

            switch (command.get(0)){
                case "Edit":
                    article.edit(command.get(1));
                    break;
                case "ChangeAuthor":
                    article.changeAuthor(command.get(1));
                    break;
                case "Rename":
                    article.rename(command.get(1));
                    break;

            }

        }

        System.out.println(article);

    }
}
