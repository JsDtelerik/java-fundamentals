package day_16_textProcessing;

import java.util.Scanner;

public class reverseAstringWithRecursion {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        String originalString = scan.nextLine();

        String reverseString = reverseOriginalString(originalString);

        System.out.print("The reversed string is: "+ reverseString);
    }

    static String reverseOriginalString(String str) {

        if(str.isEmpty()){
            return str;
        }
        return reverseOriginalString(str.substring(1))+str.charAt(0);
    }
}
