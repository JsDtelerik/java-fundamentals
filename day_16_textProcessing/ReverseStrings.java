package day_16_textProcessing;

import java.util.Scanner;

public class ReverseStrings {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        String text = scan.nextLine();

        while (!text.equals("end")){
            StringBuilder reversedWord = new StringBuilder(text);
            reversedWord.reverse();

            System.out.printf("%s = %s%n", text, reversedWord);
            text = scan.nextLine();
        }
    }
}
