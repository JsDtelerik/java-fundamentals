package day_16_textProcessing;

import java.util.Scanner;

public class ActivationKey {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        String activationKey = scan.nextLine();

        String[] command = scan.nextLine().split(">>>");

        while (!command[0].equals("Generate")){

            switch (command[0]){
                case "Contains":
                    printIfActivationKeyContainsGivenSubstring(command[1], activationKey);
                    break;
                case "Flip":
                    activationKey = changeTheActivationKeyFromGivenIndexToGivenIndex(activationKey, command[1], Integer.parseInt(command[2]), Integer.parseInt(command[3]));
                    break;
                case "Slice":
                    activationKey = deleteFromStartToEdnIndex(activationKey,Integer.parseInt(command[1]), Integer.parseInt(command[2]));
                    break;
            }

            command = scan.nextLine().split(">>>");
        }
        System.out.printf("Your activation key is: %s", activationKey);

    }

    static String deleteFromStartToEdnIndex(String activationKey, int start, int end) {

       String geSymbols = activationKey.substring(start, end);
       activationKey = activationKey.replace(geSymbols, "");

        System.out.println(activationKey);
       return activationKey;
    }

    static String changeTheActivationKeyFromGivenIndexToGivenIndex(String activationKey, String upperLowerCommand, int beginIndex, int ednIndex) {
        String editedKey = "";
        if(upperLowerCommand.equals("Upper")){
           editedKey = activationKey.substring(beginIndex, ednIndex).toUpperCase();
        }else if(upperLowerCommand.equals("Lower")){
            editedKey = activationKey.substring(beginIndex, ednIndex).toLowerCase();
        }
       String substringOfActK = activationKey.substring(beginIndex, ednIndex);
        activationKey = activationKey.replace(substringOfActK, editedKey);
        System.out.println(activationKey);
        return  activationKey;
    }


    static void printIfActivationKeyContainsGivenSubstring(String substring, String activationKey) {

        if(activationKey.contains(substring)){
            System.out.printf("%s contains %s%n", activationKey, substring);
        }else if(!activationKey.contains(substring)){
            System.out.printf("Substring not found!%n");
        }
    }
}
