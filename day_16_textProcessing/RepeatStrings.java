package day_16_textProcessing;

import java.util.Scanner;

public class RepeatStrings {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        String[] words = scan.nextLine().split("\\s+");
        StringBuilder appendedWords = new StringBuilder();
        for (int i = 0; i < words.length; i++) {
            for (int j = 0; j < words[i].length(); j++) {
                appendedWords.append(words[i]);
            }
        }
        System.out.println(appendedWords);
    }
}
