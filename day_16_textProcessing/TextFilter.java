package day_16_textProcessing;

import java.util.Scanner;

public class TextFilter {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String[] banList = scan.nextLine().split(", ");

        String text =  scan.nextLine();

        for (String word : banList) {
            if(text.contains(word)){
                String editedWord = editCurrentWord(word);
                text = text.replace(word, editedWord);
            }
        }
        System.out.println(text);
    }

    static String editCurrentWord(String word) {
        StringBuilder editWord = new StringBuilder();

        for (int i = 0; i < word.length(); i++) {
            editWord.append("*");
        }
        return editWord.toString();
    }
}
