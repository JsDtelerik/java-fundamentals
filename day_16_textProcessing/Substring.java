package day_16_textProcessing;

import java.util.Scanner;

public class Substring {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String wordToRemove = scan.nextLine();
        String word = scan.nextLine();

       while(word.contains(wordToRemove)){
           word = removeWordToRemove(word, wordToRemove);
       }

        System.out.println(word);
    }

    static String removeWordToRemove(String word, String wordToRemove) {

        int beginIndex = word.indexOf(wordToRemove);
        int endIndex = beginIndex + wordToRemove.length();

        return word.substring(0, beginIndex) + word.substring(endIndex);
    }
}
