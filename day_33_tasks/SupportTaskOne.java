package day_33_finalExam;

import java.util.Scanner;

public class SupportTaskOne {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String text = scan.nextLine();
        String[] command = scan.nextLine().split("\\s+");

        while (!command[0].equals("Finish")) {
            switch (command[0]) {
                case "Replace":
                    text = replaceAllGivenChars(text, command[1], command[2]);
                    break;
                case "Cut":
                    text = cutChars(text, Integer.parseInt(command[1]), Integer.parseInt(command[2]));
                    break;
                case "Make":
                    if(command[1].equals("Lower")){
                        text = text.toLowerCase();

                    }else if(command[1].equals("Upper")){
                        text = text.toUpperCase();
                    }
                    System.out.println(text);
                    break;
                case "Check":
                    checkIfTextIncludesOtherText(text, command[1]);
                    break;
                case "Sum":
                    checkIfTextEndsWithOtherText(text, Integer.parseInt(command[1]), Integer.parseInt(command[2]));
                    break;


            }


            command = scan.nextLine().split("\\s+");

        }
    }

    static void checkIfTextEndsWithOtherText(String text, int start, int end) {

        if(start>=0 && end<=text.length()-1){
            String substring = text.substring(start, end+1);
            int value = 0;
            for (int i = 0; i < substring.length(); i++) {
                char symbol = substring.charAt(i);
                value +=symbol;
            }
            System.out.println(value);
        }else{
            System.out.println("Invalid indices!");
        }
    }


    static String replaceAllGivenChars(String text, String replaceThis, String replacement) {
        text = text.replace(replaceThis, replacement);
        System.out.println(text);
        return text;
    }

    static void checkIfTextIncludesOtherText(String text, String checkThis) {

        if (text.contains(checkThis)) {
            System.out.printf("Message contains %s%n", checkThis);
        } else {
            System.out.printf("Message doesn't contain %s%n", checkThis);
        }
    }



    static String cutChars(String text, int startIndex, int endIndex) {
        if(startIndex<0 || endIndex > text.length()-1){
            System.out.println("Invalid indices!");

        }else{
            String substring = text.substring(startIndex, endIndex+1);
            text = text.replace(substring, "");
            System.out.println(text);
            return text;
        }


        return text;
    }


}