package day_33_finalExam;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SecondProblem {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = Integer.parseInt(scan.nextLine());
        String regex = "(?<command>![A-Z][a-z]{3,}!:)(?<message>\\[[A-Za-z]{8,}\\])";
        Pattern pattern = Pattern.compile(regex);
        for (int i = 0; i < n; i++) {
            String input = scan.nextLine();
            Matcher matcher = pattern.matcher(input);
            StringBuilder decryptedMessage = new StringBuilder();
            if(matcher.find()){
                String message = matcher.group("message");
                message = message.replace("[", "");
                message = message.replace("]", "");

                for (int j = 0; j < message.length(); j++) {
                    char symbol = message.charAt(j);
                    int addThisNumber = symbol;
                        decryptedMessage.append(addThisNumber+" ");

                }
                String decrypted = decryptedMessage.toString();
                String commandFromLine = matcher.group("command");
                commandFromLine = commandFromLine.replace("!", "");

                System.out.println(commandFromLine + " " + decrypted);

            }else{
                System.out.println("The message is invalid");
            }


        }

    }
}
