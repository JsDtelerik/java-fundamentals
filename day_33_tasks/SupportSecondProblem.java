package day_33_finalExam;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SupportSecondProblem {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String regex = "([^<>]*)(?<password>\\>[0-9]{3}\\|[a-z]{3}\\|[A-Z]{3}\\|[^\\<\\>]{3}\\<)\\1";
        Pattern pattern = Pattern.compile(regex);
        int n = Integer.parseInt(scan.nextLine());

        for (int i = 0; i < n; i++) {
            String givenPassword = scan.nextLine();
            Matcher matcher = pattern.matcher(givenPassword);

            if(matcher.find()){
                String matchedPassword = matcher.group("password");
                matchedPassword = matchedPassword.replace("|", "");
                matchedPassword = matchedPassword.replace("<", "");
                matchedPassword = matchedPassword.replace(">", "");
                StringBuilder concPass = new StringBuilder();
                for (int j = 0; j < matchedPassword.length(); j++) {
                    char symbol = matchedPassword.charAt(j);
                    concPass.append(symbol);
                }
                System.out.printf("Password: %s%n", concPass.toString());
            }else{
                System.out.println("Try another password!");
            }
        }
    }
}
