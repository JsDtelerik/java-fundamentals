package day_33_finalExam;

import java.util.Scanner;

public class SupportFirstProblem {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String text = scan.nextLine();
        String[] command = scan.nextLine().split("\\s+");

        while (!command[0].equals("End")) {
            switch (command[0]) {
                case "Translate":
                    text = replaceAllGivenChars(text, command[1], command[2]);
                    break;
                case "Includes":
                    checkIfTextIncludesOtherText(text, command[1]);
                    break;
                case "Start":
                    checkIfTextEndsWithOtherText(text, command[1]);
                    break;
                case "Lowercase":
                    text = text.toLowerCase();
                    System.out.println(text);
                    break;
                case "FindIndex":
                    findFirstIndexOfGivenChar(text, command[1]);
                    break;
                case "Remove":
                    text = cutChars(text, Integer.parseInt(command[1]), Integer.parseInt(command[2]));
                    break;
            }


            command = scan.nextLine().split("\\s+");

        }
    }


    static String replaceAllGivenChars(String text, String replaceThis, String replacement) {
        text = text.replace(replaceThis, replacement);
        System.out.println(text);
        return text;
    }

    static void checkIfTextIncludesOtherText(String text, String checkThis) {

        if (text.contains(checkThis)) {
            System.out.println("True");
        } else {
            System.out.println("False");
        }
    }


    static void checkIfTextEndsWithOtherText(String text, String check) {
        if (text.startsWith(check)) {
            System.out.println("True");
        } else {
            System.out.println("False");
        }


    }

    static void findFirstIndexOfGivenChar(String text, String letter) {
        int indexOfLetter = text.lastIndexOf(letter);
        System.out.println(indexOfLetter);

    }

    static String cutChars(String text, int startIndex, int length) {

        String substring = text.substring(startIndex, startIndex + length);
        text = text.replace(substring, "");
        System.out.println(text);
        return text;
    }


}


