package day_33_finalExam;

import java.sql.SQLOutput;
import java.util.*;

public class ThirdProblem {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        Map<String, List<String>> userNames = new TreeMap<>();
        String[] command = scan.nextLine().split("->");
        while (!command[0].equals("Statistics")){
            switch (command[0]){
                case "Add":
                    userNames = addNewUserName(userNames, command[1]);
                    break;
                case "Send":
                    userNames = sendEmailsFromUsers(userNames, command[1], command[2]);
                    break;
                case "Delete":
                    userNames = deleteGivenUser(userNames, command[1]);
                    break;
            }

            command = scan.nextLine().split("->");
        }

        System.out.printf("Users count: %d%n", userNames.size());
        userNames.entrySet().stream().sorted((e1, e2) -> Integer.compare(e2.getValue().size(), e1.getValue().size()))
                .forEach(e ->  {
                    System.out.printf("%s%n", e.getKey());
                    e.getValue().stream().forEach(user -> System.out.println(" "+"- "+ user));
                });

//fo
    }



    static Map<String, List<String>> addNewUserName(Map<String, List<String>> userNames, String user) {
        if (userNames.containsKey(user)){
            System.out.printf("%s is already registered%n", user);
        }else{
            userNames.put(user, new ArrayList<>());
            return userNames;
        }
        return userNames;
    }
    static Map<String, List<String>> sendEmailsFromUsers(Map<String, List<String>> userNames, String user, String email) {

        if(userNames.containsKey(user)){
            userNames.get(user).add(email);
            return userNames;
        }else{
            userNames.put(user, new ArrayList<>());
            userNames.get(user).add(email);
            return userNames;
        }

    }

    static Map<String, List<String>> deleteGivenUser(Map<String, List<String>> userNames, String user) {
        if (userNames.containsKey(user)){
            userNames.remove(user);
            return userNames;
        }else{
            System.out.printf("%s not found!%n", user);
        }
        return userNames;
    }


}
