package day_14_midExam.realExam;


import java.util.*;
import java.util.stream.Collectors;

public class ProblemTwo {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        List<String> numbers = Arrays.stream(scan.nextLine().split("\\s+")).collect(Collectors.toList());

        String[] command = scan.nextLine().split("\\s+");

        while (!command[0].equals("end")){


            switch (command[0]){
                case "reverse":
                    numbers = reverseElements(numbers, Integer.parseInt(command[2]), Integer.parseInt(command[4]));
                    break;
                case "sort":
                    numbers = sortNumbersFromIndexToCount(numbers, Integer.parseInt(command[2]), Integer.parseInt(command[4]));
                    break;
                case "remove":
                    numbers = removeElements(numbers, Integer.parseInt(command[1]));
                    break;

            }

            command = scan.nextLine().split("\\s+");
        }

        printResult(numbers);

    }



    static List<String> reverseElements(List<String> numbers, int startIndex, int countOfElements) {

        List<String> reversedElements = new ArrayList<>();

        for (int i = 1; i <= countOfElements; i++) {
            reversedElements.add(numbers.get(startIndex));
            numbers.remove(startIndex);
        }
        Collections.reverse(reversedElements);

        for (int i = 0; i < reversedElements.size(); i++) {
            numbers.add(startIndex++, reversedElements.get(i));

        }

        return numbers;
    }

    static List<String> sortNumbersFromIndexToCount(List<String> numbers, int sortStart, int countOfElements) {
        List<String> addelements = new ArrayList<>();

        for (int i = 1; i <=countOfElements ; i++) {
            addelements.add(numbers.get(sortStart));
            numbers.remove(sortStart);

        }

        Collections.sort(addelements);

        for (int i = 0; i < addelements.size(); i++) {
            numbers.add(sortStart++, addelements.get(i));

        }

        return numbers;

    }

    static List<String> removeElements(List<String> numbers, int count) {
        for (int i = 1; i <= count; i++) {

               numbers.remove(0);

        }


        return  numbers;

    }

    static void printResult(List<String> numbers) {

        System.out.println(String.join(", ", numbers));
    }



}
