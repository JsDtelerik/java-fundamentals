package day_14_midExam.realExam;

import java.util.Scanner;

public class Assist {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int biscuitsProducedPerDayByASingleWorker = Integer.parseInt(scan.nextLine());

        int countOfWorkers = Integer.parseInt(scan.nextLine());

        int target = Integer.parseInt(scan.nextLine());
        double biscuits = 0;
        double biscuitsPerDay = 0;
        for (int i = 1; i <= 30; i++) {

            if(i % 3 == 0){
             biscuitsPerDay = (Math.floor(biscuitsProducedPerDayByASingleWorker*countOfWorkers)*0.75);
            }else{
                biscuitsPerDay = Math.floor(biscuitsProducedPerDayByASingleWorker*countOfWorkers);
            }

            biscuits += biscuitsPerDay;

        }
        System.out.printf("You have produced %.0f biscuits for the past month.%n", biscuits);

        if(biscuits>target){
            System.out.printf("You produce %.02f percent more biscuits.", Math.abs(100-((biscuits/target)*100)));
        }
        System.out.printf("You produce %.02f percent less biscuits.", Math.abs(100-(biscuits/target*100)));
    }
}
