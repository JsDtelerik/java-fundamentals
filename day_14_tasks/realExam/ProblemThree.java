package day_14_midExam.realExam;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ProblemThree {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        List<String> message = new ArrayList<>();
        String[] command = scan.nextLine().split(" ");

        while (!command[0].equals("end")){
            switch (command[0]){
                case "Chat":
                    message = addChatMessage(command, message);
                    break;
                case "Delete":
                    message = deleteMessage(message, command);
                    break;
                case "Edit":
                    message = editExistingMessage(message, command);
                    break;
                case "Pin":
                    message = pinMessageAndMoveItToTheEnd(message, command);
                    break;
                case "Spam":
                    message = spamMessage(message, command);
                    break;

            }

            command = scan.nextLine().split("\\s+");
        }

        printMessage(message);

    }



    static List<String> addChatMessage(String[] command, List<String> message) {

            message.add(command[1]);


        return message;
    }

    static List<String> deleteMessage(List<String> message, String[] command) {

            message.remove(command[1]);



        return message;
    }


    static List<String> editExistingMessage(List<String> message, String[] command) {

        if(message.contains(command[1])){
            for (int i = 0; i < message.size(); i++) {
                if(message.get(i).equals(command[1])){
                    message.set(i, command[2]);
                    break;
                }
            }
        }
        return message;
    }

    static List<String> pinMessageAndMoveItToTheEnd(List<String> message, String[] command) {
        if (message.contains(command[1])) {

            for (int i = 0; i < message.size(); i++) {
                if (message.get(i).equals(command[1])) {
                    message.remove(i);
                    message.add(command[1]);
                    break;

                }
            }
        }
        return message;
    }
    static List<String> spamMessage(List<String> message, String[] command) {

        for (int i = 1; i < command.length; i++) {
            message.add(command[i]);

        }
        return message;
    }

    static void printMessage(List<String> message) {
        for (String s : message) {
            System.out.println(s);

        }
    }

}
