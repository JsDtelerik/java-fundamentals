package day_14_midExam.realExam;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

import static day_14_midExam.realExam.ProblemTwo.printResult;

public class AssistPartTwo {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        List<String> sugarCubes = Arrays.stream(scan.nextLine().split("\\s+")).collect(Collectors.toList());

        String[] command = scan.nextLine().split("\\s+");

        while(!command[0].equals("Mort")){

            switch (command[0]){

                case "Add":
                    sugarCubes = addValueAtTheEnd(sugarCubes, command);
                    break;
                case "Remove":
                    sugarCubes = removeFirstElementEqualToCommand(sugarCubes, command[1]);
                    break;
                case "Replace":
                    sugarCubes = replaceElementWithANewValue(sugarCubes, command[1], command[2]);
                    break;
                case "Collapse":
                    sugarCubes = removeAllElementsLessThanGivenElement(sugarCubes, Integer.parseInt(command[1]));
                    break;

            }

            command = scan.nextLine().split("\\s+");
        }

        print(sugarCubes);
    }


    static List<String> addValueAtTheEnd(List<String> sugarCubes, String[] command) {

        sugarCubes.add(command[1]);
        return sugarCubes;
    }

    static List<String> removeFirstElementEqualToCommand(List<String> sugarCubes, String element) {
        for (int i = 0; i < sugarCubes.size(); i++) {
            if(sugarCubes.get(i).equals(element)){
                sugarCubes.remove(i);
                break;
            }

        }

        return sugarCubes;
    }

    private static List<String> replaceElementWithANewValue(List<String> sugarCubes, String elementValue, String replacement) {

        for (int i = 0; i < sugarCubes.size(); i++) {
            if(sugarCubes.get(i).equals(elementValue)){
                sugarCubes.set(i, replacement);
                break;
            }

        }
        return sugarCubes;
    }

    static List<String> removeAllElementsLessThanGivenElement(List<String> sugarCubes, int command) {
        for (int i = 0; i < sugarCubes.size(); i++) {
            if(Integer.parseInt(sugarCubes.get(i))<command){
                sugarCubes.remove(i);
                i = -1;
            }
        }
        return sugarCubes;
    }

    static void print(List<String> sugarCubes) {

        System.out.println(String.join(" ", sugarCubes));
    }

}
