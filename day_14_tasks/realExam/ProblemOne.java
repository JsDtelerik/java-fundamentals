package day_14_midExam.realExam;

import java.util.Scanner;

public class ProblemOne {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        double neededExperience = Double.parseDouble(scan.nextLine());
        int countOfBattles = Integer.parseInt(scan.nextLine());
        int counter = 0;
        double sumExperience = 0;
        boolean tankUnlocked = false;
        for (int i = 1; i <= countOfBattles; i++) {
            double experienceGained = Double.parseDouble(scan.nextLine());
            counter++;
            boolean specialExperience = false;
            if (i % 3 == 0) {
                sumExperience += experienceGained *1.15;
                specialExperience = true;
            }

            if (i % 5 == 0) {
                sumExperience += experienceGained * 0.90;
                specialExperience = true;
            }
            if (i % 15 == 0) {
                sumExperience += experienceGained * 1.05;
                specialExperience = true;
            }

            if(!specialExperience){
                sumExperience += experienceGained;
            }

            if (sumExperience >= neededExperience) {
                tankUnlocked = true;
                break;
            }


        }

        if(tankUnlocked){
            System.out.printf("Player successfully collected his needed experience for %d battles.", counter);
        }else{
            System.out.printf("Player was not able to collect the needed experience, %.02f more needed.", neededExperience-sumExperience);
        }

    }
}
