package day_08_methods;

import javax.xml.crypto.dom.DOMCryptoContext;
import java.util.Scanner;

public class calculations {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        String command = scan.nextLine();
        double firstNumber = Double.parseDouble(scan.nextLine());
        double secondNumber = Double.parseDouble(scan.nextLine());
       switch (command){
           case "add": sumBothNumbers(firstNumber, secondNumber); break;
           case "subtract": subtractBothNumbers(firstNumber, secondNumber); break;
           case "multiply": multiplyBothNumbers(firstNumber, secondNumber); break;
           case "divide": divideBothNumbers(firstNumber, secondNumber); break;
       }
    }


    private static void sumBothNumbers(double firstNumber, double secondNumber) {
        double sum = firstNumber+secondNumber;
        System.out.printf("%.0f", sum);
    }
    private static void subtractBothNumbers(double firstNumber, double secondNumber) {
        double subtract = firstNumber-secondNumber;
        System.out.printf("%.0f", subtract);

    }

    private static void multiplyBothNumbers(double firstNumber, double secondNumber) {
        double multiply = firstNumber*secondNumber;
        System.out.printf("%.0f", multiply);
    }

    private static void divideBothNumbers(double firstNumber, double secondNumber) {
        double divide = firstNumber/secondNumber;
        System.out.printf("%.0f", divide);
    }


}
