package day_08_methods;

import java.util.Scanner;

public class multiplyEvensByOdds {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int number = Math.abs(Integer.parseInt(scan.nextLine()));
        int sumOfEvenNumbers = getSumOfEvenNumbersFromReceivedNumber(number);
        int sumOfOddNumbers = getSumOfOddNumbersFromReceivedNumber(number);

        printMultiplyResultOfTheBothSumOfNumbers(sumOfEvenNumbers, sumOfOddNumbers);
    }



    static int getSumOfEvenNumbersFromReceivedNumber(int number) {
        int sumOfEvenNumbers = 0;
        for (int i = number; i >0 ; i /= 10) {
            int currentNumber = i %10;
            if (currentNumber %2 == 0){
                sumOfEvenNumbers += currentNumber;
            }
        }
        return sumOfEvenNumbers;
    }

    static int getSumOfOddNumbersFromReceivedNumber(int number) {
        int sumOddNumbers = 0;
        while (number>0){
            int currentNumber = number%10;
            if (currentNumber%2 !=0){
                sumOddNumbers +=currentNumber;
            }
            number/=10;
        }
        return sumOddNumbers;
    }

    static void printMultiplyResultOfTheBothSumOfNumbers(int sumOfEvenNumbers, int sumOfOddNumbers) {
        int sum = sumOfEvenNumbers*sumOfOddNumbers;
        System.out.println(sum);
    }
}
