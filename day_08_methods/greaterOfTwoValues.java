package day_08_methods;

import java.util.Scanner;

public class greaterOfTwoValues {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String type = scan.nextLine();
        String getMax = " ";
        switch (type){
            case "int":
                int firstReceivedNumber = Integer.parseInt(scan.nextLine());
                int secondReceivedNumber = Integer.parseInt(scan.nextLine());
               getMax = getMaxInt(firstReceivedNumber, secondReceivedNumber); break;
            case "char":
                char firstReceivedSymbol = scan.nextLine().charAt(0);
                char secondReceivedSymbol = scan.nextLine().charAt(0);
                getMax = getMaxChar(firstReceivedSymbol, secondReceivedSymbol); break;
            case "string":
                String firstReceivedString = scan.nextLine();
                String secondReceivedString = scan.nextLine();
                getMax = getMaxString(firstReceivedString, secondReceivedString); break;

        }
        System.out.println(getMax);

    }

    static String getMaxInt(int firstReceivedNumber, int secondReceivedNumber) {
        String result = " ";
        if (firstReceivedNumber>=secondReceivedNumber){
            result = String.valueOf(firstReceivedNumber);
        }else{
            result = String.valueOf(secondReceivedNumber);
        }
        return result;
    }

    static String getMaxChar(char firstReceivedSymbol, char secondReceivedSymbol) {
        String result = " ";
        if (firstReceivedSymbol>=secondReceivedSymbol){
            result = String.valueOf(firstReceivedSymbol);
        }else {
            result = String.valueOf(secondReceivedSymbol);
        }

        return result;
    }

    static String getMaxString(String firstReceivedString, String secondReceivedString) {
        String result = " ";
        if (firstReceivedString.compareTo(secondReceivedString)>=0){
            result = firstReceivedString;
        }else{
            result = secondReceivedString;
        }

        return result;
    }

}
