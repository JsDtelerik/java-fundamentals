package day_08_methods;

import java.util.Scanner;

public class calculateRectangleArea {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int width = Integer.parseInt(scan.nextLine());
        int height = Integer.parseInt(scan.nextLine());

       int area = calculateRectangleAreaAndReturnIt(width, height);
        printRectangle(area);
    }



    static int calculateRectangleAreaAndReturnIt(int width, int height) {
        int area = width * height;
        return area;
    }

    private static void printRectangle(int area) {
        System.out.println(area);
    }

}
