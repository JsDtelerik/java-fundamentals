package day_08_methods;

import java.util.Scanner;

public class signOfInteger {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        intSign(Integer.parseInt(scan.nextLine()));

    }

    public static void  intSign( int number){
        if (number>0){
            System.out.printf("The number %d is positive.", number);
        }else if (number<0){
            System.out.printf("The number %d is negative.", number);
        }else{
            System.out.printf("The number 0 is zero.");
        }

    }
}
