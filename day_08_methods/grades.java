package day_08_methods;

import java.util.Scanner;

public class grades {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        receivedGrade(scan.nextLine());


    }
    private static void receivedGrade(String grade) {
        double parsedGrade = Double.parseDouble(grade);
        String mark = " ";
        if (parsedGrade >= 2.00 && parsedGrade <= 2.99) {
            mark = "Fail";
        } else if (parsedGrade >= 3.00 && parsedGrade < 3.50) {
            mark = "Poor";
        } else if (parsedGrade >= 3.50 && parsedGrade < 4.50) {
            mark = "Good";
        } else if (parsedGrade >= 4.50 && parsedGrade < 5.50) {
            mark = "Very good";
        } else if (parsedGrade >= 5.50 && parsedGrade <= 6.00) {
            mark = "Excellent";
        }
        printMark(mark);
    }

    private static void printMark(String mark) {
        System.out.println(mark);

    }
}
