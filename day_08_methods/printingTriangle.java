package day_08_methods;

import java.util.Scanner;

public class printingTriangle {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int height = Integer.parseInt(scan.nextLine());
        printTriangle(height);

    }

    private static void printTriangle(int height) {
        printTop(height);
        printBottom(height);
    }

    private static void printTop(int end) {
        for (int i = 1; i <= end; i++) {
            printLines(i);
        }

    }

    private static void printBottom(int start) {
        for (int i = start - 1; i >= 1; i--) {
            printLines(i);

        }
    }

    private static void printLines(int length) {
        for (int i = 1; i <= length; i++) {
            System.out.print(i + " ");
        }
        System.out.println();
    }
}
