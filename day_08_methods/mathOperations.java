package day_08_methods;

import java.util.Scanner;

public class mathOperations {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int firstNumber = Integer.parseInt(scan.nextLine());
        String operator = scan.nextLine();
        int secondNumber = Integer.parseInt(scan.nextLine());
        String getResultAfterOperation = " ";
        switch (operator){
            case "*": getResultAfterOperation= multiplyOperation(firstNumber, secondNumber); break;
            case "/": getResultAfterOperation= divideOperation(firstNumber, secondNumber); break;
            case "-": getResultAfterOperation= subtractOperation(firstNumber, secondNumber); break;
            case "+": getResultAfterOperation= addOperation(firstNumber, secondNumber); break;
        }

        printResult(getResultAfterOperation);
    }

    static String multiplyOperation(int firstNumber, int secondNumber) {
        int calculation = firstNumber*secondNumber;
        String result = String.valueOf(calculation);
        return result;
    }

    static String divideOperation(int firstNumber, int secondNumber) {
        int calculation = firstNumber/secondNumber;
        String result = String.valueOf(calculation);
        return result;
    }

    static String subtractOperation(int firstNumber, int secondNumber) {
        int calculation = firstNumber-secondNumber;
        String result = String.valueOf(calculation);
        return result;
    }
    static String addOperation(int firstNumber, int secondNumber) {
        int calculation = firstNumber+secondNumber;
        String result = String.valueOf(calculation);
        return result;
    }
    static void printResult(String getResultAfterOperation) {
        System.out.println(getResultAfterOperation);
    }

}
