package day_08_methods;

import java.text.DecimalFormat;
import java.util.Scanner;

public class mathPower {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        double number = Double.parseDouble(scan.nextLine());
        int powerBy = Integer.parseInt(scan.nextLine());

        double poweredNumber = valueOfPoweredNumber(number, powerBy);
        printResult(poweredNumber);


    }

    static double valueOfPoweredNumber(double number, int powerBy) {

        return Math.pow(number, powerBy);
    }
    private static void printResult(double poweredNumber) {
        String pattern = "0.####";
        DecimalFormat decimalFormat = new DecimalFormat(pattern);
        String formatedNumberWithDecimalFormat = decimalFormat.format(poweredNumber);
        System.out.println(formatedNumberWithDecimalFormat);
    }
}
