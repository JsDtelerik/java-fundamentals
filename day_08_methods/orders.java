package day_08_methods;

import java.util.Scanner;

public class orders {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String product = scan.nextLine();
        int quantity = Integer.parseInt(scan.nextLine());

        switch (product){
            case "coffee": billCalculation(quantity, 1.50); break;
            case "water": billCalculation(quantity, 1.00); break;
            case "coke": billCalculation(quantity, 1.40); break;
            case "snacks": billCalculation(quantity, 2.00); break;
        }

    }

    private static void billCalculation(int quantity, double price) {
        printPrice(quantity*price);
    }

    private static void printPrice(double sum) {
        System.out.printf("%.02f", sum);
    }
}
