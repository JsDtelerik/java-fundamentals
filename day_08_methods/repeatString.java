package day_08_methods;

import java.util.Scanner;

public class repeatString {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        String text = scan.nextLine();
        int repetition = Integer.parseInt(scan.nextLine());
        String repeatedWords = repeatedText(text, repetition);
        printResult(repeatedWords);

    }

    private static String repeatedText(String text, int repetition) {
        StringBuilder newWord = new StringBuilder("");
        for (int i = 1; i <=repetition ; i++) {
            newWord.append(text);
        }

        return newWord.toString();
    }

    private static void printResult(String printRepeatedReceivedWord) {
        System.out.println(printRepeatedReceivedWord);
    }


}
