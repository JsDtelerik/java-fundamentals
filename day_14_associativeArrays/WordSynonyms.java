package day_14_associativeArrays;


import java.util.*;

public class WordSynonyms {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = Integer.parseInt(scan.nextLine());
        Map<String, List<String>> synonymsDictionary = new LinkedHashMap<>();

        for (int i = 1; i <=n ; i++) {
            String word = scan.nextLine();
            String synonym  = scan.nextLine();
           if(!synonymsDictionary.containsKey(word)){
               List<String> synonymsForCurrentWord = new ArrayList<>();
               synonymsForCurrentWord.add(synonym);
               synonymsDictionary.put(word, synonymsForCurrentWord);
           }else{
               List<String> synonymsForCurrentWord = synonymsDictionary.get(word);
               synonymsForCurrentWord.add(synonym);
               synonymsDictionary.put(word, synonymsForCurrentWord);
           }

        }
        for (Map.Entry<String, List<String>> stringListEntry : synonymsDictionary.entrySet()) {

            System.out.printf("%s - %s%n", stringListEntry.getKey(), String.join(", ", stringListEntry.getValue()));

        }

        }


}
