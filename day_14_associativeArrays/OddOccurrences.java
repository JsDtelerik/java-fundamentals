package day_14_associativeArrays;

import java.util.*;

public class OddOccurrences {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        String[] words = scan.nextLine().split("\\s+");
        Map<String, Integer> sortedWordByAppearance = new LinkedHashMap<>();

        for (int i = 0; i < words.length; i++) {
            String word = words[i].toLowerCase();
            if(!sortedWordByAppearance.containsKey(word)){
                sortedWordByAppearance.put(word, 1);
            }else{
                sortedWordByAppearance.put(word, sortedWordByAppearance.get(word)+1);
            }
        }
        List<String> addWord = new ArrayList<>();
        for (Map.Entry<String, Integer> stringIntegerEntry : sortedWordByAppearance.entrySet()) {
                if(stringIntegerEntry.getValue() % 2 != 0){
                        addWord.add(stringIntegerEntry.getKey());
                }
        }
        System.out.println(String.join(", ", addWord));

    }
}
