package day_14_associativeArrays;

import java.util.Arrays;
import java.util.Scanner;

public class WordFilter {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        String[] words = scan.nextLine().split("\\s+");

        Arrays.stream(words).filter(w -> w.length() % 2 == 0).forEach(System.out::println);


        }
    }

