package day_02;

import java.util.Scanner;

public class PrintAndSum {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int starNumber = Integer.parseInt(scan.nextLine());
        int endNumber = Integer.parseInt(scan.nextLine());
        int sumNumbers = 0;
        for (int i = starNumber; i <=endNumber; i++) {
            System.out.print(i + " ");
            sumNumbers +=i;
        }
        System.out.printf("%nSum: %d", sumNumbers);
    }
}
