package day_02;

import java.util.Scanner;

public class Vacation {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int groupNumber = Integer.parseInt(scan.nextLine());
        String groupType = scan.nextLine();
        String dayOfTheWeek = scan.nextLine();

        double priceWithoutDiscount = 0;
        double priceWithDiscount = 0;
        switch (groupType){
            case "Students":
                if (dayOfTheWeek.equals("Friday")){
                    priceWithoutDiscount = groupNumber*8.45;
                }else if (dayOfTheWeek.equals("Saturday")){
                    priceWithoutDiscount = groupNumber*9.80;
                }else if (dayOfTheWeek.equals("Sunday")){
                    priceWithoutDiscount = groupNumber*10.46;
                }
                if (groupNumber>=30){
                    priceWithDiscount =priceWithoutDiscount*0.85;
                }else{
                    priceWithDiscount = priceWithoutDiscount;
                }
                break;
            case "Business":
                if (dayOfTheWeek.equals("Friday")){
                    priceWithoutDiscount = groupNumber*10.90;
                }else if (dayOfTheWeek.equals("Saturday")){
                    priceWithoutDiscount = groupNumber*15.60;
                }else if (dayOfTheWeek.equals("Sunday")){
                    priceWithoutDiscount = groupNumber*16.00;
                }
                if (groupNumber>=100){
                    double pricePerPerson = priceWithoutDiscount/groupNumber;
                    priceWithDiscount =priceWithoutDiscount-(pricePerPerson*10);
                }else{
                    priceWithDiscount = priceWithoutDiscount;
                }
                break;
            case "Regular":
                if (dayOfTheWeek.equals("Friday")){
                    priceWithoutDiscount = groupNumber*15.00;
                }else if (dayOfTheWeek.equals("Saturday")){
                    priceWithoutDiscount = groupNumber*20.00;
                }else if (dayOfTheWeek.equals("Sunday")){
                    priceWithoutDiscount = groupNumber*22.50;
                }
                if (groupNumber>=10 && groupNumber<=20){
                    priceWithDiscount =priceWithoutDiscount*0.95;
                }else{
                    priceWithDiscount = priceWithoutDiscount;
                }
                break;
        }

        System.out.printf("Total price: %.02f", priceWithDiscount);


    }
}
