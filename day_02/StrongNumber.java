package day_02;

import java.util.Scanner;

public class StrongNumber {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int number = Integer.parseInt(scan.nextLine());
        int keepNumber = number;
        int newN = 0;
        int factSum = 0;
        while (number!=0){
            newN = number %10;
            int fact = 1;
            for (int i = 1; i <=newN ; i++) {
                    fact *=i;
            }
            factSum += fact;
            number /=10;

        }
        if (keepNumber == factSum){
            System.out.print("yes");
        } else {
            System.out.print("no");
        }
    }
}
