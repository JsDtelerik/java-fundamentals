package day_02;

import java.util.Scanner;

public class VendingMachine {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String insertedCoins = scan.nextLine();
        double sumInsertedCoins =0.0;
        while (!insertedCoins.equals("Start")){
            if (!insertedCoins.equals("Start")){
               double coins = Double.parseDouble(insertedCoins);
               //0.1, 0.2, 0.5, 1, and 2
               if (coins == 0.1 || coins == 0.2 || coins == 0.5 || coins == 1 || coins == 2){
                   sumInsertedCoins += coins;
               }else{
                   System.out.printf("Cannot accept %.02f%n",coins);
               }
            }
            insertedCoins = scan.nextLine();
        }
        String purchasedProduct = scan.nextLine();
        double productCost = 0.0;
        while (!purchasedProduct.equals("End")){
            switch (purchasedProduct){
                case "Nuts": productCost = 2.00; break;
                case "Water": productCost = 0.70; break;
                case "Crisps": productCost = 1.50; break;
                case "Soda": productCost = 0.80; break;
                case "Coke": productCost = 1.00; break;
                default:
                    System.out.printf("Invalid product%n");
                    purchasedProduct = scan.nextLine();
                    continue;

            }
            if (productCost<=sumInsertedCoins){
                System.out.printf("Purchased %s%n", purchasedProduct);
                sumInsertedCoins-=productCost;
            }else{
                System.out.printf("Sorry, not enough money%n");
            }
            purchasedProduct = scan.nextLine();
        }
        System.out.printf("Change: %.02f", sumInsertedCoins);
    }
}
