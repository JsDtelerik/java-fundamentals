package day_02;

import java.util.Scanner;

public class Login {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        String userName = scan.nextLine();
        String password = "";
        for (int i = userName.length() - 1; i >= 0; i--) {
           password = password + userName.charAt(i);
        }
        boolean isBlocked = false;
        String enteredPassword = scan.nextLine();
        int passwordCounter = 0;
        while (!enteredPassword.equals(password)){
            passwordCounter++;
            if (passwordCounter<4){
                System.out.println("Incorrect password. Try again.");
            }else if (passwordCounter == 4){
                System.out.printf("User %s blocked!", userName);
                isBlocked = true;
                break;

            }
            enteredPassword = scan.nextLine();
        }
        if (!isBlocked){
            System.out.printf("User %s logged in.", userName);
        }

    }
}
