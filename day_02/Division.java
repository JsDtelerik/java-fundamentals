package day_02;

import java.util.Scanner;

public class Division {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = Integer.parseInt(scan.nextLine());
        int divisibleN = 0;
        if (n % 2 == 0) {
            divisibleN = 2;
        }

        if (n % 3 == 0) {
            divisibleN = 3;
        }
        if (n % 6 == 0) {
            divisibleN = 6;
        }
        if (n % 7 == 0) {
            divisibleN = 7;
        }
        if (n % 10 == 0) {
            divisibleN = 10;

        }
        if (divisibleN == 0) {
            System.out.print("Not divisible");
        } else {
            System.out.printf("The number is divisible by %d", divisibleN);
        }

    }
}









