package day_02.additional;

import java.util.Scanner;

public class GamingStore {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        double currentBalance = Double.parseDouble(scan.nextLine());
        String gameName = scan.nextLine();
        double gamePrice = 0.0;
        double sumExpensesForBoughtGames = 0.0;
        label:
        while (!gameName.equals("Game Time")){
            switch (gameName){
                case "OutFall 4": gamePrice = 39.99; break;
                case "CS: OG": gamePrice = 15.99; break;
                case "Zplinter Zell": gamePrice = 19.99; break;
                case "Honored 2": gamePrice = 59.99; break;
                case "RoverWatch": gamePrice = 29.99; break;
                case "RoverWatch Origins Edition": gamePrice = 39.99; break;
                default:
                    System.out.println("Not Found");
                    gameName = scan.nextLine();
                    continue;
            }
            if (currentBalance>=gamePrice){
                currentBalance -= gamePrice;
                sumExpensesForBoughtGames +=gamePrice;
                System.out.printf("Bought %s%n", gameName);
            }else if (currentBalance<gamePrice){
                System.out.println("Too Expensive");
            }
            if (currentBalance == 0){
                System.out.println("Out of money!");
                break label;
            }
            gameName = scan.nextLine();
        }

        if (gameName.equals("Game Time")){
            System.out.printf("Total spent: $%.02f. Remaining: $%.02f", sumExpensesForBoughtGames, currentBalance);
        }

    }
}
