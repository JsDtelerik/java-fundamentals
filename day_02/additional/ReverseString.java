package day_02.additional;

import java.util.Scanner;

public class ReverseString {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String word = scan.nextLine();
        String password = scan.nextLine();

        StringBuilder reversedWord = new StringBuilder(word);
        reversedWord.reverse();


        String wReversed = reversedWord.toString();
        if (password.equals(wReversed)){
            System.out.println(wReversed);
        }


    }
}
