package day_02;

import java.util.Scanner;

public class RageExpenses {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int lostGames = Integer.parseInt(scan.nextLine());
        double headsetPrice = Double.parseDouble(scan.nextLine());
        double mousePrice = Double.parseDouble(scan.nextLine());
        double keyboardPrice = Double.parseDouble(scan.nextLine());
        double displayPrice = Double.parseDouble(scan.nextLine());

        double expenses = (headsetPrice*(lostGames/2))+(mousePrice*(lostGames/3))+(keyboardPrice*(lostGames/6))+(displayPrice*(lostGames/12));

        System.out.printf("Rage expenses: %.02f lv.", expenses);

    }
}
