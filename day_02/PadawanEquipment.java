package day_02;

import java.util.Scanner;

public class PadawanEquipment {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        double money = Double.parseDouble(scan.nextLine());
        int studentsCount = Integer.parseInt(scan.nextLine());
        double lightsaberPrice = Double.parseDouble(scan.nextLine());
        double robePrice = Double.parseDouble(scan.nextLine());
        double beltPrice = Double.parseDouble(scan.nextLine());
        double totalMoneyNeeded = (robePrice * studentsCount) + (lightsaberPrice * (Math.ceil(studentsCount * 1.10)) + (beltPrice * studentsCount) - (beltPrice * (studentsCount / 6)));
        if (money >= totalMoneyNeeded) {
            System.out.printf("The money is enough - it would cost %.2flv.", totalMoneyNeeded);
        } else {
            System.out.printf("Ivan Cho will need %.2flv more.", totalMoneyNeeded - money);
        }


    }
}
