package day_03;

import java.util.Scanner;

public class SpecialNumbers {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = Integer.parseInt(scan.nextLine());
        for (int i = 1; i <= n; i++) {
            int number = i;
            int keepNumber = number;
            int sum = 0;
            while (number > 0) {
                int number1 = number % 10;
                sum = sum + number1;
                number = number / 10;
            }
            if (sum == 5 || sum == 7 || sum == 11) {
                System.out.printf("%d -> True%n", keepNumber);
            } else {
                System.out.printf("%d -> False%n", keepNumber);
            }
        }
    }
}
