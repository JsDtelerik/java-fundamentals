package day_03;

import java.util.Scanner;

public class poundsToDollars {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        double britishPounds = Double.parseDouble(scan.nextLine());
        double usDollarsExchangeRate = 1.31;
        double britishPoundsToUsDollars = britishPounds*usDollarsExchangeRate;

        System.out.printf("%.03f", britishPoundsToUsDollars);
    }
}
