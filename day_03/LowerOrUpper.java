package day_03;

import java.util.Scanner;

public class LowerOrUpper {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        char enteredChar = scan.nextLine().charAt(0);

        if (enteredChar >=65 && enteredChar<=90){
            System.out.println("upper-case");
        }else if (enteredChar>= 97 && enteredChar<=122){
            System.out.println("lower-case");
        }

    }
}
