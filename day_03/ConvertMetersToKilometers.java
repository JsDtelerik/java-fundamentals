package day_03;

import java.util.Scanner;

public class ConvertMetersToKilometers {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int meters = Integer.parseInt(scan.nextLine());
        double kilometers = (1.0*meters/1000);
        System.out.printf("%.02f", kilometers);

    }
}
