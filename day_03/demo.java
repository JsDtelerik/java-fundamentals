package day_03;

import com.sun.jdi.connect.Connector;

import java.util.Scanner;

public class demo {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = Integer.parseInt(scanner.nextLine());


        boolean isSpecialNumber;
        for (int i = 1; i <= n; i++) {
            int number = i;
            int keepNumber = number;
            int sum = 0;
            while (number > 0) {
                sum += number % 10;
                number = number / 10;
            }
            isSpecialNumber = (sum == 5) || (sum == 7) || (sum == 11);



            System.out.printf("%d -> %b%n", keepNumber, isSpecialNumber);

        }
    }
}
