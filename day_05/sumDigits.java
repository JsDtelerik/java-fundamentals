package day_05;

import java.util.Scanner;

public class sumDigits {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = Integer.parseInt(scan.nextLine());
        int sumDigit = 0;
        while (n>0){
            int currentDigit = n%10;
            sumDigit += currentDigit;
            n /= 10;
        }
        System.out.print(sumDigit);
    }
}
