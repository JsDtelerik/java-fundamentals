package day_05.additional;

import java.util.Scanner;

public class fromLeftToTheRight {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int nCombinations = Integer.parseInt(scan.nextLine());
        int separatedNumber = Integer.MIN_VALUE;
        for (int i = 1; i <= nCombinations ; i++) {
            String combination = scan.nextLine();
            int keepNumber = 0;
            boolean isNegative = false;
            int round = 0;
            for (int j = 0; j < combination.length(); j++) {
                char symbol = combination.charAt(j);
                int parsedNumberToInt = Character.getNumericValue(symbol);

                if(symbol != 32){
                    if (symbol == 45){
                        isNegative = true;
                    }
                    if (parsedNumberToInt>0){
                        keepNumber +=parsedNumberToInt;
                    }
                }else{
                    if(isNegative){
                        keepNumber *=-1;
                        round++;
                        isNegative = false;

                    }
                    if (separatedNumber<keepNumber){
                        separatedNumber = keepNumber;
                        keepNumber = 0;
                    }
                }
            }
                if(isNegative && round>0){
                    keepNumber *=-1;
                }
            if (separatedNumber<keepNumber){
                separatedNumber = keepNumber;

            }
            System.out.println(Math.abs(separatedNumber));
            separatedNumber = Integer.MIN_VALUE;
        }
    }
}
