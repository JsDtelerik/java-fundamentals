package day_05.additional;

import java.util.Scanner;

public class fromLeftToright {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int n = Integer.parseInt(scan.nextLine());

        for (int i = 1; i <=n ; i++) {
            String input = scan.nextLine();
            int index = input.indexOf(" ");
            long firstNumber = Long.parseLong(input.substring(0, index));
            long secondNumber = Long.parseLong(input.substring(index + 1));

            long sum =0;
            long greater = 0;

            greater = Math.max(firstNumber, secondNumber);
            while(greater!=0){
                sum += greater%10;
                greater /=10;
            }

            System.out.println(Math.abs(sum));
        }

    }
}
