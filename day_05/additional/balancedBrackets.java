package day_05.additional;

import java.util.Scanner;

public class balancedBrackets {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int nLines = Integer.parseInt(scan.nextLine());
        boolean isBalanced = false;
        int countOpening = 0;
        int countClosing = 0;


        for (int i = 0; i <= nLines; i++) {
            String text = scan.nextLine();

            if (text.equals("(")) {
                countOpening++;
                if (countOpening - countClosing >= 2) {
                    isBalanced = false;
                    break;
                }
            } else if (text.equals(")")) {
                countClosing++;
            }


            if (countOpening == countClosing) {
                isBalanced = true;
            } else {
                isBalanced = false;
            }
            if (countClosing > countOpening) {
                isBalanced = false;
                break;

            }


        }
        if (isBalanced) {
            System.out.println("BALANCED");
        } else {
            System.out.println("UNBALANCED");
        }
    }
}