package day_05.additional;

import java.util.Scanner;

public class DataTypeFinder {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String input = scan.nextLine();
        String type = " ";
        while (!input.equals("END")) {
            if (input.equalsIgnoreCase("true") || input.equalsIgnoreCase("false")) {
                type = "boolean";
            } else if (input.length() == 1) {
                char symbol = input.charAt(0);
                if (symbol < 48 || symbol > 57) {
                    type = "character";
                } else {
                    type = "integer";
                }
            } else if (input.length() > 1) {

                for (int i = 0; i <= input.length() - 1; i++) {
                    char symbol = input.charAt(i);
                    char currentChar = symbol;
                    String combination = " ";
                    if(symbol != 45){
                        if (symbol >= 48 && symbol <= 57) {
                            if (!type.equalsIgnoreCase("floating point") && !type.equalsIgnoreCase("string")){
                                type = "integer";
                            }
                        } else if (symbol == 46 && !type.equalsIgnoreCase("string")) {
                            type = "floating point";
                        }else{
                            type = "string";
                        }

                    }

                }
            }
            System.out.printf("%s is %s type%n", input, type);
            type = "empty";
            input = scan.nextLine();
        }

    }
}
