package day_05;

import java.util.Scanner;

public class PokeMon {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int pokePowerN = Integer.parseInt(scan.nextLine());
        int distanceM = Integer.parseInt(scan.nextLine());
        int exhaustionFactorY = Integer.parseInt(scan.nextLine());
        int targetCounts = 0;
        int powerLeft = pokePowerN;
        double fiftyPercent = pokePowerN/2.0;
        label:
        while (powerLeft>=distanceM){
            if(distanceM == 0){
                break label;
            }
           powerLeft -= distanceM;
            targetCounts++;
           if ((powerLeft == fiftyPercent) && exhaustionFactorY>0){
               powerLeft /= exhaustionFactorY;
           }

        }

        System.out.println(powerLeft);
        System.out.println(targetCounts);
    }
}
