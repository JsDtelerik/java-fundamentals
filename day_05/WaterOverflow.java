package day_05;

import java.nio.charset.IllegalCharsetNameException;
import java.util.Scanner;

public class WaterOverflow {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int nLines = Integer.parseInt(scan.nextLine());
        int capacity = 255;
        int pourLitters = 0;
        for (int i = 1; i <= nLines ; i++) {
            int litters = Integer.parseInt(scan.nextLine());
            if(litters>capacity){
                System.out.println("Insufficient capacity!");
                continue;
            }
            capacity-=litters;
            pourLitters +=litters;
        }
        System.out.print(pourLitters);
    }
}
