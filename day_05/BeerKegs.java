package day_05;

import java.util.Scanner;

public class BeerKegs {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int numberKegs = Integer.parseInt(scan.nextLine());
        double volume = 0.0;
        double biggestKeg = Double.MIN_VALUE;
        String keepKegName = " ";
        for (int i = 1; i <=numberKegs ; i++) {
            String modelKeg = scan.nextLine();
            double kegRadius = Double.parseDouble(scan.nextLine());
            int kegHeight = Integer.parseInt(scan.nextLine());
            volume = Math.PI*(Math.pow(kegRadius, 2))*kegHeight;
            if (biggestKeg<volume){
                biggestKeg = volume;
                keepKegName = modelKeg;
            }
        }
        System.out.print(keepKegName);
    }
}
