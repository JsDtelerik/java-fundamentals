package day_05;

import java.util.Scanner;

public class Snowballs {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        byte snowballs = scan.nextByte();
        long snowballValue = 0;
        long bestSnoball = Integer.MIN_VALUE;
        short keepSnowballsSnow = 0;
        short keepSnowBallTime = 0;
        short keepSnowBallQuality = 0;
        for (byte i = 1; i <= snowballs; i++) {
            short snowballsSnow = scan.nextShort();
            short snowballsTime = scan.nextShort();
            short snowballsQuality = scan.nextShort();
            snowballValue = (long)(Math.pow((snowballsSnow/snowballsTime), snowballsQuality));

            if (snowballValue > bestSnoball){
                bestSnoball = snowballValue;
                keepSnowballsSnow = snowballsSnow;
                keepSnowBallTime = snowballsTime;
                keepSnowBallQuality = snowballsQuality;
            }

        }
        System.out.printf("%d : %d = %d (%d)", keepSnowballsSnow, keepSnowBallTime, bestSnoball, keepSnowBallQuality);
    }
}
