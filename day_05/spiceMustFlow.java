package day_05;

import java.util.Scanner;

public class spiceMustFlow {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int yieldPerDay = Integer.parseInt(scan.nextLine());
        int daysCounter = 0;
        int spice = 0;

        while (yieldPerDay>=100){
            spice += yieldPerDay;
            daysCounter++;
            yieldPerDay -= 10;
            spice -=26;
        }
        if (daysCounter>=1){
            spice -=26;
        }
        System.out.println(daysCounter);
        System.out.print(spice);
    }
}
