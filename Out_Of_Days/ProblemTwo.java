package Out_Of_Days_MidExamActTwo;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class ProblemTwo {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        List<String> products = Arrays.stream(scan.nextLine().split("!")).collect(Collectors.toList());

        String[] command = scan.nextLine().split(" ");

        while (!command[0].equals("Go")){

            switch (command[0]){
                case "Urgent":
                    products = addTheProductAtTheBeggingOfOurList(products, command[1]);
                    break;
                case "Unnecessary":
                    products = removeProductFromTheList(products, command[1]);
                    break;
                case "Correct":
                    products = addANewProductAndRemoveTheOldOneIfItExists(products, command[1], command[2]);
                    break;
                case "Rearrange":
                    products = rerangeProducts(products, command[1]);
                    break;
            }

            command = scan.nextLine().split(" ");

        }

        System.out.println(String.join(", ", products));

    }



    static List<String> addTheProductAtTheBeggingOfOurList(List<String> products, String productName) {
     if(!products.contains(productName)){
         products.add(0, productName);
     }
     return products;
    }

    static List<String> removeProductFromTheList(List<String> products, String productName) {

            products.remove(productName);


        return products;
    }


    static List<String> addANewProductAndRemoveTheOldOneIfItExists(List<String> products, String oldProduct, String newOne) {

        if(products.contains(oldProduct)){
            for (int i = 0; i < products.size(); i++) {
                if(products.get(i).equals(oldProduct)){
                    products.set(i, newOne);
                    break;
                }

            }
        }
        return products;
    }


    static List<String> rerangeProducts(List<String> products, String productName) {
        if(products.contains(productName)){
            products.remove(productName);
            products.add(productName);
        }
        return products;
    }



}
