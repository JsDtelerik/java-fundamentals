package Out_Of_Days_MidExamActTwo;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class ProblemThree {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        List<String> sentence = Arrays.stream(scan.nextLine().split(" ")).collect(Collectors.toList());
        String[] command = scan.nextLine().split(" ");

        while (!command[0].equals("Stop")) {

            switch (command[0].toLowerCase()) {
                case "delete":
                    sentence = removeTheWordAfterTheGivenIndex(sentence, Integer.parseInt(command[1]));
                    break;
                case "swap":
                    sentence = swapGivenWordsIfTheyExists(sentence, command[1], command[2]);
                    break;
                case "put":
                    sentence = addAWordBeforeTheGivenIndex(sentence, command[1], Integer.parseInt(command[2]));
                    break;
                case "sort":
                    sentence = sortWordByDescendingOrder(sentence);
                    break;
                case "replace":
                    sentence = replaceAWordIfItExists(sentence, command[1], command[2]);
                    break;

            }

            command = scan.nextLine().split(" ");
        }


        System.out.println(String.join(" ", sentence));

    }


    static List<String> removeTheWordAfterTheGivenIndex(List<String> sentence, int index) {

        if (index >= 0 && index < sentence.size() - 1) {
            sentence.remove(index + 1);
        } else if (index == -1) {
            sentence.remove(0);
        }
        return sentence;
    }

    static List<String> swapGivenWordsIfTheyExists(List<String> sentence, String wordOne, String wordTwo) {

        if (sentence.contains(wordOne) && sentence.contains(wordTwo)) {

            for (int i = 0; i < sentence.size(); i++) {
                if (sentence.get(i).equals(wordOne)) {

                    sentence.set(i, wordTwo);
                } else if (sentence.get(i).equals(wordTwo)) {

                    sentence.set(i, wordOne);
                }

            }

        }
        return sentence;
    }

    static List<String> addAWordBeforeTheGivenIndex(List<String> sentence, String word, int index) {

        if (index <= sentence.size() && index >= 0) {
            sentence.add(index - 1, word);
       /*} else if (index == sentence.size()) {
            sentence.add(word); */
        }

        return sentence;
    }

    static List<String> sortWordByDescendingOrder(List<String> sentence) {

        sentence.sort(Collections.reverseOrder());
        return sentence;
    }

    static List<String> replaceAWordIfItExists(List<String> sentence, String word, String replaceThisWord) {

        if (sentence.contains(replaceThisWord)) {
            for (int i = 0; i < sentence.size(); i++) {
                if (sentence.get(i).equals(replaceThisWord)) {
                    sentence.set(i, word);

                }
            }
        }
        return sentence;
    }

}