package Out_Of_Days_MidExamActTwo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class ThirdOne {

        public static void main(String[] args) {
            Scanner scan = new Scanner(System.in);
            List<String> collection = new ArrayList<>();
            String[] input = scan.nextLine().split(" ");

            for (int i = 0; i < input.length; i++) {
                collection.add(input[i]);
            }

            input = scan.nextLine().split(" ");

            while (!input[0].equals("Stop")) {

                switch (input[0]) {
                    case "Delete":
                        int indexToDelete = Integer.parseInt(input[1])+1;
                        if (collection.size() > indexToDelete && indexToDelete >= 0) {
                            collection.remove(indexToDelete);
                        }

                        break;
                    case "Swap":
                        String word1 = input[1];
                        String word2 = input[2];
                        if(collection.contains(word1)&&collection.contains(word2)) {
                            int indexWord1 = collection.indexOf(word1);
                            int indexWord2 = collection.indexOf(word2);
                            collection.remove(indexWord1);
                            collection.add(indexWord1,word2);
                            collection.remove(indexWord2);
                            collection.add(indexWord2,word1);
                        }

                        break;
                    case "Put":
                        int indexToPut = Integer.parseInt(input[2]) - 1;

                        if (collection.size() >= indexToPut && indexToPut >= 0) {
                            collection.add(indexToPut, input[1]);
                        }
                        break;
                    case "Replace":
                        String wordOne=input[1];
                        String wordTwo=input[2];
                        if(collection.contains(wordTwo))
                        {
                            int index=collection.indexOf(wordTwo);
                            collection.remove(wordTwo);
                            collection.add(index,wordOne);
                        }
                        break;
                    case "Sort":
                        Collections.sort(collection, Collections.reverseOrder());
                        break;
                }
                input = scan.nextLine().split(" ");
            }
            for (String output : collection) {
                System.out.print(output+" ");
            }
        }
    }

