package Out_Of_Days_MidExamActTwo;

import java.util.Scanner;

public class ProblemOne {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int daysOfTrip = Integer.parseInt(scan.nextLine());
        double budget = Double.parseDouble(scan.nextLine());
        int groupOfPeople = Integer.parseInt(scan.nextLine());
        double fuelPricePerKm = Double.parseDouble(scan.nextLine());
        double foodExpensesPerPersonPerDay = Double.parseDouble(scan.nextLine());
        double priceForOneRoomPerAPerson = Double.parseDouble(scan.nextLine());

        double priceForFoodPerAday  = foodExpensesPerPersonPerDay*groupOfPeople*daysOfTrip;
        double priceForAHotelRoom = groupOfPeople*priceForOneRoomPerAPerson*daysOfTrip;

        if(groupOfPeople>10){
            priceForAHotelRoom *= 0.75;
        }
        double foodAndHotelExpenses = priceForAHotelRoom+priceForFoodPerAday;
        double currentExpenses = foodAndHotelExpenses;

        boolean notEnoughMoney = false;
        int countDays = 0;



        for (int i = 1; i <= daysOfTrip; i++) {
            double travelledDistancePerADay = Double.parseDouble(scan.nextLine());
            double expensesForFuelPerDay = fuelPricePerKm*travelledDistancePerADay;

             currentExpenses += expensesForFuelPerDay;

            if(i % 3 == 0 || i % 5 == 0){
                currentExpenses *= 1.4;

            }
            if(i % 7 == 0){
                currentExpenses -= currentExpenses/groupOfPeople;

            }

            countDays++;

            if(budget<currentExpenses){
                notEnoughMoney = true;
                break;
            }

        }

        if(notEnoughMoney){
            System.out.printf("Not enough money to continue the trip. You need %.02f$ more.", currentExpenses-budget);
        }else{
            System.out.printf("You have reached the destination. You have %.02f$ budget left.", budget-currentExpenses);
        }


    }
}
