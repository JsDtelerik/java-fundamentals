package day_12_object.studentsSecondPart;

import day_12_object.studentsFirstPart.Student;

public class StudentActTwo {
    private String firstName;
    private String lastName;
    private int age;
    private String town;

    public StudentActTwo (String firstName, String lastName, int age, String town){
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.town = town;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }
}
