package day_12_object.studentsSecondPart;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        List<StudentActTwo> students = new ArrayList<>();
        String[] command = scan.nextLine().split("\\s+");

        while (!command[0].equals("end")){
            String firstName = command[0];
            String lastname = command[1];
            int age = Integer.parseInt(command[2]);
            String town = command[3];


               StudentActTwo student = students.stream().filter(e -> e.getFirstName().equals(firstName) && e.getLastName().equals(lastname)).
                        findFirst().orElse(null);

            if(student == null){
                StudentActTwo studentToAdd = new StudentActTwo(firstName, lastname, age, town);
                students.add(studentToAdd);
            }else{
                student.setFirstName(firstName);
                student.setLastName(lastname);
                student.setAge(age);
                student.setTown(town);

            }
            command = scan.nextLine().split("\\s+");
        }
        String townType = scan.nextLine();

        List<StudentActTwo> filtered = students.stream().filter(e -> e.getTown().equals(townType)).collect(Collectors.toList());

        for (StudentActTwo studentActTwo : filtered) {
            System.out.printf("%s %s is %d years old%n", studentActTwo.getFirstName(), studentActTwo.getLastName(), studentActTwo.getAge());

        }

    }

    static boolean check(List<StudentActTwo> students, String firstName, String lastName) {
        for (StudentActTwo student : students) {
            if(student.getFirstName().equals(firstName) && student.getLastName().equals(lastName)){
                return true;
            }
        }
        return false;
    }
}
