package day_12_object.songs;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = Integer.parseInt(scan.nextLine());
        List<Song> songs = new ArrayList<>();

        for (int i = 1; i <= n; i++) {
            String[] command = scan.nextLine().split("_");

            String type = command[0];
            String name = command[1];
            String time = command[2];
            Song song = new Song();
            song.setTypeList(type);
            song.setName(name);
            song.setTime(time);

            songs.add(song);
        }

        String typeList = scan.nextLine();

            List<Song> filterSong = songs.stream().filter(e -> e.getTypeList().equals(typeList)).collect(Collectors.toList());

        if(!typeList.equals("all")) {
            for (Song song1 : filterSong) {
                System.out.println(song1.getName());
            }
        }else{
            for (Song song2 : songs) {
                System.out.println(song2.getName());

            }
            }


    }
}
