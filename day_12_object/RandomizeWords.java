package day_12_object;
import java.util.Scanner;
import java.util.Random;

public class RandomizeWords {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String[] sentence = scan.nextLine().split(" ");
        Random random = new Random();

        for (int i = 0; i < sentence.length; i++) {
            int randomIndex = random.nextInt(sentence.length);
            String temp = sentence[i];
            sentence[i] = sentence[randomIndex];
            sentence[randomIndex] = temp;
        }

        for (String words : sentence) {
            System.out.println(words);

        }
    }
}
