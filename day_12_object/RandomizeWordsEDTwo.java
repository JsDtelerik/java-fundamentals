package day_12_object;

import java.util.Random;
import java.util.Scanner;

public class RandomizeWordsEDTwo {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        Random rnd = new Random();
        String[] words = scan.nextLine().split(" ");

        for (int i = 0; i < words.length; i++){
            int rndIndex = rnd.nextInt(words.length);
            String keepCurrentWord = words[i];
            words[i] = words[rndIndex];
            words[rndIndex]= keepCurrentWord;
        }

        for (String word : words) {
            System.out.println(word);

        }
    }
}
