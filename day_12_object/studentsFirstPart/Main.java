package day_12_object.studentsFirstPart;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        List<Student> students = new ArrayList<>();

        String[] command = scan.nextLine().split("\\s+");
        while (!command[0].equals("end")){
            String firstName = command[0];
            String lastname = command[1];
            int age = Integer.parseInt(command[2]);
            String town = command[3];


            Student student = students.stream().filter(e -> e.getFirstName().equals(firstName) && e.getLastName().equals(lastname)).
                    findFirst().orElse(null);

            if(student == null){
                Student studentToAdd = new Student(firstName, lastname, age, town);
                students.add(studentToAdd);
            }else{
                student.setFirstName(firstName);
                student.setLastName(lastname);
                student.setAge(age);
                student.setTown(town);

            }

            command = scan.nextLine().split("\\s+");
        }
        String filterCity = scan.nextLine();
        List<Student> filtered = students.stream().filter(e -> e.getTown().equals(filterCity)).collect(Collectors.toList());
        for (Student student : filtered) {
            System.out.printf("%s %s is %d years old%n", student.getFirstName(), student.getLastName(), student.getAge());

        }


    }
}
