package day_09_methods;

import java.util.Arrays;
import java.util.Scanner;


public class arrayManipulator {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String[] numbers = scan.nextLine().split(" ");
        String command = scan.nextLine();
        while (!command.equals("end")) {
            String[] arrCommandLine = command.split(" ");
            String mainCommand = arrCommandLine[0];

            switch (mainCommand) {
                case "exchange":
                    String commandIndex = arrCommandLine[1];
                    int index = Integer.parseInt(commandIndex);
                    if (index+1 <= 0 || index >= numbers.length) {
                        System.out.println("Invalid index");
                    } else {
                        String[] exchangedArray = new String[numbers.length];
                        int innerIndex = 0;
                        for (int j = index + 1; j < numbers.length; j++) {
                            exchangedArray[innerIndex++] = numbers[j];
                        }


                        for (int i = 0; i <= index; i++) {
                            exchangedArray[innerIndex++] = numbers[i];
                        }

                        numbers = exchangedArray;
                    }

                    break;
                case "max":
                    String maxNumEvenOrOdd = arrCommandLine[1];
                    if (maxNumEvenOrOdd.equals("even")) {
                        int[] transformStringArrNumbersToInt = Arrays.stream(numbers).mapToInt(Integer::parseInt).toArray();
                        int keepMaxEvenIndex = 0;
                        int keptMaxValueEven = Integer.MIN_VALUE;
                        boolean isFound = false;
                        for (int i = 0; i < numbers.length; i++) {
                            int numMaxEven = transformStringArrNumbersToInt[i];
                            if (numMaxEven % 2 == 0 && keptMaxValueEven <= numMaxEven) {
                                keepMaxEvenIndex = i;
                                keptMaxValueEven = numMaxEven;
                                isFound = true;
                            }
                        }
                        if (isFound) {
                            System.out.println(keepMaxEvenIndex);
                        } else {
                            System.out.println("No matches");
                        }

                    } else if (maxNumEvenOrOdd.equals("odd")) {
                        int[] transformNumArrToInt = Arrays.stream(numbers).mapToInt(Integer::parseInt).toArray();
                        int keepMaxOddIndex = 0;
                        int keptMaxValueOdd = Integer.MIN_VALUE;
                        boolean oddIsFound = false;
                        for (int i = 0; i < transformNumArrToInt.length; i++) {
                            int numMaxOdd = transformNumArrToInt[i];
                            if (numMaxOdd % 2 != 0 && keptMaxValueOdd <= numMaxOdd) {
                                keepMaxOddIndex = i;
                                keptMaxValueOdd = numMaxOdd;
                                oddIsFound = true;
                            }

                        }
                        if (oddIsFound) {
                            System.out.println(keepMaxOddIndex);
                        } else {
                            System.out.println("No matches");
                        }

                    }
                    break;
                case "min":
                    String minNumEvenOrOdd = arrCommandLine[1];
                    if (minNumEvenOrOdd.equals("even")) {
                        int[] transformMinEven = Arrays.stream(numbers).mapToInt(Integer::parseInt).toArray();
                        int keepMinEvenIndex = 0;
                        int keptNumMinEvenValue = Integer.MAX_VALUE;
                        boolean isFound = false;
                        for (int i = 0; i < numbers.length; i++) {
                            int numMinEven = transformMinEven[i];
                            if (numMinEven % 2 == 0 && numMinEven <= keptNumMinEvenValue) {
                                keepMinEvenIndex = i;
                                keptNumMinEvenValue = numMinEven;
                                isFound = true;
                            }
                        }
                        if (isFound) {
                            System.out.println(keepMinEvenIndex);
                        } else {
                            System.out.println("No matches");
                        }

                    } else if (minNumEvenOrOdd.equals("odd")) {
                        int[] transformForMinOddValues = Arrays.stream(numbers).mapToInt(Integer::parseInt).toArray();
                        int keepMinOddIndex = 0;
                        int keptNumMinOddValue = Integer.MAX_VALUE;
                        boolean oddIsFound = false;
                        for (int i = 0; i < transformForMinOddValues.length; i++) {
                            int numMaxOdd = transformForMinOddValues[i];
                            if (numMaxOdd % 2 != 0 && numMaxOdd <= keptNumMinOddValue) {
                                keepMinOddIndex = i;
                                keptNumMinOddValue = numMaxOdd;
                                oddIsFound = true;
                            }

                        }
                        if (oddIsFound) {
                            System.out.println(keepMinOddIndex);
                        } else {
                            System.out.println("No matches");
                        }

                    }

                    break;
                case "first":
                    String firstCountCommand = arrCommandLine[1];
                    int countCommandToInt = Integer.parseInt(firstCountCommand);
                    if (countCommandToInt > numbers.length) {
                        System.out.println("Invalid count");
                        break;
                    }
                    String firstOddOrEvenCommand = arrCommandLine[2];

                    if (firstOddOrEvenCommand.equals("even")) {
                        String valueOfFoundNumbers = "";
                        boolean isFound = false;
                        int counterFoundFirstEvenElements = 0;
                        int[] transformForTheFirstCountEvenElement = Arrays.stream(numbers).mapToInt(Integer::parseInt).toArray();
                        for (int i = 0; i < transformForTheFirstCountEvenElement.length; i++) {
                            int firstCountEvenNum = transformForTheFirstCountEvenElement[i];
                            if (firstCountEvenNum % 2 == 0 && counterFoundFirstEvenElements < countCommandToInt) {
                                counterFoundFirstEvenElements++;
                                valueOfFoundNumbers += firstCountEvenNum + " ";
                                isFound = true;
                            }

                        }
                        if (isFound) {
                            String[] firstEvenReadyForPrint = valueOfFoundNumbers.split(" ");
                            System.out.println("[" + String.join(", ", firstEvenReadyForPrint) + "]");

                        } else {
                            System.out.println("[]");
                        }

                    } else if (firstOddOrEvenCommand.equals("odd")) {
                        String valueOfFoundNumbers = "";
                        boolean isFound = false;
                        int counterFoundFirstOddElements = 0;
                        int[] transformForTheFirstCountOddElement = Arrays.stream(numbers).mapToInt(Integer::parseInt).toArray();
                        for (int i = 0; i < transformForTheFirstCountOddElement.length; i++) {
                            int firstCountEvenNum = transformForTheFirstCountOddElement[i];
                            if (firstCountEvenNum % 2 != 0 && counterFoundFirstOddElements < countCommandToInt) {
                                counterFoundFirstOddElements++;
                                valueOfFoundNumbers += firstCountEvenNum + " ";
                                isFound = true;
                            }

                        }
                        if (isFound) {
                            String[] firstOddReadyForPrint = valueOfFoundNumbers.split(" ");
                            System.out.println("[" + String.join(", ", firstOddReadyForPrint) + "]");

                        } else {
                            System.out.println("[]");
                        }

                    }


                    break;
                case "last":
                    String lastCountCommand = arrCommandLine[1];
                    int countCommandToIntForLast = Integer.parseInt(lastCountCommand);
                    if (countCommandToIntForLast > numbers.length) {
                        System.out.println("Invalid count");
                        break;
                    }
                    String lastOddOrEvenCommand = arrCommandLine[2];

                    if (lastOddOrEvenCommand.equals("even")) {
                        String valueOfFoundNumbers = "";
                        boolean isFound = false;
                        int counterFoundLastEvenElements = 0;
                        int[] transformForTheFirstCountEvenElement = Arrays.stream(numbers).mapToInt(Integer::parseInt).toArray();
                        for (int i = transformForTheFirstCountEvenElement.length; i > 0; i--) {
                            int lastCountEvenNum = transformForTheFirstCountEvenElement[i - 1];
                            if (lastCountEvenNum % 2 == 0 && counterFoundLastEvenElements < countCommandToIntForLast) {
                                counterFoundLastEvenElements++;
                                valueOfFoundNumbers += lastCountEvenNum + " ";
                                isFound = true;
                            }

                        }
                        if (isFound) {
                            String[] lastEvenForReverse = valueOfFoundNumbers.split(" ");
                            String[] reversedLastEven = new String[lastEvenForReverse.length];
                            int count = 0;
                            for (int i = lastEvenForReverse.length; i > 0 ; i--) {
                                reversedLastEven[count++] = lastEvenForReverse[i-1];
                            }

                            System.out.println("[" + String.join(", ", reversedLastEven) + "]");

                        } else {
                            System.out.println("[]");
                        }


                    }else if (lastOddOrEvenCommand.equals("odd")) {
                        String valueOfFoundNumbers = "";
                        boolean isFound = false;
                        int counterFoundLasttOddElements = 0;
                        int[] transformForTheLastCountOddElement = Arrays.stream(numbers).mapToInt(Integer::parseInt).toArray();
                        for (int i = transformForTheLastCountOddElement.length; i >0 ; i--) {
                            int firstCountEvenNum = transformForTheLastCountOddElement[i-1];
                            if (firstCountEvenNum % 2 != 0 && counterFoundLasttOddElements < countCommandToIntForLast) {
                                counterFoundLasttOddElements++;
                                valueOfFoundNumbers += firstCountEvenNum + " ";
                                isFound = true;
                            }

                        }
                        if (isFound) {
                            String[] lastOddReadyForPrint = valueOfFoundNumbers.split(" ");
                            String[] reversedLastOdd = new String[lastOddReadyForPrint.length];
                            int count = 0;
                            for (int i = lastOddReadyForPrint.length; i > 0 ; i--) {
                                reversedLastOdd[count++] = lastOddReadyForPrint[i-1];
                            }
                            System.out.println("[" + String.join(", ", reversedLastOdd) + "]");

                        } else {
                            System.out.println("[]");
                        }

                    }
                    break;


            }
            command = scan.nextLine();
        }
        System.out.println("["+String.join(", ", numbers)+"]");
    }

}
