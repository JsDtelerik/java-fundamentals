package day_09_methods;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class addAndSubtract {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        List<Integer> numbers = new ArrayList<>();
        numbers.add(0, Integer.parseInt(scan.nextLine()));
        numbers.add(1, Integer.parseInt(scan.nextLine()));
        numbers.add(2, Integer.parseInt(scan.nextLine()));

        int calculations = calculationsSumOfNumOneAndNumTwoSubtractThrird(numbers);
        printResult(calculations);
    }


    static int calculationsSumOfNumOneAndNumTwoSubtractThrird(List<Integer> numbers) {
        int sum = (numbers.get(0) + numbers.get(1))-numbers.get(2);
        return sum;
    }


    private static void printResult(int calculations) {
        System.out.println(calculations);
    }
}
