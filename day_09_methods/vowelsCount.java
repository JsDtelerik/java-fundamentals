package day_09_methods;

import java.util.Scanner;

public class vowelsCount {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        String word = scan.nextLine();


        printVowelContainedInTheWord(word);

    }

    private static void printVowelContainedInTheWord(String word) {
        int vowelCounter = 0;
        for (int i = 0; i < word.length(); i++) {
            char symbol = word.charAt(i);
            String convertedSymbol = String.valueOf(symbol);

        switch (convertedSymbol.toLowerCase()) {
            case "a":
            case "e":
            case "i":
            case "o":
            case "u":
                vowelCounter++;
                break;
        }
        }
        System.out.print(vowelCounter);

    }
}
