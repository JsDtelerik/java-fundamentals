package day_09_methods;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class factorialDivision {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        long firstNumber = Integer.parseInt(scan.nextLine());
        long secondNumber = Integer.parseInt(scan.nextLine());

        long factorialOfFirstNumber = calculateFactorialOfFirstNumber(firstNumber);
        long factorialOfSecondNumber = calculateFactorialOfSecondNumber(secondNumber);
        printDivedResult(factorialOfFirstNumber, factorialOfSecondNumber);

    }


    static long calculateFactorialOfFirstNumber(long firstNumber) {
        long calculateFactorial = 1;
        if (firstNumber == 0) {
            firstNumber = 1;
        }
        for (int i = 1; i <= firstNumber; i++) {
            calculateFactorial *= i;
        }
        return calculateFactorial;
    }

    static long calculateFactorialOfSecondNumber(long secondNumber) {
        long calculateFactorial = 1;
        if (secondNumber == 0) {
            secondNumber = 1;
        }
        for (int i = 1; i <= secondNumber; i++) {
            calculateFactorial *= i;

        }
        return calculateFactorial;
    }

    private static void printDivedResult(long factorialOfFirstNumber, long factorialOfSecondNumber) {

        double result = 1.0 * factorialOfFirstNumber / factorialOfSecondNumber;

        System.out.printf("%.02f", result);
    }

}
