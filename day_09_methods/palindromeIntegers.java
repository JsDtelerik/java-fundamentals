package day_09_methods;

import java.util.List;
import java.util.Scanner;

public class palindromeIntegers {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        String command = scan.nextLine();

        while (!command.equals("END")){
            String number = command;
            String reverseNumb = reverseNumber(command);
            boolean compare =  compareBothNumbers(number,reverseNumb);
            System.out.println(compare);
            command = scan.nextLine();
        }
    }


    static String reverseNumber(String command) {
       String intToString = command;
       StringBuilder reverseCommand = new StringBuilder(intToString);
       reverseCommand.reverse();
       return reverseCommand.toString();
    }
    static boolean compareBothNumbers(String number, String reverseNumb) {
        if (number.equals(reverseNumb)){
            return true;
        }else{
            return false;
        }
    }

}
