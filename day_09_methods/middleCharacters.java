package day_09_methods;

import java.util.Scanner;

public class middleCharacters {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String text = scan.nextLine();

        if(text.length()%2==0){
            printEvenMiddleCharater(text);

        }else{

            printOddMiddleCharacter(text);

        }


    }


    private static void printEvenMiddleCharater(String text) {
        String keepElements = "";
        for (int i = (text.length()/2)-1; i <=text.length()/2 ; i++) {
            char symbol = text.charAt(i);
            keepElements+=symbol;
        }
        System.out.print(keepElements);
    }
    private static void printOddMiddleCharacter(String text) {
        String middleElement = "";
        for (int i = text.length()/2; i <=text.length()/2 ; i++) {
            char symbol = text.charAt(i);
            middleElement += symbol;
        }
        System.out.print(middleElement);
    }

}
