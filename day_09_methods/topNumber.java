package day_09_methods;

import java.util.Scanner;

public class topNumber {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = Integer.parseInt(scan.nextLine());

        for (int i = 16; i <=n; i++) {
            boolean divisibleByEight = checkThePossibilityToDivideIByEight(i);
            boolean holdsAtLeastOneOddDigit = chekIfIHoldsAtLeastOneOddDigit(i);
            if(divisibleByEight && holdsAtLeastOneOddDigit){
                System.out.println(i);
            }

        }
    }



    private static boolean checkThePossibilityToDivideIByEight(int i) {
        int iDigits =0;
        while(i>0){
            int lastDigit = i%10;
            iDigits +=lastDigit;
            i /=10;
        }
        if(iDigits %8 ==0){
            return true;
        }else{
            return false;
        }
    }
    static boolean chekIfIHoldsAtLeastOneOddDigit(int i) {
        while(i>0){
            int lastDigit = i %10;

            if(lastDigit % 2 != 0){
                return true;
            }
            i /= 10;
        }
        return false;
    }
}
