package day_09_methods;

import java.util.Scanner;

public class passwordValidator {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        String password = scan.nextLine();

        if(passwordIsvalid(password)){
            System.out.println("Password is valid");
        }

    }

    static boolean passwordIsvalid(String password) {
        boolean chekLength = checkPasswordLength(password);
        boolean chekConsist = checkPasswordConsist(password);
        boolean checkDigits = chekPasswordDigits(password);

        if (chekLength && chekConsist && checkDigits){
            return true;
        }
        return false;
    }



    static boolean checkPasswordLength(String password) {
        if(password.length()>=6 && password.length()<=10){
            return  true;
        }else{
            System.out.println("Password must be between 6 and 10 characters");
            return  false;
        }
    }

    static boolean checkPasswordConsist(String password) {
        for (int i = 0; i < password.length(); i++) {
            char symbol = password.charAt(i);
            boolean number = symbol>=48 && symbol<=57;
            boolean capitalLetter = symbol>=65 && symbol<=90;
            boolean smallLetter = symbol>=97 && symbol<=122;
            if (!number && !capitalLetter && !smallLetter){
                System.out.println("Password must consist only of letters and digits");
                return  false;
            }

        }
        return true;
    }

    static boolean chekPasswordDigits(String password) {
        int digitCounter = 0;
        for (int i = 0; i < password.length(); i++) {
            char symbol = password.charAt(i);
            if (symbol>=48 && symbol<=57){
                digitCounter++;
                if (digitCounter>=2){
                    return  true;
                }
            }
        }
        System.out.println("Password must have at least 2 digits");
        return false;
    }
}
