package day_09_methods;

import java.util.Scanner;

public class tryToPrint {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        String[] elements = scan.nextLine().split(" ");

        for (String element : elements) {
            System.out.println(element);

        }
    }
}
