package day_09_methods;

import java.util.Scanner;

public class charactersInRange {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        char firstSymbol = scan.nextLine().charAt(0);
        char secondSymbol = scan.nextLine().charAt(0);



        printCharaterInRange(findLowerChar(firstSymbol, secondSymbol),findHigherChar(firstSymbol, secondSymbol));

    }

    private static void printCharaterInRange(char lowerChar, char higherChar) {
        int beginIndex = (int) lowerChar;
        int endIndex = (int) higherChar;

        for (int i = beginIndex+1; i < endIndex; i++) {
            System.out.printf("%c ", i);

        }
    }

    static char findLowerChar(char firstSymbol, char secondSymbol) {
     if (firstSymbol<secondSymbol){
         return firstSymbol;
     }else {
         return secondSymbol;
     }
    }
    static char findHigherChar(char firstSymbol, char secondSymbol) {
        if (firstSymbol>secondSymbol){
            return firstSymbol;
        }else {
            return  secondSymbol;
        }
    }
}
