package day_09_methods;

import java.util.Scanner;

public class nXnMatrix {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = Integer.parseInt(scan.nextLine());

        for (int i = 1; i <=n ; i++) {
            printLines(n);

        }
    }

    private static void printLines(int n) {
        for (int i = 1; i <=n ; i++) {
            System.out.printf("%d ", n);

        }
        System.out.println();
    }
}
