package day_09_methods;

import java.sql.ClientInfoStatus;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class smallestOfThreeNumbers {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        List<Integer> numbers = new ArrayList<>();
        numbers.add(Integer.parseInt(scan.nextLine()));
        numbers.add(Integer.parseInt(scan.nextLine()));
        numbers.add(Integer.parseInt(scan.nextLine()));

        Collections.sort(numbers);

        printSmallestNumber(numbers);
    }

    private static void printSmallestNumber(List<Integer> numbers) {
        System.out.print(numbers.get(0));
    }
}
