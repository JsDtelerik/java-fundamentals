package day_19_RegularExpressions;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Furniture {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        String input = scan.nextLine();
        String regex = ">>(?<furniture>[A-zZ-a]+)<<(?<price>[0-9]+\\.?[0-9]*)!(?<quantity>[0-9]+)";
        Pattern pattern = Pattern.compile(regex);

        List<String> addedFurniture = new ArrayList<>();
        double totalSum = 0.0;
        while(!input.equals("Purchase")){
            Matcher matcher = pattern.matcher(input);
            if(matcher.find()){
            addedFurniture.add(matcher.group(("furniture")));
            totalSum += Double.parseDouble(matcher.group(("price"))) * Integer.parseInt(matcher.group("quantity"));

            }

            input = scan.nextLine();
        }
        System.out.println("Bought furniture:");
        addedFurniture.forEach(System.out::println);
        System.out.printf("Total money spend: %.02f", totalSum);
    }
}
