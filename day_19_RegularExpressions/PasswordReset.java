package day_19_RegularExpressions;

import java.util.Scanner;

public class PasswordReset {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String password = scan.nextLine();
        String[] input = scan.nextLine().split("\\s+");
        while (!input[0].equals("Done")) {
            switch (input[0]) {
                case "TakeOdd":
                    password = takeOddIndexFromOurPassword(password);
                    break;
                case "Cut":
                    password = cutFromIndexToGivenLength(password, Integer.parseInt(input[1]), Integer.parseInt(input[2]));
                    break;
                case "Substitute":
                    password = substituteSubstringIfItexists(password, input[1], input[2]);
                    break;
            }

            input = scan.nextLine().split("\\s+");

        }

        System.out.printf("Your password is: %s", password);
    }



    static String takeOddIndexFromOurPassword(String password) {
        StringBuilder editedPassword = new StringBuilder();
        for (int i = 0; i < password.length(); i++) {

            if (i % 2 != 0) {
                char symbol = password.charAt(i);
                editedPassword.append(symbol);
            }
        }

        System.out.println(editedPassword);
        return editedPassword.toString();
    }

    static String cutFromIndexToGivenLength(String password, int index, int length) {
        StringBuilder editedPassword = new StringBuilder(password);
        editedPassword.delete(index, index+length);
        System.out.println(editedPassword);
        return editedPassword.toString();
    }

    static String substituteSubstringIfItexists(String password, String substring, String replacement) {
        String editedPassword = password;
        if(password.contains(substring)){
            editedPassword = editedPassword.replace(substring, replacement);
            System.out.println(editedPassword);
        }else{
            System.out.println("Nothing to replace!");
        }
        return  editedPassword;
    }



}