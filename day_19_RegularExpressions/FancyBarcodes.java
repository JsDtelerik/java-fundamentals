package day_19_RegularExpressions;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FancyBarcodes {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = Integer.parseInt(scan.nextLine());
        String barcodeRegex = "@#+(?<barcode>([A-Z][A-Za-z0-9]{4,}[A-Z]))@#+";
        Pattern barcodePattern = Pattern.compile(barcodeRegex);
        String productGroupRegex = "[0-9]";
        Pattern productGroupPattern = Pattern.compile(productGroupRegex);
        for (int i = 1; i <=n ; i++) {
            String currentBarcode = scan.nextLine();
            Matcher matcher = barcodePattern.matcher(currentBarcode);
            if(matcher.find()){
                StringBuilder productGroup = new StringBuilder();
                Matcher productMatcher = productGroupPattern.matcher(matcher.group());
                boolean matched = false;
                while (productMatcher.find()){
                    productGroup.append(productMatcher.group());
                    matched = true;
                }
                if(!matched){
                    System.out.printf("Product group: 00%n");
                }else{
                    System.out.printf("Product group: %s%n", productGroup.toString());
                }

            }else{
                System.out.printf("Invalid barcode%n");
            }
        }


    }
}
