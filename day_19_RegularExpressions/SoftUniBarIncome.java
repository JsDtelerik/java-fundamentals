package day_19_RegularExpressions;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SoftUniBarIncome {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String regex = "%(?<buyer>[A-Z][a-z]+)%([^|$%.]*)<(?<item>\\w+)>([^|$%.]*)\\|(?<quantity>\\d+)\\|([^|$%.]*?)(?<price>[0-9]*\\.?[0-9]*)\\$";
        Pattern pattern = Pattern.compile(regex);
        String input = scan.nextLine();
        double profit = 0.0;
        while (!input.equals("end of shift")){
            Matcher matcher = pattern.matcher(input);
            if(matcher.find()){
                double currentSaleProfit = Integer.parseInt(matcher.group("quantity"))* Double.parseDouble(matcher.group("price"));
                profit += currentSaleProfit;
                System.out.printf("%s: %s - %.02f%n", matcher.group("buyer"), matcher.group("item"), currentSaleProfit);
            }

            input = scan.nextLine();
        }
        System.out.printf("Total income: %.02f", profit);
    }
}
