package day_19_RegularExpressions;

import java.util.*;


public class HeroesOfCodeAndLogicVII {

    static class Hero {
        private String name;
        private Integer hitPoint;
        private Integer manaPoint;

        public Hero(String name, Integer hitPoint, Integer manaPoint){
            this.name = name;
            this.hitPoint = hitPoint;
            this.manaPoint = manaPoint;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Integer getHitPoint() {
            return hitPoint;
        }

        public void setHitPoint(Integer hitPoint) {
            this.hitPoint = hitPoint;
        }
        public Integer getManaPoint() {
            return manaPoint;
        }

        public void setManaPoint(Integer manaPoint) {
            this.manaPoint = manaPoint;
        }

        @Override
        public String toString() {
            return String.format("%s%n  HP: %d%n  MP: %d%n",getName(), getHitPoint(), getManaPoint()) ;

        }


    }

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int n = Integer.parseInt(scan.nextLine());
        Map<String, Hero> heroes = new TreeMap<>();

        for (int i = 1; i <= n; i++) {
            String[] input = scan.nextLine().split("\\s+");
            int hitPoint = 0;
            int manaPoint = 0;
            if(Integer.parseInt(input[1])>=100){
                hitPoint = 100;
            }else{
                hitPoint = Integer.parseInt(input[1]);
            }

            if(Integer.parseInt(input[2])>=200){
                manaPoint = 200;
            }else{
                manaPoint = Integer.parseInt(input[2]);
            }
            Hero hero = new Hero(input[0], hitPoint, manaPoint);
            heroes.put(input[0], hero);
        }


        String[] command = scan.nextLine().split(" - ");

        while (!command[0].equals("End")){
            switch (command[0]){
                case "CastSpell":
                    heroes = castSpells(heroes, command[1], Integer.parseInt(command[2]), command[3]);
                    break;
                case "TakeDamage":
                    heroes = takeDamage(heroes, command[1], Integer.parseInt(command[2]), command[3]);
                    break;
                case "Recharge":
                    heroes = rechargeHero(heroes, command[0], command[1], Integer.parseInt(command[2]));
                    break;
                case "Heal":
                    heroes = rechargeHero(heroes, command[0], command[1], Integer.parseInt(command[2]));
                    break;

            }

            command = scan.nextLine().split("\\s+-\\s+");
        }
        heroes.entrySet().stream().sorted((a, b) -> b.getValue().getHitPoint().compareTo(a.getValue().getHitPoint()))
               .forEach(stringHeroEntry -> {
                    System.out.print(stringHeroEntry.getValue());
                });


    }



    static Map<String, Hero> castSpells(Map<String, Hero> heroes, String heroName, int manaPointNeeded, String spellName) {

        if(heroes.get(heroName).getManaPoint()>=manaPointNeeded){
            heroes.get(heroName).setManaPoint(heroes.get(heroName).getManaPoint()-manaPointNeeded);
            System.out.printf("%s has successfully cast %s and now has %d MP!%n", heroName, spellName, heroes.get(heroName).getManaPoint());
        }else{
            System.out.printf("%s does not have enough MP to cast %s!%n", heroName, spellName);
        }
        return heroes;
    }


    static Map<String, Hero> takeDamage(Map<String, Hero> heroes, String heroName, int damage, String attacker) {
        if(heroes.get(heroName).getHitPoint()>damage){
            heroes.get(heroName).setHitPoint(heroes.get(heroName).getHitPoint()-damage);
            System.out.printf("%s was hit for %d HP by %s and now has %d HP left!%n", heroName, damage, attacker, heroes.get(heroName).getHitPoint());
        }else{
            System.out.printf("%s has been killed by %s!%n", heroName, attacker);
            heroes.remove(heroName);
        }
        return heroes;
    }

    static Map<String, Hero> rechargeHero(Map<String, Hero> heroes, String command, String heroName, int amount) {
        if(command.equals("Recharge")){
            if(amount+heroes.get(heroName).getManaPoint()>=200){
                int amountManaToBeAdded = 200-heroes.get(heroName).getManaPoint();
                heroes.get(heroName).setManaPoint(heroes.get(heroName).getManaPoint()+amountManaToBeAdded);
                System.out.printf("%s recharged for %d MP!%n", heroName, amountManaToBeAdded);
            }else{
                heroes.get(heroName).setManaPoint(heroes.get(heroName).getManaPoint()+amount);
                System.out.printf("%s recharged for %d MP!%n", heroName, amount);
            }
        }else if(command.equals("Heal")){
            if(amount+heroes.get(heroName).getHitPoint()>=100){
                int amountManaToBeAdded = 100-heroes.get(heroName).getHitPoint();
                heroes.get(heroName).setHitPoint(heroes.get(heroName).getHitPoint()+amountManaToBeAdded);
                System.out.printf("%s healed for %d HP!%n", heroName, amountManaToBeAdded);
            }else{
                heroes.get(heroName).setHitPoint(heroes.get(heroName).getHitPoint()+amount);
                System.out.printf("%s healed for %d HP!%n", heroName, amount);
            }
        }
        return heroes;
    }


}
