package day_19_RegularExpressions;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class NetherRealms {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        List<String> demons = Arrays.stream(scan.nextLine().split("\\s*,\\s*")).collect(Collectors.toList());
        List<String> sortedDemons = demons.stream().sorted(String::compareTo).collect(Collectors.toList());
        for (int i = 0; i < sortedDemons.size(); i++) {

            printCurrentDemonStatistic(sortedDemons.get(i));
        }


    }

    static void printCurrentDemonStatistic(String demon) {
        int demonHealth = calculateDemonHealth(demon);
        double demonDamage = calculateDemonDamage(demon);
        System.out.printf("%s - %d health, %.02f damage%n", demon, demonHealth, demonDamage);

    }
    static int calculateDemonHealth(String demon) {
        String regexHealth = "[^0-9+,\\-*\\/.]";
        Pattern pattern = Pattern.compile(regexHealth);
        Matcher matcher = pattern.matcher(demon);

        StringBuilder demonCharacter = new StringBuilder();
        while (matcher.find()){
            demonCharacter.append(matcher.group());
        }
        demonCharacter.toString();
        int health = 0;
        for (int i = 0; i < demonCharacter.length(); i++) {
            char symbol = demonCharacter.charAt(i);
            health += symbol;

        }
        return health;
    }

    static double calculateDemonDamage(String demon) {
        String digitRegex = "([+-]?[0-9]+\\.?[0-9]*)";
        Pattern digitPatter = Pattern.compile(digitRegex);
        Matcher digitMatcher = digitPatter.matcher(demon);
        double damage = 0.0;
        while (digitMatcher.find()){
            damage = damage+(Double.parseDouble(digitMatcher.group()));
        }
        String specialRegex = "[\\*]|[\\/]";
        Pattern special = Pattern.compile(specialRegex);
        Matcher specialMatcher = special.matcher(demon);
        while (specialMatcher.find()){
            switch (specialMatcher.group()){
                case "*":
                    damage = damage*2;
                    break;
                case "/":
                    damage = damage/2;
                    break;
            }
        }
        return damage;
    }

}
