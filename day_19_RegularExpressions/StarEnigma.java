package day_19_RegularExpressions;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StarEnigma {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = Integer.parseInt(scan.nextLine());
        String starRegex = "[SsTtAaRr]";
        Pattern starPattern = Pattern.compile(starRegex);

        List<String> decryptedMessages = new ArrayList<>();
        for (int i = 1; i <= n; i++) {
            String input = scan.nextLine();
            Matcher starMatcher = starPattern.matcher(input);
            int count = 0;
            while (starMatcher.find()){
                count++;
            }
            StringBuilder decryptedMessage = new StringBuilder();
            for (int j = 0; j < input.length(); j++) {
                char symbol = (char)(input.charAt(j)-count);

                decryptedMessage.append(symbol);
            }

            decryptedMessages.add(decryptedMessage.toString());
        }

        List<String> attackedPlanets = new ArrayList<>();
        List<String> destroyedPlanets = new ArrayList<>();
        String decryptPlanetsRegex = "@(?<planetName>[A-Za-z]+)([^@!:>-]*):(?<population>\\d+)([^@!:>-]*)!(?<attackType>[AD])!([^@!:>-]*)->(?<soldiers>\\d+)";
        Pattern decryptPlanetsPattern =  Pattern.compile(decryptPlanetsRegex);

        for (int i = 0; i < decryptedMessages.size(); i++) {
            String message = decryptedMessages.get(i);
            Matcher decryptPlanetsMatcher = decryptPlanetsPattern.matcher(message);
            if(decryptPlanetsMatcher.find()){
                switch (decryptPlanetsMatcher.group("attackType")){
                    case "A":
                        attackedPlanets.add(decryptPlanetsMatcher.group("planetName"));
                        break;
                    case "D":
                        destroyedPlanets.add(decryptPlanetsMatcher.group("planetName"));
                        break;
                }
            }
        }

        System.out.printf("Attacked planets: %d%n", attackedPlanets.size());
        attackedPlanets.stream().sorted(String::compareTo).forEach(planet -> {
            System.out.printf("-> %s%n", planet);
        });
        System.out.printf("Destroyed planets: %d%n", destroyedPlanets.size());
        destroyedPlanets.stream().sorted(String::compareTo).forEach(planet -> {
            System.out.printf("-> %s%n", planet);
        });
    }
}
