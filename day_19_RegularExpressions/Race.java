package day_19_RegularExpressions;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Race {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        List<String> names = Arrays.stream(scan.nextLine().split(", ")).collect(Collectors.toList());
        Map<String, Integer> racers  = new LinkedHashMap<>();
        names.forEach(name -> racers.put(name, 0));
        String input = scan.nextLine();

        while   (!input.equals("end of race")){

            Pattern pattern = Pattern.compile("\\w+");
            Matcher matcher = pattern.matcher(input);
            if(matcher.find()){
                StringBuilder currentRacerName = new StringBuilder();
                int distance = 0;
                for (int i = 0; i < input.length(); i++) {
                    char symbol = input.charAt(i);
                    if(Character.isDigit(symbol)){
                        distance+=Character.getNumericValue(symbol);
                    }else if(Character.isAlphabetic(symbol)){
                        currentRacerName.append(symbol);
                    }
                }

                if(racers.containsKey(currentRacerName.toString())){
                    racers.put(currentRacerName.toString(), racers.get(currentRacerName.toString())+distance);
                }
            }

            input = scan.nextLine();
        }

        List<String>sorted = racers.entrySet().stream()
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder())).limit(3)
                .map(Map.Entry::getKey).collect(Collectors.toList());
        System.out.printf("1st place: %s%n", sorted.get(0));
        System.out.printf("2nd place: %s%n", sorted.get(1));
        System.out.printf("3rd place: %s%n", sorted.get(2));

    }
}
