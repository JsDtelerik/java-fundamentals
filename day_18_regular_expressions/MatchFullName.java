package day_18_regular_expressions;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class MatchFullName {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String regex = "(?<firstname>\\b[A-Z][a-z]+) (?<lastanem>[A-Z][a-z]+)\\b";
        String names = scan.nextLine();
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(names);
        while (matcher.find()){
            System.out.print(matcher.group() + " ");
        }
    }
}
