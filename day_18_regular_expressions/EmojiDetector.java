package day_18_regular_expressions;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EmojiDetector {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        String input = scan.nextLine();
        long coolness = calculateTheEmojiCoolness(input);
        String regex = "([\\:*])\\1([A-Z][a-z]{2,})\\1\\1";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(input);
        List<String> allEmojis = new ArrayList<>();

        while (matcher.find()){
            allEmojis.add(matcher.group());
        }

        List<String> coolEmojis = new ArrayList<>();
        for (String emoji : allEmojis) {
            String separatedEmoji = emoji.replace("*", "");
            String reSeparate = separatedEmoji.replace(":", "");
            long currentEmojiCoolness = 0L;
            for (int i = 0; i < reSeparate.length(); i++) {
            char symbol = reSeparate.charAt(i);
            currentEmojiCoolness +=symbol;
            }
            if(currentEmojiCoolness>=coolness){
                coolEmojis.add(emoji);

            }
        }
        System.out.printf("Cool threshold: %d%n", coolness);
        System.out.printf("%d emojis found in the text. The cool ones are:%n", allEmojis.size());
        for (String coolEmoji : coolEmojis) {
            System.out.println(coolEmoji);
        }
    }

    static long calculateTheEmojiCoolness(String input) {
        long coolness = 1;
        boolean noDigits = true;
        for (int i = 0; i < input.length(); i++) {
            char symbol = input.charAt(i);
            if(Character.isDigit(symbol)){
                coolness *= Character.getNumericValue(symbol);
                noDigits = false;
            }
        }

        if(noDigits){
            return 0;
        }else{
            return coolness;
        }

    }
}
