package day_06_arrays;

import java.util.Arrays;
import java.util.Scanner;

public class sumEvenNumbers {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int[] numbers = Arrays.stream(scan.nextLine().split(" ")).mapToInt(Integer::parseInt).toArray();
        int sumEvenNumbers = 0;
        for (int number : numbers) {
            if ( number % 2 == 0){
                sumEvenNumbers+=number;
            }

        }
        System.out.print(sumEvenNumbers);
    }
}
