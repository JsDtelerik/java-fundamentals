package day_06_arrays;

import java.util.Arrays;
import java.util.Scanner;

public class equalArrays {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int[] firstLine = Arrays.stream(scan.nextLine().split(" ")).mapToInt(Integer::parseInt).toArray();
        int[] secondLine = Arrays.stream(scan.nextLine().split(" ")).mapToInt(Integer::parseInt).toArray();
        int arrSum = 0;
        boolean isDifferent = false;
        for (int i = 0; i < secondLine.length; i++) {
            arrSum += secondLine[i];
            if (firstLine[i] != secondLine[i]) {
                System.out.printf("Arrays are not identical. Found difference at %d index.", i);
                isDifferent = true;
                break;
            }
        }
        if (!isDifferent) {
            System.out.printf("Arrays are identical. Sum: %d", arrSum);
        }

    }
}
