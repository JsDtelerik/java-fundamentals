package day_06_arrays;

import java.util.Scanner;

public class dayOfWeek {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int index = Integer.parseInt(scan.nextLine());
        String[] dayOfWeek = {"Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"};
        if (index >=1 && index<=7){
            System.out.print(dayOfWeek[index-1]);
        }else{
            System.out.print("Invalid day!");
        }

    }
}
