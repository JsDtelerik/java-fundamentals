package day_06_arrays;

import java.util.Arrays;
import java.util.Scanner;

public class reverseArrayOfStrings {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        String[] elements = scan.nextLine().split(" ");

        String keepValue = " ";
        for (int i = 0; i < elements.length/2; i++) {
            keepValue = elements[i];
            elements[i] = elements[elements.length-1-i];
            elements[elements.length-1-i] = keepValue;

        }

        for (String element : elements) {
            System.out.print(element + " ");

        }
    }
}
