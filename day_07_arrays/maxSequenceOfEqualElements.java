package day_07_arrays;

import java.util.Arrays;
import java.util.Scanner;

public class maxSequenceOfEqualElements {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int[] numbers = Arrays.stream(scan.nextLine().split(" ")).mapToInt(Integer::parseInt).toArray();

        int number = 0;
        int counter = 1;
        int maxCount = 0;

        for (int i = 1; i < numbers.length; i++) {
            if (numbers[i] == numbers[i - 1]) {
                counter++;
            } else {
                counter = 1;
            }
            if (counter > maxCount) {
                maxCount = counter;
                number = numbers[i];
            }
        }
        for (int i = 1; i <= maxCount; i++) {
            System.out.print(number + " ");
        }
    }
}

