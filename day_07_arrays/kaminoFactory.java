package day_07_arrays;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Scanner;

public class kaminoFactory {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int length = Integer.parseInt(scan.nextLine());
        String dna = scan.nextLine();
        int dnaMaxSum = 0;
        int dnaSum = 0;
        int sumOne = 0;
        int sumTwo = 0;
        int counterDna = 0;
        int counterOnes = 1;
        int keepBest = 0;
        int[] keepDna = new int[length];
        int compereArrOne = 0;
        int compereArrTwo = 0;



        while (!dna.equals("Clone them!")) {
            int[] currentDNA = Arrays.stream(dna.split("!+")).mapToInt(Integer::parseInt).toArray();
            counterDna++;
            boolean isIndex = false;
            for (int i = 1; i < currentDNA.length; i++) {
                if (currentDNA[i] == 1 && currentDNA[i - 1] == 1) {
                    counterOnes++;
                }
            }
            if (counterOnes > dnaMaxSum) {
                dnaMaxSum = counterOnes;
                keepDna = currentDNA;
                keepBest = counterDna;

            } else if (counterOnes == dnaMaxSum) {
                label:
                if (!isIndex) {
                    for (int index = 0; index < keepDna.length; index++) {
                        if (keepDna[index] == 1) {
                            compereArrOne = index;
                            break;
                        }
                    }
                    for (int index1 = 0; index1 < currentDNA.length; index1++) {
                        if (currentDNA[index1] == 1) {
                            compereArrTwo = index1;
                            break;
                        }
                    }
                    if (compereArrTwo > compereArrOne) {
                        keepBest = counterDna;

                        keepDna = currentDNA;
                        isIndex = true;
                        break label;
                    }

                }

                if (!isIndex) {

                    for (int i = 0; i < keepDna.length; i++) {
                        sumOne += keepDna[i];
                    }
                    for (int i = 0; i < currentDNA.length; i++) {
                        sumTwo += currentDNA[i];
                    }


                    if (sumOne > sumTwo) {

                        dnaMaxSum = sumOne;

                    } else if (sumTwo > sumOne) {
                        keepBest = counterDna;
                        dnaMaxSum = sumTwo;
                        keepDna = currentDNA;


                    }
                }
            }


            dna =scan.nextLine();
            counterOnes = 1;
        }

        System.out.printf("Best DNA sample %d with sum: %d.%n",keepBest,dnaMaxSum);
        for(int i:keepDna){
            System.out.print(i+" ");

        }
    }
}