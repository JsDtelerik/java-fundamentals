package day_07_arrays;

import java.util.Scanner;

public class arrayRotation {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String[] numbers = scan.nextLine().split(" ");
        int n = Integer.parseInt(scan.nextLine());
        for (int i = 0; i < n; i++) {
            String number = numbers[0];
            for (int index = 0; index < numbers.length-1; index++) {
                numbers[index] = numbers[index+1];
            }
        numbers[numbers.length-1] = number;
        }

        System.out.println(String.join(" ", numbers));

    }
}
