package day_07_arrays;

import java.util.Arrays;
import java.util.Scanner;

public class equalSums {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int[] numbers = Arrays.stream(scan.nextLine().split(" ")).mapToInt(Integer::parseInt).toArray();

        boolean isFound = false;
        int keepIndexI = 0;
        for (int i = 0; i < numbers.length; i++) {
            int leftSumCalc = 0;
            int rightSumCalc = 0;

            for (int leftSum = 0; leftSum < i; leftSum++) {
                leftSumCalc += numbers[leftSum];
            }
            for (int rightSum = i + 1; rightSum < numbers.length; rightSum++) {
                rightSumCalc += numbers[rightSum];

            }
            if (leftSumCalc == rightSumCalc) {
                keepIndexI = i;
                isFound = true;
                break;
            }
        }
        if (isFound) {
            System.out.print(keepIndexI);
        } else {
            System.out.print("no");
        }
    }
}