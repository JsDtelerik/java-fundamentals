package day_07_arrays;

import java.util.Arrays;
import java.util.Scanner;

public class ladyBugs {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int fieldSize = Integer.parseInt(scan.nextLine());
        int[] field = new int[fieldSize];

        int[] initialBugsLocation = Arrays.stream(scan.nextLine().split(" ")).mapToInt(Integer::parseInt).toArray();

        for (int i : initialBugsLocation) {
            if (field.length-1>=i){
                field[i]=1;
            }


        }



        String command = scan.nextLine();
    label:
        while (!command.equals("end")){
            String[] arrCommand = command.split(" ");
            int bugIndex = Integer.parseInt(arrCommand[0]);
            String movingDirection = arrCommand[1];
            int flightLength = Integer.parseInt(arrCommand[2]);
            boolean flightAway = false;

            if (field[bugIndex] == 1){
                if(movingDirection.equals("right") && flightLength>0){
                    int calc = bugIndex+flightLength;
                    if (calc == field.length) {
                        field[bugIndex] = 0;
                        command = scan.nextLine();
                        break label;
                    }
                    if(field[bugIndex+flightLength]==0){
                        field[bugIndex+flightLength] = 1;
                        field[bugIndex]=0;
                    }else if (field[bugIndex+flightLength]==1){
                        for (int i = field[bugIndex+flightLength]; i < field.length; i++) {
                            if (field[i] == 0){
                                field[i] = 1;
                                field[bugIndex]=0;
                                break;
                            }else if (i == field.length-1 && field[i] == 1){
                                flightAway = true;
                                break;
                            }

                        }
                    }
                }else if (movingDirection.equals("right")&&flightLength<0){
                    if(field[bugIndex-Math.abs(flightLength)]==0){
                        field[bugIndex-Math.abs(flightLength)]=1;
                        field[bugIndex]=0;
                    }else if (field[bugIndex-Math.abs(flightLength)]==1){
                        for (int i = bugIndex-Math.abs(flightLength); i <= 0; i--) {
                            if (field[i] == 0){
                                field[i] = 1;
                                field[bugIndex]=0;
                                break;
                            }else if (i == 0 && field[i] == 1){
                                flightAway = true;
                                break;
                            }
                        }
                    }
                }else if (movingDirection.equals("left") && flightLength>0){
                    if (field[bugIndex-flightLength]==0){
                        field[bugIndex-flightLength]=1;
                        field[bugIndex]=0;
                    }else if (field[bugIndex-flightLength]==1){
                        for (int i = bugIndex-flightLength; i < field.length; i--) {
                            if (field[i] == 0){
                                field[i] = 1;
                                field[bugIndex]=0;
                                break;
                            }else if ( i == 0 && field[i] == 1){
                                flightAway = true;
                                break;
                            }
                        }
                    }
                }else if (movingDirection.equals("left") && flightLength<0){
                    if (field[bugIndex+Math.abs(flightLength)]==0){
                        field[bugIndex+Math.abs(flightLength)] = 1;
                        field[bugIndex]=0;
                    }else if (field[bugIndex+Math.abs(flightLength)]==1){
                        for (int i = bugIndex+Math.abs(flightLength); i <=field.length ; i++) {
                            if (field[i] == 0){
                                field[i] = 1;
                                field[bugIndex]=0;
                                break;
                            }else if (i == 0 && field.length-1==1){
                                flightAway = true;
                                break;
                            }
                        }
                    }
                }
            }

            if (flightAway){
                if (field[bugIndex] == 1 ){
                    field[bugIndex] = 0;
                }
            }



            command = scan.nextLine();
        }

        for (int i : field) {
            System.out.print(i + " ");

        }
    }
}
