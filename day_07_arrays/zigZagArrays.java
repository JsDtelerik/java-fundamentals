package day_07_arrays;

import java.util.Arrays;
import java.util.Scanner;

public class zigZagArrays {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int n = Integer.parseInt(scan.nextLine());

        int[] firstLine = new int[n];
        int[] secondLine = new int[n];
        for (int i = 0; i < n; i++) {
            int[] temp = Arrays.stream(scan.nextLine().split(" ")).mapToInt(Integer::parseInt).toArray();
            if ((i+1)%2 == 0){
                firstLine[i]=temp[1];
                secondLine[i]=temp[0];
            }else{
                firstLine[i]=temp[0];
                secondLine[i]=temp[1];
            }
        }
        for (int number1 : firstLine) {
            System.out.print(number1 + " ");
        }
        System.out.println();
        for (int number2 : secondLine) {
            System.out.print(number2 + " ");


        }

    }
}
