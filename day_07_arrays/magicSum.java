package day_07_arrays;

import java.util.Arrays;
import java.util.Scanner;

public class magicSum {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int[] numbers = Arrays.stream(scan.nextLine().split(" ")).mapToInt(Integer::parseInt).toArray();
        int magicNum = Integer.parseInt(scan.nextLine());
        for (int i = 0; i < numbers.length; i++) {
            for (int j = i; j <numbers.length; j++) {
                if(i == j){
                    continue;
                }
                if((numbers[i]+numbers[j]) == magicNum){
                    System.out.println(numbers[i] + " " + numbers[j]);
                    break;
                }
            }
        }
    }
}
