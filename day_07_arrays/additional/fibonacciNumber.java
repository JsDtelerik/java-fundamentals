package day_07_arrays.additional;

import java.util.Scanner;

public class fibonacciNumber {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = Integer.parseInt(scan.nextLine());
        int[] getFabonacci = new int[n+2];
        getFabonacci[0] = 0;
        getFabonacci[1] = 1;
        for (int i = 2; i <= n; i++) {
            getFabonacci[i] = getFabonacci[i-1]+getFabonacci[i-2];
        }
        
        System.out.println(getFabonacci[n]);
    }
}
