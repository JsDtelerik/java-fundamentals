package day_07_arrays.additional;

import java.util.Arrays;
import java.util.Scanner;

public class encryptSortAndPrintArray {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int receivedWords = Integer.parseInt(scan.nextLine());
        int wordSum = 0;
        int[] wordsSumArray = new int[receivedWords];
        for (int i = 0; i <receivedWords ; i++) {
            String text = scan.nextLine();
            int  vowelSum = calculateStringVowelSum(text);
            int  consonantsSum = calculateStringConsonantsSum(text);
            wordSum = vowelSum+consonantsSum;
            wordsSumArray[i] = wordSum;
        }
        Arrays.sort(wordsSumArray);

        printSortedArray(wordsSumArray);

    }

    static int calculateStringVowelSum(String text) {
        int vowelSum = 0;
        for (int i = 0; i < text.length(); i++) {
            char symbol = text.charAt(i);
            if (symbol == 65 || symbol == 97 || symbol == 69 || symbol == 101 || symbol==73 || symbol == 105
                    || symbol == 79 || symbol == 111 || symbol == 85 || symbol == 117){
                vowelSum += symbol*text.length();
            }
        }
        return vowelSum;
    }

    static int calculateStringConsonantsSum(String text) {
        int consonantsSum = 0;
        for (int i = 0; i < text.length(); i++) {
            char symbol = text.charAt(i);
            if (symbol == 65 || symbol == 97 || symbol == 69 || symbol == 101 || symbol==73 || symbol == 105
                    || symbol == 79 || symbol == 111 || symbol == 85 || symbol == 117){
                continue;
            }else{
                consonantsSum += symbol/text.length();
            }
        }
        return consonantsSum;
    }
    private static void printSortedArray(int[] wordsSumArray) {
        for (int i : wordsSumArray) {
            System.out.println(i);

        }
    }

}
