package day_07_arrays;

import java.util.Arrays;
import java.util.Scanner;

public class topInteger {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int[] numbers = Arrays.stream(scan.nextLine().split(" ")).mapToInt(Integer::parseInt).toArray();
        for (int i = 0; i < numbers.length; i++) {
            int number = numbers[i];
            boolean isBigger = false;
            if (i == numbers.length-1){
                System.out.print(number);
                break;
            }

            for (int j = i+1; j <= numbers.length-1; j++) {
                if(number>numbers[j]){
                    isBigger = true;
                }else{
                    isBigger = false;
                    break;
                }
            }
            if (isBigger){
                System.out.print(number + " ");
            }

        }

    }
}
