package day_07_arrays;

import java.util.Scanner;

public class commonElements {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        String[] firstArr = scan.nextLine().split(" ");
        String[] secondArr = scan.nextLine().split(" ");

        for (String word2 : secondArr) {
            for (String word1 : firstArr) {
                if (word2.equals(word1)) {
                    System.out.print(word1 + " ");
                    break;

                }
            }
        }
    }
}

