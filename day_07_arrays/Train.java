package day_07_arrays;

import java.util.Scanner;

public class Train {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = Integer.parseInt(scan.nextLine());

        int[] wagons = new int[n];
        for (int i = 0; i < wagons.length; i++){
            wagons[i] = Integer.parseInt(scan.nextLine());
        }
        int sum = 0;
        for (int wagon : wagons) {
            System.out.print(wagon + " ");
            sum +=wagon;

        }
        System.out.printf("%n%d",sum);
    }
}
