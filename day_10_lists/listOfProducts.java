package day_10_lists;

import java.util.*;
import java.util.stream.Collectors;

public class listOfProducts {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = Integer.parseInt(scan.nextLine());
        List<String> products = new ArrayList<>();

        for (int i = 0; i < n; i++) {
            String newProduct = scan.nextLine();
            products.add(i, newProduct);
        }

        Collections.sort(products);
        int i = 1;
        for (String product : products) {
            System.out.printf("%d.%s%n", i, product);
            i++;
        }
    }
}
