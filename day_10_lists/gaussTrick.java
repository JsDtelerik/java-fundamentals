package day_10_lists;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class gaussTrick {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        List<Integer> numbers = Arrays.stream(scan.nextLine().split("\\s+")).map(Integer::parseInt).collect(Collectors.toList());

        List<Integer> gaussTrickCalculation = returnNewListWithSumNumbers(numbers);

        print(gaussTrickCalculation);
    }


    static List<Integer> returnNewListWithSumNumbers(List<Integer> numbers) {
            int getSize = numbers.size()/2;
        for (int i = 0; i < getSize; i++) {
            int sum = numbers.get(i)+numbers.get(numbers.size()-1);
            numbers.set(i, sum);
            numbers.remove(numbers.size()-1);


        }
        return numbers;
    }

    static void print(List<Integer> gaussTrickCalculation) {
        List<String> intToString = new ArrayList<>(gaussTrickCalculation.size());
        for (Integer i : gaussTrickCalculation) {
            intToString.add(i.toString());
        }

        System.out.println(String.join(" ", intToString));

    }

}
