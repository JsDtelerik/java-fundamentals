package day_10_lists;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class mergingLists {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        List<String> firstLine = Arrays.stream(scan.nextLine().split("\\s+")).collect(Collectors.toList());
        List<String> secondLine = Arrays.stream(scan.nextLine().split("\\s+")).collect(Collectors.toList());

        int longerLine = findLongerLine(firstLine, secondLine);
        List<String> splitLines = splitBothLines(firstLine, secondLine, longerLine);
        print(splitLines);
    }



    static int findLongerLine(List<String> firstLine, List<String> secondLine) {
        if(firstLine.size()>=secondLine.size()){
            return firstLine.size();
        }else{
            return  secondLine.size();
        }
    }

    static List<String> splitBothLines(List<String> firstLine, List<String> secondLine, int longerLine) {
        List<String> splitLn = new ArrayList<>();
        for (int i = 0; i < longerLine; i++) {
            if(firstLine.size()>i){
                splitLn.add(firstLine.get(i));
            }
            if(secondLine.size()>i){
                splitLn.add(secondLine.get(i));
            }
        }
        return splitLn;
    }

    static void print(List<String> splitLines) {

        System.out.println(String.join(" ", splitLines));
    }

}
