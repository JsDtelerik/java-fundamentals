package day_10_lists;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class listManipulationBasics {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        List<Integer> numbers = Arrays.stream(scan.nextLine().split("\\s+")).map(Integer::parseInt).collect(Collectors.toList());

        String[] command = scan.nextLine().split("\\s+");
        List<Integer> manipulatedList = new ArrayList<>();
        while(!command[0].equals("end")){

            switch (command[0]){

                case "Add":
                 manipulatedList = addCommand(numbers, command);
                    break;
                case "Remove":
                    manipulatedList = removeCommand(numbers, command);
                    break;
                case "RemoveAt":
                    manipulatedList = removeAtCommand(numbers, command);
                    break;
                case "Insert":
                    manipulatedList = insertCommand(numbers, command);
                    break;

            }

            command = scan.nextLine().split("\\s+");
        }

        printManipulatedList(manipulatedList);
    }


    static List<Integer> addCommand(List<Integer> numbers, String[] command) {

        numbers.add(numbers.size(), Integer.parseInt(command[1]));
        return numbers;
    }
    static List<Integer> removeCommand(List<Integer> numbers, String[] command) {

        numbers.remove(Integer.valueOf(Integer.parseInt(command[1])));

        return numbers;
    }

    static List<Integer> removeAtCommand(List<Integer> numbers, String[] command) {

        numbers.remove(Integer.parseInt(command[1]));
        return (numbers);
    }
    static List<Integer> insertCommand(List<Integer> numbers, String[] command) {

        numbers.add(Integer.parseInt(command[2]), Integer.parseInt(command[1]));

        return numbers;
    }
    private static void printManipulatedList(List<Integer> manipulatedList) {
        List<String> editedListToString = new ArrayList<>();
        for (Integer i : manipulatedList) {
            editedListToString.add(i.toString());
        }
        System.out.print(String.join(" ", editedListToString));
    }


}
