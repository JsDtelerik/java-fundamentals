package day_10_lists;

import java.text.DecimalFormat;
import java.util.*;
import java.util.stream.Collectors;

public class sumAdjacentEqualNumbers {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        List<Double> numbers = Arrays.stream(scan.nextLine().split("\\s+")).map(Double::parseDouble).collect(Collectors.toList());


        List<Double> sumEquals = sumEqualNumbers(numbers);

        printResult(sumEquals);
    }



    static List<Double> sumEqualNumbers(List<Double> numbers) {

        for (int i = 0; i < numbers.size()-1; i++) {
         if(numbers.get(i).equals(numbers.get(i+1))){
             double sum = numbers.get(i)+numbers.get(i+1);
             numbers.set(i, sum);
             numbers.remove(i+1);
             i = -1;
         }
        }

        return numbers;
    }

    private static void printResult(List<Double> sumEquals) {
        DecimalFormat decimalFormat = new DecimalFormat("0.#");
        for (Double num : sumEquals) {
            System.out.print(decimalFormat.format(num) + " ");

        }
    }

}
