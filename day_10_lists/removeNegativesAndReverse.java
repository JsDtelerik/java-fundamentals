package day_10_lists;

import com.sun.jdi.IntegerValue;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class removeNegativesAndReverse {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        List<Integer> numbers = Arrays.stream(scan.nextLine().split("\\s+")).map(Integer::parseInt).collect(Collectors.toList());



        for(int i = 0; i<numbers.size(); i++) {
            if(numbers.get(i)<0){
                numbers.remove(i);
                i = -1;
            }
        }

            if(numbers.isEmpty()){
                System.out.println("empty");
            }else{
                Collections.reverse(numbers);
                for (Integer reversedNumbers : numbers) {
                    System.out.print(reversedNumbers + " ");

                }
            }


    }
}
