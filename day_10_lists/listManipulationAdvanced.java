package day_10_lists;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class listManipulationAdvanced {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        List<Integer> numbers = Arrays.stream(scan.nextLine().split("\\s+")).map(Integer::parseInt).collect(Collectors.toList());

        String[] command = scan.nextLine().split("\\s+");

        while(!command[0].equals("end")){

            switch (command[0]){

                case "Contains":
                    checkIfListOfNumbers(numbers, command);
                    break;
                case "Print":
                    printEvenOrOddNumbersFromTheList(numbers, command);
                    break;
                case "Get":
                    sumAllIntegersFromTheListAndPrintIt(numbers);
                    break;
                case "Filter":
                    printNumbersThatFullfillTheConditionFromCommand(numbers, command);
                    break;
            }

            command = scan.nextLine().split("\\s+");
        }
    }



    private static void checkIfListOfNumbers(List<Integer> numbers, String[] command) {
        if(numbers.contains(Integer.parseInt(command[1]))){
            System.out.println("Yes");
        }else{
            System.out.println("No such number");
        }

    }
    private static void printEvenOrOddNumbersFromTheList(List<Integer> numbers, String[] command) {

        switch (command[1]){
            case "even":
                for (Integer number : numbers) {
                    if (number %2 ==0){
                        System.out.print(number + " ");
                    }
                }
                System.out.println();
                break;
            case "odd":
                for (Integer number : numbers) {
                    if (number %2 !=0){
                        System.out.print(number + " ");
                    }
                }
                System.out.println();
                break;
        }

    }

    private static void sumAllIntegersFromTheListAndPrintIt(List<Integer> numbers) {
        int sum = 0;
        for (Integer number : numbers) {
            sum += number;
        }
        System.out.println(sum);
    }

    private static void printNumbersThatFullfillTheConditionFromCommand(List<Integer> numbers, String[] command) {
        int commandNumber = Integer.parseInt(command[2]);
        int condition = 0;
        switch (command[1]){
            case ">": condition = 1; break;
            case "<": condition = 2; break;
            case "<=": condition = 3; break;
            case ">=": condition = 4; break;
        }
            printFilteredNumber(numbers, condition, commandNumber);
    }

    private static void printFilteredNumber(List<Integer> numbers, int condition, int commandNumber) {

        for (Integer num : numbers) {
            if (condition == 1){
                if(num > commandNumber){
                    System.out.print(num + " ");
                }
            }else if (condition == 2){
                if(num < commandNumber){
                    System.out.print(num + " ");
                }
            }else if(condition == 3){
                if(num <= commandNumber){
                    System.out.print(num + " ");
                }
            }else if(condition == 4){
                if(num >= commandNumber){
                    System.out.print(num + " ");
                }
            }

        }
        System.out.println();
    }

}
