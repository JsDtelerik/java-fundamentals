package day_01;

import java.util.Scanner;

public class MultiplicationTable {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = Integer.parseInt(scan.nextLine());
        int counter =0;
        while (counter<10){
            counter++;
            System.out.printf("%d X %d = %d%n", n, counter, n*counter);
        }
    }
}
