package day_01;

import java.util.Scanner;

public class ForeignLanguages {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String country = scan.nextLine();
        switch (country) {
            case "USA":
            case "England":
                System.out.print("English");
                break;
            case "Mexico":
            case "Spain":
            case "Argentina":
                System.out.print("Spanish");
                break;

            default:
                System.out.print("unknown"); break;

        }
    }
}
