package day_01;

import java.util.Scanner;

public class SumOfOddNumbers {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = Integer.parseInt(scan.nextLine());
        int number = 0;
        int oddNumberSum = 0;
        for (int i = 1; i <=n; i++) {
            number = (i*2)-1;
            System.out.println(number);
            oddNumberSum+=number;

        }
        System.out.printf("Sum: %d", oddNumberSum);
    }
}
