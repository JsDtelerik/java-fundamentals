package day_01;

import java.util.Scanner;

public class MultiplicationTableDoWhile {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int n = Integer.parseInt(scan.nextLine());
        int multiplier = Integer.parseInt(scan.nextLine());

        do{
            System.out.printf("%d X %d = %d%n", n, multiplier, n*multiplier);
            multiplier++;
        }while(multiplier<=10);

        }

}
